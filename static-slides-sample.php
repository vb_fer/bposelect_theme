<?php

// Warning :
// > You should copy this file (e.g. my-static-slides.php) if you want to customize the static slides!
// > Automatic update will replace this file so all your changes can be lost :(

// --- Slide template -----------------------------------
//<div class="da-slide">
//	<h2>Slide tittle</h2>
//	<p>Slide text here...</p>
//	<a href="http://jltweb.info/realisations/wp-parallax-content-plugin/" class="da-link">Read more link</a>
//	<div class="da-img"><img src="$plugin_abs_path/images/1.png" alt="Slkide image" /></div>
//</div>
// -------------------------------------------------------

$read_more = __('Read more','wp-parallax-content-slider');

$outputStatic = <<<STATICOUTPUT
<div id="da-slider" class="da-slider">
	<div class="da-slide">
		<h2><a href="http://bristonoutsourcing.com.au/solutions/it-management-and-support/">it management and support solutions</a></h2>
		<p><a href="http://bristonoutsourcing.com.au/solutions/it-management-and-support/">BOSS provides Integrated IT solutions to help your business achieve optimum competency and productivity. The end-to-end IT services are designed to deliver the desired results through managed operations while controlling the costs.</a></p>
		<p><a href="http://bristonoutsourcing.com.au/solutions/it-management-and-support/"><img src="$plugin_abs_path/images/morebutton.png" alt="more button" /></a></p>
		<div class="da-img"><a href="http://bristonoutsourcing.com.au/solutions/it-management-and-support/"><img src="$plugin_abs_path/images/1.png" alt="briston success" /></a></div>
	</div>
	<div class="da-slide">
		<h2><a href="http://bristonoutsourcing.com.au/hiring-in-house-vs-outsourcing/">HIRING IN-HOUSE VS OUTSOURCING</a></h2>
		<p><a href="http://bristonoutsourcing.com.au/hiring-in-house-vs-outsourcing/">Analysing the Suitable Hiring Option for Your Business.</a></p>
		<div class="da-img"><a href="#"><img src="$plugin_abs_path/images/2.png" alt="briston success" /></a></div>
	</div>
	<div class="da-slide">
		<h2><a href="http://bristonoutsourcing.com.au/smart-outsourcing-maximising-roi-by-tackling-common-issues/">SMART OUTSOURCING</a></h2>
		<p><a href="http://bristonoutsourcing.com.au/smart-outsourcing-maximising-roi-by-tackling-common-issues/">Maximising ROI By Tackling Common Issues.</a></p>
		<div class="da-img"><a href="#"><img src="$plugin_abs_path/images/3.png" alt="briston success" /></a></div>
	</div>
	
	
	
	
	<nav class="da-arrows">
		<span class="da-arrows-prev"></span>
		<span class="da-arrows-next"></span>
	</nav>
</div>
STATICOUTPUT;

?>