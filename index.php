<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

		

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
        <div class="innerPageHeader"></div>
        <div class="innerContainer">
        	<div class="innerLeft">
            	<?php if ( have_posts() ) : ?>
                    <?php /* The loop */ ?>
					 <?php //$wp_query = new WP_Query( 'cat=-7' ); ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                        <?php //get_template_part( 'content', get_post_format() ); ?>
                        <h2><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
                        <p>Posted in : &nbsp;<?php the_category()?> &nbsp; | by <?php the_author() ?></p>
                        <p><?php the_content('Click here to read more'); ?></p>
						
                        
                    <?php endwhile; ?>
					<hr>
                    <?php bposelect_paging_nav(); ?>
        
                <?php else : ?>
                    <?php get_template_part( 'content', 'none' ); ?>
                <?php endif; ?>
            </div>
            <div class="innerRight">	<?php get_template_part( 'default', 'right_sidebar' ); ?><?php get_sidebar(); ?>
			<div style="width:100%;height:70px;margin:auto;text-align:center;clear:both;padding-top:5px;">
				<?php echo (show_ads('outsourcinginfo','top')); ?>
			</div>
			<div style="width:100%;height:70px;margin:auto;text-align:center;clear:both;padding-top:5px;">
				<?php echo (show_ads('outsourcinginfo','mid')); ?>
			</div>
			<div style="width:100%;height:70px;margin:auto;text-align:center;clear:both;padding-top:5px;">
				<?php echo (show_ads('outsourcinginfo','bottom')); ?>
			</div>
			</div>
			
			<!-- start of ads placement -->
				
				
				<!--end ads placement-->
        </div>
                
		</div>
        
		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>