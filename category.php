<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div class="newsHeader">
    	<img src="http://dev.graphicrebel.com/briston/wp-content/themes/briston/images/newsHeader.png" alt="news Header"/>
    </div>	

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>
			</header><!-- .archive-header -->

		<div class="newsHolder">
                <div class="newsContainer"> 
                    <div class="newsLeft"><?php get_sidebar(); ?></div>
                    <div class="newsRight">               
                        <?php if ( have_posts() ) : ?>
                            <?php /* The loop */ ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php //get_template_part( 'content', get_post_format() ); ?>
                                <h2><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
                                <p>Posted By : <?php the_author()?> on : <?php the_date()?></p>
                                <?php the_content()?>
                                
                                <br/>
                            <?php endwhile; ?>
                
                            <?php twentythirteen_paging_nav(); ?>
                
                        <?php else : ?>
                            <?php get_template_part( 'content', 'none' ); ?>
                        <?php endif; ?>
                        </div>
                </div>        
			</div>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>