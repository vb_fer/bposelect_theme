<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
    
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css"/>
    
	<?php wp_head(); ?>
</head>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js"></script>



<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/jquery.ui/jquery-ui.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.ui/jquery-ui.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.multiple.select.js"></script>

  <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/tootip.js"></script>

<!-- lightbox -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.css?v=2.1.5" media="screen" />

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- end of lightbox -->
<script>
  $(function() {
    
    $( "#companyname" ).autocomplete({
      source: "<?php echo get_template_directory_uri();?>/compsource.php",
	  minLength: 2,
	  select: function(event, ui) { 
			$("#companyname").val(ui.item.value);
            $("#searchcompanyname").submit(); }
    
    });
  });
</script>

<script type="text/javascript">
//a public function that the container uses to pass in values for the labels
function public_Labels(label1, label2, label3, label4, label5){
	t1.innerText = label1;
	t2.innerText = label2;
	t3.innerText = label3;
	t4.innerText = label4; 
	t5.innerText = label5;
}

//a public function that the container uses to pass in values for the card containers
function public_Contents(contents1, contents2, contents3, contents4, contents5){

	t1Contents.innerHTML = contents1;
	t2Contents.innerHTML = contents2;
	t3Contents.innerHTML = contents3;
	t4Contents.innerHTML = contents4;
	t5Contents.innerHTML = contents5;

	init();
}

//sets the default display to tab 1
function init(){
	tabContents.innerHTML = t1Contents.innerHTML;
	//alert('asdas');
}	

//this is the tab switching function
var currentTab;
var tabBase;
var firstFlag = true;


function changeTabs(){
	if(firstFlag == true){
		currentTab = t1;
		tabBase = t1base;
		firstFlag = false;
	}

	if(window.event.srcElement.className == "xtab"){

		currentTab.className = "xtab";
		tabBase.style.backgroundColor = "#fff";
		currentTab = window.event.srcElement;
		tabBaseID = currentTab.id + "base";
		tabContentID = currentTab.id + "Contents";
		tabBase = document.all(tabBaseID);
		tabContent = document.all(tabContentID);
		currentTab.className = "selTab";
		tabBase.style.backgroundColor = "";
		tabContents.innerHTML = tabContent.innerHTML;
	}
}

</script>

<body <?php body_class(); ?>  onload="init();" >
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			
            <div class="headerContainer">                
                <div class="headerLeftContainer">
                    <a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/bposelectlogo.png" alt="BPO Select logo"/>
                    </a>
                </div>
                <div class="headerRightContainer">
                	<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<h3 class="menu-toggle"><?php _e( 'Menu', 'twentythirteen' ); ?></h3>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>					
				</nav>
                </div>
               
                
            </div>
			<div id="navbar" class="navbar">
				<!-- #site-navigation -->
			</div><!-- #navbar -->
			
		</header><!-- #masthead -->
        
		<!-- -->
		<div id="main" class="site-main">
        
        
