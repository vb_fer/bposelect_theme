<?php
/**
 * Template Name: Boss Homepage
 *
 * Author Bryan Durana
 * http://dev.bristonoutsourcing.com/
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

 
 

get_header(); ?>
<!-- rotating div for news -->

<!-- end of rotating div for news -->
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<!--Your code-->
			<div class="sliderContainer">
            	<div class="sliderInnerContainer">
                	<?php if ( function_exists( 'get_wp_parallax_content_slider' ) ) { get_wp_parallax_content_slider(); } ?>
                </div>            	
            </div>
            <div class="homeContentContainer">
            	<h3>BOSS SOLUTIONS FOR BUSINESS GROWTH</h3>
                <p>Briston Outsourcing Service Solutions (BOSS) guarantees to provide cost-effective solutions and tools to empower our clients to face challenges that may affect their business operations, sales and revenue. We can provide assistance through our wide range of outsourcing services covering various practices primarily IT, sales and marketing and back office administration. We can also customise a complete service package for clients with unique and specific business settings.</p>
            </div>
            <div class="homeContentContainer">
            	<div class="heroContainer">
                	<div class="grid-block slide">
                        <div class="caption">
                            <ul>
                            	<li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#elearningrsources">E-learning Resources</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#admintraining">Administration Training</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#onlinestudent">Online Student Registry Management</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#studentsupport">Student Support for Distance Learning</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#onlinelibrary">Online Library Access Catalog</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#eresourcedev">E-learning Resource Development</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#studentportal">Student Portal Development</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#coursewaredev">Courseware Development</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#learnerdev">Learner Management Systemt Development</a></li>
                            </ul>
                        </div>
                    <img src="http://dev.bristonoutsourcing.com//wp-content/themes/boss-wp/images/hero-training.png" alt="hero banner IT"/>
					</div>
                </div>
                <div class="heroContainer">
                	<div class="grid-block slide">
                        <div class="caption">
                            <ul>
                            	<li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#webdev">Website Development and Maintenance</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#softdev">Software Development</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#appdev">Applications Development</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#networkadd">Network and Systems Administration</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#infosec">Information Security</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#datamanage">Database Management</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/it-management-and-support#voicesupport">Voice and Non-Voice Technical Support</a></li>
                            </ul>
                        </div>
                    <img src="http://dev.bristonoutsourcing.com//wp-content/themes/boss-wp/images/hero-itsupport.png" alt="hero banner IT"/>
					</div>
                </div>
                <div class="heroContainer">
                	<div class="grid-block slide">
                        <div class="caption">
                        	<ul>
                            	<li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#copywriting">Copy Writing and Editing</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#salesmarketing">Sales Campaign Management</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#marketmanagement">Marketing Strategy Management</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#onlinemanagement">Online Marketing Campaign Management</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#outboundsupport">Outbound Sales Support</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#visualpublishing">Visual Design and Publishing</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#marketinganalysis">Marketing Research and Analysis</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#datamining">Data Mining</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/sales-and-marketing#emailmarketing">Email Marketing</a></li>
                            </ul>
                        </div>
                    <img src="http://dev.bristonoutsourcing.com//wp-content/themes/boss-wp/images/hero-sales.png" alt="hero banner Sales"/>
					</div>
                </div>
                <div class="heroContainer">
                	<div class="grid-block slide">
                        <div class="caption">
                            <ul>
                            	<li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#talentstaffing">Talent Acquisition and Staffing</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#customerassistance">Customer Care and Helpdesk Assistance</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#dataencoding">Data Encoding</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#executivemanagement">Executive or Virtual Assistance</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#researchmanagement">Research Management</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#datapresentation">Data Presentations</a></li>
                                <li><a href="http://dev.bristonoutsourcing.com//solutions/administrative-support#emailsupport">Email and Chat Support</a></li>
                            </ul>
                        </div>
                    <img src="http://dev.bristonoutsourcing.com//wp-content/themes/boss-wp/images/hero-admin.png" alt="hero admin Sales"/>
					</div>
                </div> 
            </div>
           <br/><br/>
           
            <div class="newsHomeContainer">
            	<div class="newsContainer">
                    <ul id="news">					
					<li>
						<h3><a href="http://dev.bristonoutsourcing.com//hiring-in-house-vs-outsourcing/">What is happening out there?</a></h3> 
                        <p><a href="http://dev.bristonoutsourcing.com//hiring-in-house-vs-outsourcing/">The concept of outsourcing has been around since the 1970s, back when many Western manufacturing companies realised the need to consign less-than-essential work or tasks that external groups specialised in. But it was really in the early 2000s that outsourcing started gaining traction as a mainstream business industry. </a></p>
					</li>
					<li>
						<h3><a href="http://dev.bristonoutsourcing.com//smart-outsourcing-maximising-roi-by-tackling-common-issues/">Smart Outsourcing : Maximising ROI</a></h3> 
                        <p><a href="http://dev.bristonoutsourcing.com//smart-outsourcing-maximising-roi-by-tackling-common-issues/">There are two sides of the same coin just as well as outsourcing has its advantages and disadvantages. While the benefit of getting quality service at a lower cost is highlighted, there are emerging issues that makes business owners think that they might be at a loss if they gear towards outsourcing.</a></p>
					</li>					
					<li>
						<h3><a href="http://dev.bristonoutsourcing.com//hiring-in-house-vs-outsourcing/">Hiring In-House vs Outsourcing</a></h3> 
                        <p><a href="http://dev.bristonoutsourcing.com//hiring-in-house-vs-outsourcing/">Business owners, would immediately resort to outsourcing blinded with the benefit of lowering their operational costs. Then again, without analysing if outsourcing is the appropriate business move, results may be disappointing. In the middle of a crucial business decision, how can one determine the better option between hiring in-house employees and outsourcing? A quick outline for each option has been provided.</a></p>
					</li>					
									
				</ul>
                </div>
            </div>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>