<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>

		</div><!-- #main -->

      
        
		
			<div class="footerContainer">
            	<div class="footerInner">
                    <div class="footerLeft">
                    	<ul>
                        	<li><a href="<?php echo site_url() ?>/">Home</a></li>
                            <li><a href="<?php echo site_url() ?>/about-us/">About Us</a></li>
                            <li><a href="<?php echo site_url() ?>/outsourcing-news/">Outsourcing News</a></li>
                            <li><a href="<?php echo site_url() ?>/why-outsource/">Sitemap</a></li>
							<li><a href="<?php echo site_url() ?>/glossary/">Glossary of terms</a></li>
                            <li><a href="<?php echo site_url() ?>/contact-us/">Contact Us</a></li>
                            <li>Follow Us on :</li>
                            <li>
                            	<ul>
                            	<li><a href="#"><img src="http://bposelect.com/wp-content/themes/bposelect/images/facebook.png" alt="facebook"/></a></li>
                                <li><a href="#"><img src="http://bposelect.com/wp-content/themes/bposelect/images/googleplus.png" alt="google plus"/></a></li>
                                <li><a href="#"><img src="http://bposelect.com/wp-content/themes/bposelect/images/twitter.png" alt="twitter"/></a></li>
                                <li><a href="#"><img src="http://bposelect.com/wp-content/themes/bposelect/images/linkedin.png" alt="linkedin"/></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="footerMiddle">
                    	<p>Terms and Conditions</p>
                    	<p>BPO Select is a website dedicated to assist businesses by providing an information portal of the products and services offered by BPO companies in the Philippines. To visit or access our website in any manner means that you are subject to follow the terms and conditions set for all users.</p>
                        
                       <p><a href="<?php echo site_url() ?>/termsandcondition/">Click here to read the full details of our terms and conditions.</a></p>
                    </div>
                    <div class="footerRight">
                    	<h2>Interested in listing your BPO?</h2>
                        <p>Enter the information below and we will be</p>
                        <p>intouch with you within 24 hours.</p>
                        <!--<input type="text" placeholder="Enter your name">
                        <input type="text" placeholder="Enter your email address :">
                        <button>Submit</button>-->
						<?php es_subbox( $namefield = "YES", $desc = "", $group = "" ); ?>
						
                    </div>
				</div>
            </div>
			<!-- .site-info -->
		
        <div class="copyrightContainer">
        	<div class="copyrightInner"><p>© Copyright 2014 bposelect.com</p></div>
        </div><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
	
	
</body>
</html>