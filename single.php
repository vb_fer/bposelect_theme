<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			 <div class="innerPageHeader"></div>
            <div class="innerContainer">
                    <div class="innerLeft">
                        <?php if ( have_posts() ) : ?>
							<?php /* The loop */ ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php //get_template_part( 'content', get_post_format() ); ?>
                                <h2><?php the_title()?></h2>
                                
                                <?php the_content()?>
                                
                                <br/>
                            <?php endwhile; ?>
                
                            <?php twentythirteen_paging_nav(); ?>
                            <?php comments_template(); ?>
                        <?php else : ?>
                            <?php get_template_part( 'content', 'none' ); ?>
                        <?php endif; ?>
                    </div>
                <div class="innerRight"><?php get_sidebar(); ?></div>
            </div>
            
            
             
		</div>
			
			
			
			

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>