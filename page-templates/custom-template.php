<?php
/**
 * Template Name: Custom Template
 *
 * Author Olivia Hoback
 * www.olivia.nu
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

 
 

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<!--Your code-->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>