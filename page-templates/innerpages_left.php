<?php
/**
 * Template Name: Inner Pages Left
 *
 * Author Bryan Durana
 * www.bposelect.com
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

 
 

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<!-- .entry-header -->

					<div class="entry-content">
                    	<div class="innerPageHeader">
                        </div>
						<div class="innerPageContainer">
							<div class="contentContainer">
                             <div class="innerLeft">
							 
                            <?php the_content(); ?>
							
                            <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
                             </div>
							 <div class="innerRight">
							 
							<?php get_template_part( 'default', 'right_sidebar' ); ?>
							<?php get_template_part( 'top_companies', 'sidebar' ); ?>
							 
							 <div class="widget-area">
							 <div style="width:250px;height:100px;line-height:100px;text-align:center;margin:10px 38px">
									<?php echo (show_ads('contactus','top')); ?>
							</div>
							
							<div style="width:250px;height:100px;line-height:100px;text-align:center;margin:10px 38px">
									<?php echo (show_ads('contactus','mid')); ?>
							</div>
							
							 <?php //dynamic_sidebar( 'sidebar-1' );  ?>
							 </div>
							 
							 </div>
                         	</div>
                        </div>
						<!-- start of ads placement -->
				<div style="width:100%;height:90px;margin:auto;text-align:center;clear:both;">
				<?php echo (show_ads('contactus','bottom')); ?>
				</div>
				
				<!--end ads placement-->
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>