<?php
/**
 * Template Name: BPO
 *
 * Author Bryan Durana
 * http://bristonoutsourcing.com.au/
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */


get_header(); 

$budgets = array("0"=>"149 - below",
				 "150" => "150 - 249",
				 "250" => "250 - 349",
				 "350" => "350 - 449",
				 "450" => "450 - 549",
				 "550" => "550 - 649",
				 "650" => "650 - 749",
				 "750" => "750 - 849",
				 "850" => "850 - Above");					
?>

<!-- animated tabs -->

<script type="text/javascript">
	jQuery(document).ready(function() {
	
	hash = window.location.hash;
	if(hash != ""){
	elements = $('a[href="' + hash + '"]');

		jQuery('.tabs ' + hash).show().siblings().hide();
		elements.parent('li').addClass('active').siblings().removeClass('active');;
	}
	
	
    jQuery('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = jQuery(this).attr('href');
		//alert(currentAttrValue);
		
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
		
		
		
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
		
		//alert(this);
        e.preventDefault();
    });
});
</script>
<!-- end of animated tabs -->

<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<script type="text/javascript">
              $(document).ready(function(){
                
            $('.bxslider').bxSlider({
              auto: true,
              autoControls: false
            });
              });
            </script>
			<!--Your code-->
			<div class="sliderContainer">
            	     <ul class="bxslider">
                      <li>
                      	<div class="sliderInnerContainer1">
                      		<div class="sliderInner">
                            	<h2>What is BPO Select?</h2>
                                <p><img src="http://bposelect.com/wp-content/themes/bposelect/images/bposlider.png"/></p>
                                <p><input type="button" value="know more about bpo select" onClick="window.location.href='about-us/'"/></p>
                            </div>
                      	</div>
                      </li>
                      <li>
                      	<div class="sliderInnerContainer2">
                        	<div class="sliderInner2">
                            	<h2>Makati City</h2>
                                <div class="sliderInnerTextContainer">
                                <p>
                                	Makati is the financial center of the Philippines; it has the highest concentration of multinational and local corporations in the country. Major banks, corporations, department stores as well as foreign embassies are based in Makati. The biggest trading floor of the Philippine Stock Exchange is situated along the city’s Ayala Avenue. Makati is also known for being a major cultural and entertainment hub in Metro Manila.
                                </p>
                                </div>
                                <div class="spacer"></div>
                                <input type="button" value="know more about Makati City" onClick="window.location.href='search/public/location/7'"/>
                            </div>
                        </div>
                      </li>
                      <!--<li><div class="sliderInnerContainer3"></div></li>-->
                     </ul>    	
            </div>
            <div class="tabContainer" id="tabsearch">
            	<div class="tabs">
                    <ul class="tab-links">
                        <li class="active"><a href="#tab1">Quick Search</a></li>
                        <li><a id="tab2link" href="#tab2">Advanced Search</a></li>
                    </ul>
					
                    <div class="tab-content">
					
					
                        <div id="tab1" class="tab active">
						<?php
						//print_r($_GET['qloc']);
						
						?>
						
						<form action="search/public/" method="get" >
						<input type="hidden" name="searchtype" value="quicksearch"/>
                            <h3>What do you want to find out about today?</h3>
                            <?php if(isset($_GET['a']) && $_GET['a'] ==  "on"){ ?>
							<!-- basic search drop down -->
                            <dl class="dropdown"> 
                                <dt>
                                <a>
                                  <span>Select Seat cost per month</span>    
                                  <p class="multiSel"></p>  
                                </a>
                                </dt>
                                <dd>
                                    <div class="mutliSelect">
                                        <ul>
										<?php
										foreach($budgets as $key => $budget){ 
										
										$qsbudgetmin = explode('-',str_replace(' ','',$budget));
										if(isset($_GET['price'])){
											$qscheck_budget = (in_array($key,$_GET['price']))?"checked":"";
										}
										?>
										
										<li><input name="price[]" class="qsbudget" type="checkbox" value="<?php echo $key?>" id="qsbudget<?php echo $key ?>" <?php echo $qscheck_budget;?>/><label for="qsbudget<?php echo $key ?>"> <?php echo $budget ?></label></li>
										
										<?php } ?>

											<li><input name="price[]" class="qsbudget" type="checkbox" value="" id="priceall"/> <label for="priceall">Select All</label></li>
											<li style="text-align:center;"><a style="padding:10px;font-size:16px;background-color:gray;margin:10px auto;color:white;border-radius:10px;width:20%" id="dropdowncost">Done</a></li>
                                        </ul>
									
                                    </div>
                                </dd>
                            </dl>
                            <br>
                            <dl class="dropdown2"> 
                                <dt>
                                <a>
                                  <span>Select location</span>    
                                  <p class="multiSel2"></p>  
                                </a>
                                </dt>
                                <dd>
                                    <div class="mutliSelect2" id="quicksearchlocation">
									<ul>
									<?php
									$locadv1 = $wpdb->get_results("select id,name from location where isparent = '' order by name ASC", ARRAY_A);
									foreach($locadv1 as $advloc1) {
									?>
									<li><input type="checkbox" name="qloc[]" onClick="togglechkall(this.id,'quicklocdiv<?php echo $advloc1['id']?>')"  id="quickloc<?php echo $advloc1['id']?>"> 
										<label for="quickloc<?php echo $advloc1['id']?>"><?php echo $advloc1['name'];?></label>
                                        <div id="quicklocdiv<?php echo $advloc1['id']?>" style="margin-left:15px;">
										<?php
										$location1 = $wpdb->get_results("select id,name from location where isparent = " . $advloc1['id'] . " order by name ASC", ARRAY_A);
										 foreach($location1 as $loc) { 
										 if(isset($_GET['qloc'])){
											if(in_array($loc['id'],$_GET['qloc'])){
												$check = "checked";
											}
											else{
												$check = "";
											}
											}
											?>
                                            
											<input name="qloc[]" type="checkbox" value="<?php echo $loc['id']?>" id="<?php echo $loc['id']?>" <?php echo $check;?>/><label for="<?php echo $loc['id']?>"> <?php echo $loc['name']?></label>
                                           <?php 
										   }
										   ?>
										</div>
                                        </li>
										
									<?php } ?>
									<input onClick="togglechkall(this.id,'quicksearchlocation')" name="qloc[]" type="checkbox" value="0" id="locationall" /><label for="locationall"> Select All</label>
									<li style="text-align:center;"><a style="padding:10px;font-size:16px;background-color:gray;margin:10px auto;color:white;border-radius:10px;width:20%" id="dropdowncost">Done</a></li>
									</ul>
                                    </div>
                                </dd>
                              <button>Quick Search </button>
                            </dl>
							<?php } 
							else {
							?>
							
							<div class="sectionContainer" style="width:40%;float:left;border-bottom:none;">
							<p>* What is your estimated monthly budget range per seat? ( This does not include employee salary ) </p>	
								<ul class="list-column2">
										<div id="qsdivbudget1">
										<?php 
																				
										foreach($budgets as $key => $budget){ 
										
										$budgetmin = explode('-',str_replace(' ','',$budget));
										if(isset($_GET['price'])){
											$check_budget = (in_array($key,$_GET['price']))?"checked":"";
										}
										?>
										
										<li><input name="price[]" type="checkbox" value="<?php echo $key?>" id="budget1<?php echo $key ?>" <?php echo $check_budget;?>/><label for="budget1<?php echo $key ?>"> <?php echo $budget ?></label></li>
										
										<?php } ?>

                                            </div>
											<li><input type="checkbox" onClick="togglechkall(this.id,'qsdivbudget1')" id="qsbudgetselectall" value="Select all" /> 
											<label for="qsbudgetselectall">Select all</label></li>
                                        </ul>
							</div>
							
							 <div class="sectionContainer" id="divlocation1" style="float:left;width:60%;border-bottom:none;border-left:1px dashed #ccc;padding-left:20px;">
								
                                	<p>* Where is your preferred office location? Tick all that apply : </p>
                                    <ul class="list-column2">
                                    <?php
									$locadv = $wpdb->get_results("select id,name from location where isparent = '' order by name ASC", ARRAY_A);
									foreach($locadv as $advloc) {
										if(isset($_GET['qsparentadvloc'])){
											$check_advloc = (in_array($advloc['id'],$_GET['qsparentadvloc']))?"checked":"";
										}
									?>
										<li><input onClick="togglechkall(this.id,'locdiv1<?php echo $advloc['id']?>')" name="qsparentadvloc[]" type="checkbox" value="<?php echo $advloc['id']?>" id="parentloc1<?php echo $advloc['id']?>" <?php echo $check_advloc;?>/>
											<label for="parentloc1<?php echo $advloc['id']?>"> <?php echo $advloc['name']?></label></li>
									
									<?php		
											$city = $wpdb->get_results("select id,name,isparent from location where isparent = " . $advloc['id'] . " order by name ASC", ARRAY_A);
											echo "<div id='locdiv1" . $advloc['id'] . "'><ul>";
											foreach($city as $cityloc){
												if(isset($_GET['qloc'])){
													$check_city = (in_array($cityloc['id'],$_GET['qloc']))?"checked":"";
												}
											
										?>
											<li><input class="chk<?php echo $advloc['id'];?>" type="checkbox" name="qloc[]" value="<?php echo $cityloc['id'] ?>" id="loc1<?php echo $cityloc['id']?>" <?php echo $check_city; ?>> 
											<label for="loc1<?php echo $cityloc['id'] ?>"><?php echo $cityloc['name'];?></label></li>
									<?php
											}
											echo "</ul></div>";
									}
									?>
									<li><input type="checkbox" onClick="togglechkall(this.id,'divlocation1')" name="qssearchlocationall" id="qssearchlocationall"/><label for="qssearchlocationall"> Select all</label></li>
									</ul>
							
								
                                </div>
								<div style="text-align:center">
							  <button style="width:35%;float:none">Quick Search </button>
							  </div>
							<?php } ?>
							    </form>
                            <!-- end of basic search drop down -->
                        </div>
		
                        <div id="tab2" class="tab">
						 <form action="search/public/" method="get" id="advancedsearch">
							<input type="hidden" name="searchtype" value="advsearch"/>
					
                            <h3>Advanced Search</h3>
                            <div class="advancedSearchContainer">
                            	<div class="sectionContainer">
                                	<p>Tick the boxes below that specify your outsourcing requirements. </p>
                                    
									<label for="fullmanaged" class="error"></label>
									<p>Would you like to have fully Australian managed operations? <button><a class="fancybox" href="#inline1" >Read more</a></button></p>
                                    <ul>
                                    	<li><input type="radio" name="fullmanaged" value="1" id="fullmanageyes" <?php echo (isset($_GET['fullmanaged']) && $_GET['fullmanaged'] == 1) ? "checked" : "" ?>/> 
										<label for="fullmanageyes">Yes</label></li>
                                        <li><input type="radio" name="fullmanaged" value="0" id="fullmanageno" <?php echo (isset($_GET['fullmanaged']) && $_GET['fullmanaged'] == 0) ? "checked" : "" ?>/> 
										<label for="fullmanageno">No</label>
										</li>
                                        <li><input type="radio" name="fullmanaged" value="-1" id="fullmanage" <?php echo (isset($_GET['fullmanaged']) && $_GET['fullmanaged'] == -1) ? "checked" : "" ?>> 
										<label for="fullmanage">I don't mind</label></li>
                                    </ul>
                                </div>
                                <div class="sectionContainer">
									<label for="service[]" class="error"></label>
                                	<p>* What type of service(s) are you looking to outsource? Tick all that apply: <button><a class="fancybox" href="#inline2" >Read more</a></button></p>
									
									<ul class="list-column">
                                    <?php
		
									$services = $wpdb->get_results("select id,name from services order by name ASC", ARRAY_A);
									foreach($services as $service) {
									if(isset($_GET['service'])){
										$check_service = (in_array($service['id'],$_GET['service']))?"checked":"";
									}
									
									?>
										<li><input name="service[]" type="checkbox" value="<?php echo $service['id']?>" id="service<?php echo $service['id']?>" <?php echo $check_service ?>/>
											<label for="service<?php echo $service['id']?>"> <?php echo $service['name']?></label></li>
									
									<?php
									}
									?>
									
									</ul>
									
		
                                </div>
                                <div class="sectionContainer">
								<label for="seatmodel[]" class="error"></label>
                                	<p>* Which type of BPO seating model are you looking for?  <button><a class="fancybox" href="#inline3" >Read more</a></button></p>
                                    <div class="innerSection" id="seatmodeladv">
                                    	<ul>
										
										 <?php
										$seatmodel = $wpdb->get_results("select id,name from SeatModel order by name ASC", ARRAY_A);
										foreach($seatmodel as $seatmodel){
										if(isset($_GET['seatmodel'])){
											$check_seatmodel = (in_array($seatmodel['id'],$_GET['seatmodel']))?"checked":"";
							
										}
										
										?>
										<li><input name="seatmodel[]" type="checkbox" value="<?php echo $seatmodel['id']?>" id="seatmodel<?php echo $seatmodel['id']?>" <?php echo $check_seatmodel; ?>/> 
										<label for="seatmodel<?php echo $seatmodel['id']?>"><?php echo $seatmodel['name'];?></label></li>
										
										
										<?php 
										} 
										if(isset($_GET['seatmodel'])){
											$check_nopref = (in_array(0,$_GET['seatmodel']))?"checked":"";
							
										}
										?>
										
                                            <li><input type="checkbox" onClick="togglechkall(this.id,'seatmodeladv')" name="seatmodel[]" value="0" id="noseatmodel" <?php echo $check_nopref;?> /> <label for="noseatmodel">No preference</label></li>
										
                                        </ul>
                                    </div>
                                </div>
                                <div class="sectionContainer" >
								<label for="priceadv[]" class="error"></label>
                                	<p>* What is your estimated monthly budget range per seat? ( This does not include employee salary )  <button><a class="fancybox" href="#inline4" >Read more</a></button></p>
                                    	<ul class="list-column">
										<div id="qsdivbudget">
										<?php 
																				
										foreach($budgets as $key => $budget){ 
										
										$budgetmin = explode('-',str_replace(' ','',$budget));
										if(isset($_GET['priceadv'])){
											$check_budget = (in_array($key,$_GET['priceadv']))?"checked":"";
										}
										?>
										
										<li><input name="priceadv[]" type="checkbox" value="<?php echo $key?>" id="budget<?php echo $key ?>" <?php echo $check_budget;?>/><label for="budget<?php echo $key ?>"> <?php echo $budget ?></label></li>
										
										<?php } ?>

                                            </div>
											<li><input type="checkbox" onClick="togglechkall(this.id,'qsdivbudget')" id="budgetselectall" value="Select all" /> 
											<label for="budgetselectall">Select all</label></li>
                                        </ul>
                                   
                                </div>
                                <div class="sectionContainer" id="divlocation">
								<label for="advloc[]" class="error"></label>
                                	<p>* Where is your preferred office location? Tick all that apply :  <button><a class="fancybox" href="#inline5" >Read more</a></button></p>
                                    <ul class="list-column">
                                    <?php
									$locadv = $wpdb->get_results("select id,name from location where isparent = '' order by name ASC", ARRAY_A);
									foreach($locadv as $advloc) {
										if(isset($_GET['parentadvloc'])){
											$check_advloc = (in_array($advloc['id'],$_GET['parentadvloc']))?"checked":"";
										}
									?>
										<li><input onClick="togglechkall(this.id,'locdiv<?php echo $advloc['id']?>')" name="parentadvloc[]" type="checkbox" value="<?php echo $advloc['id']?>" id="parentloc<?php echo $advloc['id']?>" <?php echo $check_advloc;?>/>
											<label for="parentloc<?php echo $advloc['id']?>"> <?php echo $advloc['name']?></label></li>
									
									<?php		
											$city = $wpdb->get_results("select id,name,isparent from location where isparent = " . $advloc['id'] . " order by name ASC", ARRAY_A);
											echo "<div id='locdiv" . $advloc['id'] . "'><ul>";
											foreach($city as $cityloc){
												if(isset($_GET['advloc'])){
													$check_city = (in_array($cityloc['id'],$_GET['advloc']))?"checked":"";
												}
											
										?>
											<li><input class="chk<?php echo $advloc['id'];?>" type="checkbox" name="advloc[]" value="<?php echo $cityloc['id'] ?>" id="loc<?php echo $cityloc['id']?>" <?php echo $check_city; ?>> 
											<label for="loc<?php echo $cityloc['id'] ?>"><?php echo $cityloc['name'];?></label></li>
									<?php
											}
											echo "</ul></div>";
									}
									?>
									<li><input type="checkbox" onClick="togglechkall(this.id,'divlocation')" name="searchlocationall" id="searchlocationall"/><label for="searchlocationall"> Select all</label></li>
									</ul>
							
								
                                </div>
                                <div class="sectionContainer">
                                	<p>What other features would you like the company to have? Tick all that apply : </p>
									
                                    <ul class="list-column">
                                    <?php
									$feature = $wpdb->get_results("select * from feature order by name ASC", ARRAY_A);
									foreach($feature as $feature) {
									if(isset($_GET['feature'])){
													$check_feature = (in_array($feature['id'],$_GET['feature']))?"checked":"";
												}
									?>
									<li><input name="feature[]" type="checkbox" value="<?php echo $feature['id']?>" id="package<?php echo $feature['id']?>" title="asdasd" <?php echo $check_feature; ?> />
									<label for="package<?php echo $feature['id']?>" title="<?php echo $feature['tooltiptext']?>"> <?php echo $feature['name']?></label></li>
				
									<?php
									}
									?>
									
									</ul>

                                </div>
                                <div class="sectionContainer2">
                                	<p>* - Required information</p>
									<?php $styleb=(isset($_GET['a']) && $_GET['a'] == "on")?"":"style='width:35%;float:right;'";?>
                                    <button <?php echo $styleb;?>>Search</button>
                                </div>
                            </div>
							</form>
                        </div>
					
                    </div>
                </div>
                <!-- lightbox 1 -->
                <div id="inline1" style="width:600px;display: none;">
                    <h3>Would you like to have fully Australian managed operations?</h3>
                    <p>
                        A lot of BPO’s will provide client services and Filipino operations managers to assist their clients by catering to the needs of the staff.  Others provide Australian management personnel who live in the Philippines and work onsite fulltime to manage daily operations.  They facilitate and coordinate regular activities as well as provide a point of contact for clients who have queries and/or concerns.  Utilising a BPO with fully Australian managed operations has proven to be very beneficial for organisations looking for a smooth and effective transition into outsourcing.
                    </p>
                </div>
                <div id="inline2" style="width:600px;display: none;">
                    <h3>What type of service(s) are you looking to outsource? </h3>
                    <p>
                        BPO’s in the Philippines can cater to a lot of organisations in a variety of industry sectors.  The services displayed in this section encapsulate the broad range available through outsourcing.  As the service options are virtually limitless, BPO Select have bundled them in to categories to assist in searching for your desired result.
There are BPO’s that specialise in certain services and even in certain industries e.g. medical transcriptionists, technical drawing.  If you are looking for a highly specific requirement that hasn’t been listed in the search results, please contact us and we can assist you in finding the right provider for you.

                    </p>
                </div>
                <div id="inline3" style="width:600px;display: none;">
                    <h3> Which type of BPO seating model are you looking for?</h3>
                    <p>
                        The BPO’s in the Philippines offer a wide range of seating options.
						BPO’s that offer Fully Managed Services act as a ‘plug and play’ option for organisations who are seeking a complete offshoring solution.  Among other specific services the core offerings from these BPO’s are HR/recruitment, IT Support, Finance and Accounting, Client Services and all boast 24/7 security of their premises.  This option proves to be the most popular choice for companies looking to outsource as it usually provides the most seamless transition to utilising offshore staff.
						BPO’s that offer Seat Leasing enable their clients to lease seats for the staff that they would recruit and employ on their own.  This is a popular option for organisations that are familiar with Filipino operations and can recruit and handle the HR and IT aspects on their own.  Seat leasing is a more cost effective solution however, it is expected that the client is going to provide their own internal business services/operations.

                    </p>
                </div>
                <div id="inline4" style="width:600px;display: none;">
                    <h3> What is your estimated monthly budget range per seat? </h3>
                    <p>
                        BPO pricing is measured per seat per month (1 seat = 1 staff member). BPO’s invoice monthly based on the amount of seats a client is occupying at that time.  These prices/rates are set by each individual BPO and vary depending on the services that they offer.  Staff salary is not included in these price ranges as staff are paid varying amounts based on their job description and/or experience.
                    </p>
                </div>
                <div id="inline5" style="width:600px;display: none;">
                    <h3>Where is your preferred office location?</h3>
                    <p>
                        The Philippines boasts multiple districts setup to cater for the BPO industry.  Listed in this section are the main locations designed for BPO operations.  For more information about each location, please click on each name.

                    </p>
                </div>
                
                <!-- end of lighbox 1 -->
                
                <div class="bottomContainer">
				
				<!-- start of ads placement -->
				<div style="width:100%;margin:auto;text-align:center">
				
				<?php echo (show_ads('home','mid')); ?>
				
				</div>
				
				<!--end ads placement-->
				
                	<div class="bottomLeft">
                    	<div class="homeArticleContainer">
                        	<h2>What is <span class="colorGreen">BPO Select</span></h2>
                            <p>BPO Select is the first and only online directory of business process outsourcing companies that are based in the Philippines. It provides instant access to company profiles, prices comparisons, service packages and other relevant outsourcing information. </p>
                            <p style="text-align:right;"><a href="/about-us/" >Click here to know more</a></p>
                        </div>
                        <div class="homeArticleContainer">
                        	<h2>How does <span class="colorGreen">BPO Select</span> work?</h2>
                            <p>BPO Select works with hundreds of business process outsourcing service providers to deliver the most comprehensive online directory. </p>
                            <ol class="homepage1">
							<li>Start by using the Quick Search tab.</li>
							<li>Expand the results by using the Advanced Search tab.</li>
							<li>Click the name of your chosen BPO to see their profile.</li>
							<li>Contact your chosen BPO directly.</li>
							</ol>
							
							<p style="text-align:right;clear:both"><a href="" >Click here to know more</a></p>
                        </div>
                        <div class="homeArticleContainer">
                       		<h2> <span class="colorGreen">Outsourcing Information</span> </h2>
                            
                            <p>Be at the forefront of all that's happenings around the business process outsourcing industry. Explore this page for fresh news and articles, blogs, and reports gathered to provide you with information to help you in your BPO search.</p>
                            <p style="text-align:right;"><a href="#" >Click here to know more</a></p>
                        </div>
                    </div>
                    <div class="bottomRight">
					<!--<form action="search/public/" method="get">-->
                    	<div class="homeSideBarContainer">
						<input type="hidden" name="searchtype" value="comploc"/>
                        	<h2>Check BPO locations in The Philippines</h2>
                            <img src="http://bposelect.com/wp-content/themes/bposelect/images/checkbpolocation.png" alt="Check bpo location"/>
                            <p>Location is a major factor to consider when trying to start an offshore business or delivery centre. Explore your location options from the list below. </p>
                            <?php
							$location = $wpdb->get_results("select id,name from location where isparent = 0 order by name ASC", ARRAY_A);
							
							?>
							<select name="location" id="quickloc">
                            	<?php foreach($location as $loc) { ?>
								<optgroup label="<?=$loc['name'];?>">
									<?php
										$locCity = $wpdb->get_results("select id,name from location where isparent =" . $loc['id'] . " order by name ASC", ARRAY_A);
										foreach($locCity as $city){
									?>
									<option value="<?php echo $city['id'];?>"><?php echo $city['name'];?></option>
									<?php
									}
									?>
									
									
								</optgroup>
                                <?php } ?>
                            </select>
                            <button onclick="window.location.href='search/public/location/' + $('#quickloc').val()">Locate Area</button>
                        </div>
						<!--</form>-->
						<form id="searchcompanyname" action="search/public/" method="post" autocomplete="off" >
                        <div class="homeSideBarContainer">
						
                        	<h2>Got a BPO company in mind?</h2>
                            <img src="http://bposelect.com/wp-content/themes/bposelect/images/gotbpoinmind.png" alt="Check bpo location"/>
                            <p>The Philippines is home to many business process outsourcing firms. Check out the profile of your preferred BPO by typing its business name in the field below. </p>
                            <input type="hidden" name="searchtype" value="compname"/>
							<input name="companyname" type="text" placeholder="Enter the company name" id="companyname" auto/>
                            <button>Submit</button>
                        </div>
						</form>
                    </div>
					<!-- start of ads placement -->
					
				<div style="width:100%;margin:auto;text-align:center;clear:both;">
				<?php echo (show_ads('home','bottom')); ?>
				</div>
				
				<!--end ads placement-->
                </div>
            </div>
            
           
           
            
		</div><!-- #content -->
	</div><!-- #primary -->
<script src='http://s.codepen.io/assets/libs/prefixfree.min.js'></script>
<script>
$(".dropdown dt a").on('click', function () {
          $(".dropdown dd ul").slideToggle('fast');
      });

      $(".dropdown dd ul li a").on('click', function () {
          $(".dropdown dd ul").hide();
      });

	  $("#dropdowncost").on('click', function () {
          $(".dropdown dd ul").hide();
      });
	  
      function getSelectedValue(id) {
			//alert('yes');
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
      });

      $('.mutliSelect input[type="checkbox"]').on('click', function () {
        
          //var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
           //   title = $(this).val() + ",";
		   var title = $('#' + this.id).next('label').text() + "," ;
        
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel').append(html);
              $(".hida").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida");
              $('.dropdown dt a').append(ret);
              
          }
      });
</script>

<script>
$(".dropdown2 dt a").on('click', function () {
          $(".dropdown2 dd ul").slideToggle('fast');
      });

      $(".dropdown2 dd ul li a").on('click', function () {
          $(".dropdown2 dd ul").hide();
      });

      function getSelectedValue(id) {
	  
           return $("#" + id).find("dt a span.value").html();
      }

      $(document).bind('click', function (e) {
          var $clicked = $(e.target);
          if (!$clicked.parents().hasClass("dropdown2")) $(".dropdown2 dd ul").hide();
      });


      $('.mutliSelect2 input[type="checkbox"]').on('click', function () {
        
          //var title = $(this).closest('.mutliSelect2').find('input[type="checkbox"]').val(),
           //   title = $(this).val() + ",";
        var title = $('#' + this.id).next('label').text() + "," ;
		
          if ($(this).is(':checked')) {
              var html = '<span title="' + title + '">' + title + '</span>';
              $('.multiSel2').append(html);
              $(".hida").hide();
          } 
          else {
              $('span[title="' + title + '"]').remove();
              var ret = $(".hida");
              $('.dropdown2 dt a').append(ret);
              
          }
      });
</script>
<script language="javascript">
	function togglechkall( id, pID ){
		$('#'+pID).find(':checkbox').each(function(){
		jQuery(this).prop('checked', $('#' + id).is(':checked'));
			});     
	}
	$(".qsbudget").change(function(){ 
	
		var flag = $(".qsbudget").index(this);
		var inputs = $(".qsbudget");
		$(".qsbudget").slice(0,flag).prop("checked",inputs[flag].checked);
		
	});
		
</script>

<script>
$(document).ready(function(){
	
	$("#advancedsearch").validate({
		rules: {
			fullmanaged: 'required',
			'service[]': 'required',
			'seatmodel[]': 'required',
			'priceadv[]': 'required',
			'advloc[]': 'required'
		},
		messages: {
			'fullmanaged': "Please select an option",
			'service[]': "Please select at least 1 service",
			'seatmodel[]': "Please select at least 1 seat model",
			'priceadv[]': "Please select your budget",
			'advloc[]': "Please select at least 1 location"
		}
	});
	
});

</script>


<?php get_footer(); ?>