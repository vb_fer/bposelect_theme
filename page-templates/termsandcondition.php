<?php
/**
 * Template Name: terms and conditions
 *
 * Author Bryan Durana
 * www.bposelect.com
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

 
 

get_header(); ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/paging-style.css"/>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.paginate.js"></script>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<!-- .entry-header -->

					<div class="entry-content">
                    	<div class="innerPageHeader">
                        </div>
						<div class="innerPageContainer">
							<div class="contentContainer">
                             <div class="innerLeft">
                            
							
								<?php the_content(); ?>
	
                             
						
							 </div>
							 <div class="innerRight">
							 <?php get_template_part( 'default', 'right_sidebar' ); ?>
							<div class="widget-area">
							 <?php get_template_part( 'top_companies', 'sidebar' ); ?>
							</div>
							 </div>
                         	</div>
                        </div>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

				
			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

	
	<script type="text/javascript">
		$(function() {
			$("#termspage").paginate({
				count 		: 10,
				start 		: 1,
				display     : 10,
				border					: true,
				border_color			: '#fff',
				text_color  			: '#797979',
				background_color    	: '#fff',	
				border_hover_color		: '#797979',
				text_hover_color  		: '#fff',
				background_hover_color	: '#797979', 
				images					: false,
				mouse					: 'press',
				onChange     			: function(page){
											$('._current','#termscontainer').removeClass('_current').hide();
											$('#p'+page).addClass('_current').show();
										  }
			});
		});
		</script>
	
<?php get_footer(); ?>