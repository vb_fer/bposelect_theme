<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="entry-content">
                    	<div class="innerPageHeader">
                        	<div class="innerPageText">
                            	<h2>REVOLUTIONISING YOUR TOTAL BUSINESS OPERATIONS</h2>
                                <p>Briston Outsourcing Service Solutions (BOSS) revolutionises the way your business operates by injecting highly efficient and productive work flow processes and systems to ensure that your company is meeting the demands of getting your way up towards success.</p>
                            </div>
                        </div>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<?php if ( have_posts() ) : ?>

			

			<div class="innerPageContainer">
                <div class="searchContainer"> 
                    
                         
                          <h3>Your Search on "<?php printf( __( '%s', 'twentythirteen' ), get_search_query() ); ?>"  Returned "<?php $search_count = 0; $search = new WP_Query("s=$s & showposts=-1"); if($search->have_posts()) : while($search->have_posts()) : $search->the_post(); $search_count++; endwhile; endif; echo $search_count;?>" Results </h3>  
                        <?php if ( have_posts() ) : ?>
                            <?php /* The loop */ ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php //get_template_part( 'content', get_post_format() ); ?>
                                <h2><a href="<?php the_permalink() ?>"><?php the_title()?></a></h2>
                                <?php the_excerpt()?>
                                
                                <br/>
                            <?php endwhile; ?>
                            
                            
                
                            <?php twentythirteen_paging_nav(); ?>
                
                        <?php else : ?>
                            <?php get_template_part( 'content', 'none' ); ?>
                        <?php endif; ?>
                        
                </div>        
			</div>

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>

			<?php twentythirteen_paging_nav(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>