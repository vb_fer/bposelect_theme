(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1099,
	height: 501,
	fps: 30,
	color: "#FFFFFF",
	manifest: [
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/boss.png", id:"boss"},
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/cloud1.png", id:"cloud1"},
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/cloud2.png", id:"cloud2"},
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/cloud3.png", id:"cloud3"},
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/cloud4.png", id:"cloud4"},
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/cloud5.png", id:"cloud5"},
		{src:"http://dev.bristonoutsourcing.com/wp-content/themes/boss-wp/images/sliderBackground.jpg", id:"sliderBackground"}
	]
};

// stage content:
(lib.mainBanner = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_11 = function() {
		/* Click to Go to Web Page
		Clicking on the specified symbol instance loads the URL in a new browser window.
		
		Instructions:
		1. Replace http://www.adobe.com with the desired URL address.
		   Keep the quotation marks ("").
		*/
		
		btn1.addEventListener("click", fl_ClickToGoToWebPage_4);
		
		function fl_ClickToGoToWebPage_4() {
			window.open("http://www.briston.com.au", "_blank");
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(11).call(this.frame_11).wait(914));

	// Layer 33
	this.instance = new lib.mc30();
	this.instance.setTransform(177.8,290,1,1,49.4);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(852).to({_off:false},0).to({rotation:-28.4,x:250.5,y:406,alpha:1},6).to({rotation:0,x:209.5,y:377},3).wait(53).to({alpha:0},5).to({_off:true},1).wait(5));

	// Layer 32
	this.instance_1 = new lib.mc31();
	this.instance_1.setTransform(189,-167);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(844).to({_off:false},0).to({y:153,alpha:1},5).to({y:103},3).wait(67).to({alpha:0},5).wait(1));

	// Layer 30
	this.instance_2 = new lib.mc29();
	this.instance_2.setTransform(912.2,346,1,1,-42.7);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(787).to({_off:false},0).to({rotation:30.4,x:796,y:415,alpha:1},5).to({rotation:0,x:823,y:408},3).wait(69).to({alpha:0},5).to({_off:true},1).wait(55));

	// Layer 29
	this.instance_3 = new lib.mc28();
	this.instance_3.setTransform(913,253.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(775).to({_off:false},0).to({alpha:1},5).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(83).to({alpha:0},5).to({_off:true},1).wait(50));

	// Layer 28
	this.instance_4 = new lib.mc27();
	this.instance_4.setTransform(788,64,1,1,-32.4);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(765).to({_off:false},0).to({rotation:11.4,x:857,y:127,alpha:1},6).to({rotation:0,x:843,y:108},4).wait(99).to({alpha:0},5).to({_off:true},1).wait(45));

	// Layer 27
	this.instance_5 = new lib.mc26();
	this.instance_5.setTransform(279,497,1,1,-43.7);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(686).to({_off:false},0).to({rotation:17.5,x:127.4,y:328.9,alpha:1},5).to({rotation:0,x:151.9,y:375.1},3).wait(109).to({alpha:0},5).to({_off:true},1).wait(116));

	// Layer 26
	this.instance_6 = new lib.mc25();
	this.instance_6.setTransform(289,46,1,1,26.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(678).to({_off:false},0).to({rotation:-16.7,x:164,y:169.1,alpha:1},5).to({rotation:0,x:188.6,y:130.1},3).wait(122).to({alpha:0},5).to({_off:true},1).wait(111));

	// Layer 25
	this.instance_7 = new lib.mc24();
	this.instance_7.setTransform(744.9,427.5);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(627).to({_off:false},0).to({alpha:1},5).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(96).to({alpha:0},5).to({_off:true},1).wait(185));

	// Layer 24
	this.instance_8 = new lib.mc23();
	this.instance_8.setTransform(1010.2,339.2,1,1,-30);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(619).to({_off:false},0).to({rotation:17.5,x:909.8,y:283.8,alpha:1},5).to({rotation:0,x:917,y:291},3).wait(112).to({alpha:0},5).to({_off:true},1).wait(180));

	// Layer 23
	this.instance_9 = new lib.mc22();
	this.instance_9.setTransform(804.4,-154.4);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(609).to({_off:false},0).to({y:95.6},5).to({y:65.6},3).wait(127).to({alpha:0},5).to({_off:true},1).wait(175));

	// Layer 22
	this.instance_10 = new lib.mc21();
	this.instance_10.setTransform(386.3,465.9,1,1,-38);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(504).to({_off:false},0).to({rotation:16.2,x:282.8,y:406.4,alpha:1},5).to({rotation:0,x:303.3,y:411.5},3).wait(117).to({alpha:0},5).to({_off:true},1).wait(290));

	// Layer 21
	this.instance_11 = new lib.mc20();
	this.instance_11.setTransform(148.5,269.9);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(495).to({_off:false},0).to({alpha:1},4).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(129).to({alpha:0},5).to({_off:true},1).wait(285));

	// Layer 20
	this.instance_12 = new lib.mc19();
	this.instance_12.setTransform(241.9,207.2,1,1,-26.9);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(484).to({_off:false},0).to({rotation:8.4,x:175.2,y:71,alpha:1},7).to({rotation:0,x:177.3,y:105.8},4).wait(144).to({alpha:0},5).to({_off:true},1).wait(280));

	// Layer 19
	this.instance_13 = new lib.mc18();
	this.instance_13.setTransform(769.2,419.1);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(404).to({_off:false},0).to({alpha:1},5).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(159).to({alpha:0},5).to({_off:true},1).wait(345));

	// Layer 18
	this.instance_14 = new lib.mc17();
	this.instance_14.setTransform(1046.3,264.6,1,1,-46.5);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(395).to({_off:false},0).to({rotation:24.4,x:878.3,y:306.6,alpha:1},6).to({rotation:0,x:931.3,y:298.6},3).wait(175).to({alpha:0},5).to({_off:true},1).wait(340));

	// Layer 17
	this.instance_15 = new lib.mc16();
	this.instance_15.setTransform(827.3,-164.9);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(387).to({_off:false},0).to({y:95.1},5).to({y:65.1},3).wait(189).to({alpha:0},5).to({_off:true},1).wait(335));

	// Layer 16
	this.instance_16 = new lib.mc13();
	this.instance_16.setTransform(279.8,499.5,1,1,-26);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(293).to({_off:false},0).to({rotation:8.2,x:179.1,y:379.1,alpha:1},6).to({rotation:0,x:194.1,y:400.1},3).wait(147).to({alpha:0},5).to({_off:true},1).wait(470));

	// Layer 15
	this.instance_17 = new lib.mc15();
	this.instance_17.setTransform(148,-160);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(285).to({_off:false},0).to({y:156,alpha:1},5).to({y:140},3).wait(161).to({alpha:0},5).to({_off:true},1).wait(465));

	// Layer 14
	this.instance_18 = new lib.mc14();
	this.instance_18.setTransform(239.1,158.5,1,1,-25.2);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(277).to({_off:false},0).to({rotation:25.7,x:350.1,y:89.5,alpha:1},5).to({rotation:0,x:293.1,y:91.5},3).wait(174).to({alpha:0},5).to({_off:true},1).wait(460));

	// Layer 10
	this.instance_19 = new lib.mc9();
	this.instance_19.setTransform(768.9,420);
	this.instance_19.alpha = 0;
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(199).to({_off:false},0).to({alpha:1},5).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(157).to({alpha:0},5).to({_off:true},1).wait(552));

	// Layer 9
	this.instance_20 = new lib.mc8();
	this.instance_20.setTransform(925,139.5);
	this.instance_20.alpha = 0;
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(192).to({_off:false},0).to({y:319.5,alpha:1},4).to({y:279.5},3).wait(173).to({alpha:0},5).to({_off:true},1).wait(547));

	// Layer 2
	this.instance_21 = new lib.mc7();
	this.instance_21.setTransform(812,43,1,1,-13.7);
	this.instance_21.alpha = 0;
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(187).to({_off:false},0).to({rotation:12.4,x:899,y:129,alpha:1},5).to({rotation:0,x:886,y:105},3).wait(181).to({alpha:0},5).to({_off:true},1).wait(543));

	// Layer 13
	this.instance_22 = new lib.mc12();
	this.instance_22.setTransform(296.6,486,0.991,0.853,-32.5);
	this.instance_22.alpha = 0;
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(172).to({_off:false},0).to({rotation:11.5,x:162.5,y:378.5,alpha:1},6).to({rotation:0,x:189.6,y:413.5},3).wait(86).to({alpha:0},5).to({_off:true},1).wait(667));

	// Layer 12
	this.instance_23 = new lib.mc11();
	this.instance_23.setTransform(31,224.9,1,1,47.4);
	this.instance_23.alpha = 0;
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(158).to({_off:false},0).to({rotation:-13.9,x:180.5,y:263.9,alpha:1},7).to({rotation:0,x:150},3).wait(97).to({alpha:0},5).to({_off:true},1).wait(662));

	// Layer 11
	this.instance_24 = new lib.mc10();
	this.instance_24.setTransform(311,-51.1,1,1,23.2);
	this.instance_24.alpha = 0;
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(142).to({_off:false},0).to({rotation:-10.2,x:213,y:140.9,alpha:1},5).to({rotation:0,x:211,y:105.9},3).wait(112).to({alpha:0},5).to({_off:true},1).wait(657));

	// Layer 4
	this.instance_25 = new lib.mc6();
	this.instance_25.setTransform(214,-177);
	this.instance_25.alpha = 0;
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(44).to({_off:false},0).to({y:49,alpha:1},5).to({y:23},2).wait(74).to({y:43},3).to({y:-157,alpha:0},4).to({_off:true},1).wait(792));

	// Layer 8
	this.instance_26 = new lib.mc5();
	this.instance_26.setTransform(142,254);
	this.instance_26.alpha = 0;
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(39).to({_off:false},0).to({alpha:1},5).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(75).to({alpha:0},5).to({_off:true},1).wait(794));

	// Layer 7
	this.instance_27 = new lib.mc4();
	this.instance_27.setTransform(252,443.5,1,1,-23);
	this.instance_27.alpha = 0;
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(34).to({_off:false},0).to({rotation:7.7,x:209.5,y:375.5,alpha:1},5).to({rotation:0,x:214,y:401.5},2).wait(81).to({alpha:0},5).to({_off:true},1).wait(797));

	// Layer 6
	this.instance_28 = new lib.mc3();
	this.instance_28.setTransform(844,453.5,1,1,-15.4);
	this.instance_28.alpha = 0;
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(16).to({_off:false},0).to({rotation:8,x:773,y:407.5,alpha:1},5).to({rotation:0,x:784,y:413.5},2).wait(138).to({alpha:0},6).to({_off:true},1).wait(757));

	// Layer 5
	this.instance_29 = new lib.mc2();
	this.instance_29.setTransform(934.5,269);
	this.instance_29.alpha = 0;
	this.instance_29._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(11).to({_off:false},0).to({alpha:1},5).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0},0).wait(1).to({alpha:1},0).wait(136).to({alpha:0},6).to({_off:true},1).wait(760));

	// Layer 3
	this.instance_30 = new lib.mc1();
	this.instance_30.setTransform(795.9,51,1,1,-39.2);
	this.instance_30.alpha = 0;
	this.instance_30._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(4).to({_off:false},0).to({rotation:6.5,x:903.4,y:135,alpha:1},5).to({rotation:0,x:892.9,y:103},2).wait(143).to({alpha:0},6).to({_off:true},1).wait(764));

	// Layer 1
	this.instance_31 = new lib.shineMc();
	this.instance_31.setTransform(523.5,248.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(925));

	// background
	this.instance_32 = new lib.boss();
	this.instance_32.setTransform(289.5,100);

	this.instance_33 = new lib.sliderBackground();

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_33},{t:this.instance_32}]}).wait(925));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(528.4,250.5,1120.2,501);


// symbols:
(lib.boss = function() {
	this.initialize(img.boss);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,467,296);


(lib.cloud1 = function() {
	this.initialize(img.cloud1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,314,196);


(lib.cloud2 = function() {
	this.initialize(img.cloud2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,247,314);


(lib.cloud3 = function() {
	this.initialize(img.cloud3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,258,126);


(lib.cloud4 = function() {
	this.initialize(img.cloud4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,260,169);


(lib.cloud5 = function() {
	this.initialize(img.cloud5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,321,147);


(lib.sliderBackground = function() {
	this.initialize(img.sliderBackground);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1099,501);


(lib.shineMc = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 4 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_69 = new cjs.Graphics().p("Az+KJIKl0RILrAAIqiURg");
	var mask_graphics_70 = new cjs.Graphics().p("Ax8KJIKl0RILrAAIqiURg");
	var mask_graphics_71 = new cjs.Graphics().p("Av6KJIKl0RILrAAIqiURg");
	var mask_graphics_72 = new cjs.Graphics().p("At4KJIKl0RILrAAIqiURg");
	var mask_graphics_73 = new cjs.Graphics().p("Ar2KJIKl0RILrAAIqiURg");
	var mask_graphics_74 = new cjs.Graphics().p("ArIKJIKl0RILsAAIqlURg");
	var mask_graphics_75 = new cjs.Graphics().p("ArIKJIKl0RILsAAIqlURg");
	var mask_graphics_76 = new cjs.Graphics().p("ArIKJIKl0RILsAAIqlURg");
	var mask_graphics_77 = new cjs.Graphics().p("ArIKJIKl0RILsAAIqlURg");
	var mask_graphics_78 = new cjs.Graphics().p("ArIKJIKl0RILsAAIqlURg");
	var mask_graphics_79 = new cjs.Graphics().p("AqzKJIKl0RILrAAIqkURg");
	var mask_graphics_80 = new cjs.Graphics().p("AoxKJIKj0RILtAAIqkURg");
	var mask_graphics_81 = new cjs.Graphics().p("AmvKJIKj0RILtAAIqkURg");
	var mask_graphics_82 = new cjs.Graphics().p("AktKJIKj0RILtAAIqkURg");
	var mask_graphics_83 = new cjs.Graphics().p("AirKJIKj0RILtAAIqkURg");
	var mask_graphics_84 = new cjs.Graphics().p("AgpKJIKj0RILtAAIqkURg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(69).to({graphics:mask_graphics_69,x:-127.9,y:7.3}).wait(1).to({graphics:mask_graphics_70,x:-114.9,y:7.3}).wait(1).to({graphics:mask_graphics_71,x:-101.9,y:7.3}).wait(1).to({graphics:mask_graphics_72,x:-88.9,y:7.3}).wait(1).to({graphics:mask_graphics_73,x:-75.9,y:7.3}).wait(1).to({graphics:mask_graphics_74,x:-54.5,y:7.3}).wait(1).to({graphics:mask_graphics_75,x:-28.5,y:7.3}).wait(1).to({graphics:mask_graphics_76,x:-2.5,y:7.3}).wait(1).to({graphics:mask_graphics_77,x:23.5,y:7.3}).wait(1).to({graphics:mask_graphics_78,x:49.5,y:7.3}).wait(1).to({graphics:mask_graphics_79,x:73.4,y:7.3}).wait(1).to({graphics:mask_graphics_80,x:86.4,y:7.3}).wait(1).to({graphics:mask_graphics_81,x:99.4,y:7.3}).wait(1).to({graphics:mask_graphics_82,x:112.4,y:7.3}).wait(1).to({graphics:mask_graphics_83,x:125.4,y:7.3}).wait(1).to({graphics:mask_graphics_84,x:138.4,y:7.3}).wait(40));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FFFFFF").ss(1,1,1).p("AwnFBQg/g6gshJQhaiSBahLQATgOAdgRQA6ghA0gMQA0gLDCgLIC3gIQARgCAPglQAPgmgbAAQjxgDABgCQABAAgRA5QiXAJAAgBIA0icQAAgBGlgBQAbAAAaAIQA1AQgCAjQAAABAAAlQAAAUgFANQgNAkhHApQAJAEAKAIQATAQAAAYQAAAsgJAfQgOAyggAPQgRAIgSALQgHAEgYAHQgaAIjngCQjigBAAgCIAehnQBtgnAoAEQgBABgBARQgBAIAAAKIDlANQAUgBAQgLQAfgUgUgzQAAgDg4gBQgbAAgcABIiZACQiiANgxAyQgSAHgNAKQgeAUgOAdQgrBdB+CYgAjykoQABAAg0AhQg1ApgNAlQgQAvABBCQABBGAUAcQAiAyA/ApQBWA4BCgcQBSgiAyg4QA5g8gFhAQgJhug2g/QhShiixAsgAKJk1QABgBmAAGQgaAIgdAQQg5AggMAsQgTBHAIAhQANAyBIgBQDegDABgBQABAAAVAAQATAHgKAeQAAAJgFAJQgKARgaABQgpABj7gGIheBsQGCABAAgBQAhgIAjgRQBFgkAJgxQAOhKgHgXQgLgphDgBQhCAAhggCQhYgCABAAQgMgOgDgPQgHgdAqgEQBEgGDTAEQBfhuAAgDgAS7k/QgkAAlJgBQgcAEggAQQhAAegOA0QgUBGAIAfQAJAnA1ADQBMAFC3AEQAAAAAPAJQAMANgMAVQgPAYgEACQgGADgrgBQkFgGAAAAIhiBqQGaAAABAAQAAAAA8gfQA9goAGgoQAKhFgIgZQgNgshBgCQg7gChbABQhNABAAAAQgRAAgLgGQgWgLAegfQgCgJACgDQAGgKAdAAQAvABDbAEgATAk/QAAAAgFAA");
	this.shape.setTransform(-18.2,11.1);
	this.shape._off = true;

	this.shape.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(69).to({_off:false},0).wait(55));

	// Layer 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_0 = new cjs.Graphics().p("EgqiAZsMAhCgzXIVRAAMgg/AzXg");
	var mask_1_graphics_1 = new cjs.Graphics().p("EgmSAZ7MAhUgz1IVSAAMghTAz1g");
	var mask_1_graphics_2 = new cjs.Graphics().p("EgiDAaKMAhog0TIVRAAMghlA0Tg");
	var mask_1_graphics_3 = new cjs.Graphics().p("A90aZMAh6g0xIVTAAMgh5A0xg");
	var mask_1_graphics_4 = new cjs.Graphics().p("A7vaoMAiMg1PIVTAAMgiMA1Pg");
	var mask_1_graphics_5 = new cjs.Graphics().p("A75a3MAigg1tIVTAAMgigA1tg");
	var mask_1_graphics_6 = new cjs.Graphics().p("A8DbGMAizg2LIVUAAMgizA2Lg");
	var mask_1_graphics_7 = new cjs.Graphics().p("A8MbVMAjGg2pIVTAAMgjGA2pg");
	var mask_1_graphics_8 = new cjs.Graphics().p("A8WbkMAjZg3HIVUAAMgjZA3Hg");
	var mask_1_graphics_9 = new cjs.Graphics().p("A8gbzMAjtg3lIVUAAMgjtA3lg");
	var mask_1_graphics_10 = new cjs.Graphics().p("A8pcCMAkAg4DIVTAAMgkAA4Dg");
	var mask_1_graphics_11 = new cjs.Graphics().p("A4scRMAkTg4hIVTAAMgkTA4hg");
	var mask_1_graphics_12 = new cjs.Graphics().p("A0mcgMAkmg4/IVTAAMgkoA4/g");
	var mask_1_graphics_13 = new cjs.Graphics().p("AwhcvMAk6g5dIVTAAMgk7A5dg");
	var mask_1_graphics_14 = new cjs.Graphics().p("Asbc+MAlNg57IVTAAMglOA57g");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:mask_1_graphics_0,x:-272.3,y:-26.6}).wait(1).to({graphics:mask_1_graphics_1,x:-245.2,y:-26.6}).wait(1).to({graphics:mask_1_graphics_2,x:-218,y:-26.6}).wait(1).to({graphics:mask_1_graphics_3,x:-190.9,y:-26.6}).wait(1).to({graphics:mask_1_graphics_4,x:-149.8,y:-26.6}).wait(1).to({graphics:mask_1_graphics_5,x:-94.6,y:-26.6}).wait(1).to({graphics:mask_1_graphics_6,x:-39.3,y:-26.6}).wait(1).to({graphics:mask_1_graphics_7,x:15.9,y:-26.6}).wait(1).to({graphics:mask_1_graphics_8,x:71.2,y:-26.6}).wait(1).to({graphics:mask_1_graphics_9,x:126.4,y:-26.6}).wait(1).to({graphics:mask_1_graphics_10,x:181.7,y:-26.6}).wait(1).to({graphics:mask_1_graphics_11,x:210.7,y:-26.6}).wait(1).to({graphics:mask_1_graphics_12,x:238.8,y:-26.6}).wait(1).to({graphics:mask_1_graphics_13,x:266.9,y:-26.6}).wait(1).to({graphics:mask_1_graphics_14,x:295,y:-26.6}).wait(110));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#FFFFFF").ss(3,1,1).p("Egh2AIHIA9A+QBBA+AVAEQA5BTADADQA1BBBGAtQDQCGFngjQAAgCAfAoQAtAvBBAhIARAmQAYAtAjAcQBcBRBuAqQDHBKDNhLQA8gWBNgyQBLgxBGg/IALAZQAQAeAUAcQBBBZBWAjQARALBPAdQBkAlBlAZQEvBLCWhJQAugSBMgaQA/gXApgaQBrhEB9jEQACgBBDAOQBSAOBLgEQDxgLBri2QgBgCA4gFQBHgMBAgjQDRhyBPlAQAXiDgVhXQgpiti+iUQgehEhPhIQieiPkIgcQgVg/gqhOQhUiahwhPQgSgeghgpQhEhThXg5QkZi3mKCHQhDhXhphWQjTitjDgLQhmgNhCgFQh6gIhkAVQkmA9jPFHQgCACgkBtQgRA3gRA2Qhogah7AZQj1AyhcEAQhhgMh4AvQjxBdhwEjQgfCnAIBkQAQC7B6Ctg");

	this.shape_1.mask = mask_1;

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(124));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-232.1,-147.1,35.2,285);


(lib.mc31 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("AKwCSIgCAAQgJgBgIgFQgHgHAAgPIAAhCQAAgMAIgIQAHgIAOAAQALAAAJAIQAHAIABANIAAAHIgSAAIAAgGQAAgFgDgEQgDgEgFAAQgGAAgCAEQgCAFgBAFIAAA+QAAAFADAEQACADAGAAIACAAIABAAQADgBADgCQACgDAAgGIAAgGIASAAIAAAIQgBALgHAIQgJAJgLAAIgDgBgADHCTIAAAAIgBgBQgLAAgIgIQgIgGAAgPIAAhAQAAgNAIgIQAIgHALgBIABAAIAAAAIACAAIABAAQAKABAIAHQAJAIAAANIAABAQAAAPgJAGQgIAIgKAAIgBABIgCAAgAC/AsQgDADAAAGIAABAQAAAHADACQAEADAEAAQAFAAAEgDQADgCAAgHIAAhAQAAgGgDgDQgEgDgFAAQgEAAgEADgAgQCKQgJgHAAgNIAAgGIASAAIAAAFQAAAFACAEQADADAFAAQAIABACgFQADgFAAgJQAAgIgCgDQgBgEgFgBIgBgBIgBgBIgLgFQgLgEgEgIQgEgIAAgLQAAgNAIgKQAHgKANAAQAMAAAJAIQAHAIABAKIAAABIAAABIAAAIIgRAAIAAgEQAAgGgEgFQgCgEgHAAQgFAAgDAFQgBAFgBAGQABAGABAEQABADAEACIABABIAAAAIAOAGQALAEADAHQADAJABAMQAAAQgHAKQgGAKgRAAQgLAAgIgJgAkuCTIgBAAIgBgBQgKAAgIgIQgJgGAAgPIAAhAQAAgNAJgIQAIgHAKgBIABAAIABAAIABAAIABAAQAKABAJAHQAJAIgBANIAAAHIgRAAIAAgHQAAgGgEgDQgDgDgFAAQgFAAgDADQgDADAAAGIAABAQAAAHADACQADADAFAAQAFAAADgDQAEgCAAgHIAAgWIgOAAIAAgPIAfAAIAAAlQABAPgJAGQgJAIgKAAIgBABIgBAAgAl3CTIgBAAIAAgBQgLAAgIgIQgIgGAAgPIAAhAQAAgNAIgIQAIgHALgBIAAAAIABAAIACAAIABAAQAKABAIAHQAJAIAAANIAABAQAAAPgJAGQgIAIgKAAIgBABIgCAAgAl/AsQgDADAAAGIAABAQAAAHADACQAEADAEAAQAFAAAEgDQADgCAAgHIAAhAQAAgGgDgDQgEgDgFAAQgEAAgEADgApsCTIgBAAIgBgBQgKAAgIgIQgJgGAAgPIAAhAQAAgNAJgIQAIgHAKgBIABAAIABAAIABAAIABAAQALABAIAHQAIAIAAANIAAAHIgRAAIAAgHQAAgGgDgDQgEgDgFAAQgEAAgEADQgDADAAAGIAABAQAAAHADACQAEADAEAAQAFAAAEgDQADgCAAgHIAAgWIgOAAIAAgPIAfAAIAAAlQAAAPgIAGQgIAIgLAAIgBABIgBAAgAQLCSIAAgzIgZhGIATAAIAOAwIAOgwIASAAIgXBGIAAAzgAO7CSIAAh5IASAAIAABnIAiAAIAAASgAN8CSIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAjIAjAAIAAASgANKCSIgZh5IATAAIANBUIAPhUIASAAIgZB5gAMVCSIAAh5IASAAIAAB5gALoCSIAAhpIgVAAIAAgQIA6AAIAAAQIgUAAIAABpgAJYCSIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAIZCSIAAh5IA0AAIAAAQIgiAAIAAAlIAeAAIAAARIgeAAIAAAzgAHaCSIAAh5IA0AAIAAAQIgiAAIAAAlIAeAAIAAARIgeAAIAAAzgAGbCSIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAE8CSIAAh5IA1AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAjIAjAAIAAASgAEgCSIgQgzIgLAAIAAAzIgRAAIAAh5IAcAAQAQAAAHAJQAIAJAAASQgBAKgDAIQgDAHgIAFIASA3gAEFBPIAKAAQAKAAACgFQADgFAAgJQAAgJgEgFQgCgFgKAAIgJAAgACKCSIAAhKIgBAAIgRA1IgIAAIgRg1IgBAAIAABKIgRAAIAAh5IARAAIAWBAIABAAIAVhAIARAAIAAB5gAg3CSIAAhKIAAAAIgRA1IgJAAIgRg1IAAAAIAABKIgSAAIAAh5IARAAIAWBAIAAAAIAXhAIARAAIAAB5gAiOCSIgFgaIgXAAIgGAaIgRAAIAbh5IAPAAIAbB5gAinBoIARAAIgJgsgAjWCSIgQgzIgKAAIAAAzIgSAAIAAh5IAcAAQAQAAAIAJQAHAJAAASQAAAKgDAIQgDAHgJAFIATA3gAjwBPIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgDgFgJAAIgJAAgAmvCSIgQgzIgKAAIAAAzIgSAAIAAh5IAcAAQAQAAAHAJQAIAJgBASQAAAKgDAIQgDAHgIAFIATA3gAnJBPIAJAAQAKAAACgFQADgFAAgJQgBgJgDgFQgCgFgJAAIgJAAgAogCSIAAh5IAaAAQAHAAAGACQAFABAFAGQAEAFACAFQACAHAAALIgCANQgBAHgDAEQgDAGgGAEQgGADgKAAIgJAAIAAAvgAoPBSIAJAAQAKABACgGQACgGAAgJQAAgIgBgHQgDgGgKAAIgJAAgAqoCSIgbhJIAABJIgRAAIAAh5IAQAAIAbBJIAAhJIARAAIAAB5gAr1CSIAAh5IARAAIAAB5gAsWCSIgbhJIAABJIgRAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAtkCSIAAh5IASAAIAAB5gAt+CSIgEgaIgZAAIgEAaIgRAAIAbh5IAOAAIAbB5gAuXBoIARAAIgIgsgAvGCSIgPgzIgLAAIAAAzIgRAAIAAh5IAcAAQAPAAAHAJQAIAJAAASQAAAKgDAIQgDAHgJAFIATA3gAvgBPIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAweCSIAAhpIgVAAIAAgQIA6AAIAAAQIgUAAIAABpgAHDgXIAAAAIgBgBQgLAAgIgIQgIgGAAgOIAAhAQAAgOAIgIQAIgIALAAIABAAIAAAAIACAAIABAAQAKAAAIAIQAJAIAAAOIAABAQAAAOgJAGQgIAIgKAAIgBABIgCAAgAG7h+QgDADAAAHIAABAQAAAGADACQAEADAEAAQAFAAAEgDQADgCAAgGIAAhAQAAgHgDgDQgEgDgFAAQgEAAgEADgAD/gfQgIgIAAgMIAAheIARAAIAABcQAAAHAEACQADADAEAAQAFAAADgDQADgCAAgHIAAhcIARAAIAABeQAAAMgIAIQgIAIgMAAQgLAAgJgIgADKgXIgBAAIgBgBQgKAAgIgIQgJgGAAgOIAAhAQAAgOAJgIQAIgIAKAAIABAAIABAAIACAAIABAAQAKAAAJAIQAIAIAAAOIAABAQAAAOgIAGQgJAIgKAAIgBABIgCAAgADDh+QgEADAAAHIAABAQAAAGAEACQADADAEAAQAGAAADgDQADgCABgGIAAhAQgBgHgDgDQgDgDgGAAQgEAAgDADgAgcgXIgCAAIgBgBQgJAAgJgIQgJgGAAgOIAAhAQAAgOAJgIQAJgIAJAAIABAAIACAAIABAAIABAAQAKAAAJAIQAHAIAAAOIAAAGIgQAAIAAgGQgBgHgDgDQgDgDgFAAQgFAAgDADQgEADAAAHIAABAQAAAGAEACQADADAFAAQAFAAADgDQADgDABgFIAAgYIgPAAIAAgOIAfAAIAAAmQAAAOgHAGQgJAIgKAAIgBABIgBAAgAr5gYIgCAAQgJAAgIgHQgHgGAAgPIAAhCQAAgMAIgIQAHgIANAAQAMAAAJAIQAHAIABANIAAAHIgSAAIAAgGQAAgFgDgEQgDgEgFAAQgGAAgDAEQgCAFAAAFIAAA/QABAEACAEQACAEAGgBIACAAIABAAQADAAACgDQADgDAAgFIAAgHIASAAIAAAIQgBALgHAIQgJAJgMAAIgCgBgAudgfQgJgIAAgMIAAheIASAAIAABcQAAAHACACQADADAFAAQAFAAADgDQADgCAAgHIAAhcIARAAIAABeQAAAMgIAIQgJAIgLAAQgMAAgHgIgAKAgYIgahJIAABJIgSAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAIogYIgNhPIgBAAIgNBPIgQAAIgTh5IATAAIAJBNIANhNIAOAAIAOBOIAAAAIAJhOIASAAIgSB5gAFrgYIgPg0IgLAAIAAA0IgSAAIAAh5IAdAAQAPAAAHAJQAIAJAAASQAAAKgDAIQgEAHgIAFIATA3gAFRhbIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAB9gYIAAgzIgYhGIASAAIAPAwIANgwIATAAIgYBGIAAAzgAAOgYIAAh5IA0AAIAAAQIgjAAIAAAkIAfAAIAAAQIgfAAIAAAjIAjAAIAAASgAhSgYIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gAhrhDIARAAIgJgrgAiegYIgahJIAABJIgSAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAjigYIgGgaIgYAAIgEAaIgRAAIAbh5IAOAAIAbB5gAj8hDIARAAIgIgrgAkwgYIAAhJIAAAAIgRA0IgJAAIgRg0IAAAAIAABJIgSAAIAAh5IARAAIAXBAIAAAAIAWhAIARAAIAAB5gAnUgYIAAh5IAaAAQAOAAAIAJQAHAHABAPIAAA5QAAAQgJAJQgIAIgPAAgAnDgpIAIAAQAHABAEgEQACgEAAgHIAAg7QAAgHgCgEQgEgEgHAAIgIAAgAn1gYIgahJIAABJIgSAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAo6gYIgEgaIgZAAIgEAaIgRAAIAbh5IAOAAIAbB5gApThDIARAAIgIgrgAqmgYIAAg2IgWAAIAAA2IgRAAIAAh5IARAAIAAA0IAWAAIAAg0IARAAIAAB5gAsygYIgahJIAABJIgSAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAvAgYIgEgaIgYAAIgGAaIgRAAIAbh5IAPAAIAbB5gAvZhDIARAAIgJgrgAwqgYIAAh5IARAAIAABnIAjAAIAAASg");
	this.shape.setTransform(1.6,84.6);

	this.instance = new lib.cloud2();
	this.instance.setTransform(-123.5,-157);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-123.5,-157,247,314);


(lib.mc30 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("ACPCmQgKgKAAgOIAAgIIAUAAIAAAHQAAAFAEAFQADAFAIAAQAKgBACgFQACgGAAgKQAAgKgBgEQgCgDgFgDIgCgBIgBAAIgPgHQgNgFgFgJQgEgJAAgNQAAgRAJgLQAIgLASAAQAPAAAJAKQAJAJABALIAAABIAAALIgVAAIAAgEQAAgIgDgEQgDgGgIABQgIAAgDAFQgDAGAAAHQAAAIACADQACAEAGADIABAAIABABIAPAGQAOAFAEAJQAEAKAAAPQAAASgIALQgIAMgTABQgPAAgKgKgAgOCmQgKgKAAgOIAAgIIAVAAIAAAHQAAAFADAFQABAFAIAAQALgBACgFQACgGAAgKQAAgKgCgEQgBgDgGgDIgBgBIgCAAIgMgHQgOgFgEgJQgFgJAAgNQAAgRAJgLQAJgLAPAAQAPAAAJAKQAKAJAAALIAAABIAAALIgUAAIAAgEQAAgIgDgEQgEgGgHABQgJAAgBAFQgCAGAAAHQAAAIABADQABAEAGADIABAAIAAABIAQAGQAOAFADAJQAEAKAAAPQAAASgHALQgIAMgUABQgNAAgKgKgAoKCwIgBAAIgBAAIgCAAQgMgBgKgJQgJgIAAgRIAAhLQAAgRAJgIQAKgJAMgBIACAAIABAAIABAAIABAAQAMABAKAJQAKAIAAARIAAAHIgUAAIAAgHQAAgIgEgDQgFgDgFAAQgGAAgEADQgEADAAAIIAABLQAAAIAEACQAEAEAGABQAFgBAFgEQAEgDAAgHIAAgbIgRAAIAAgSIAlAAIAAAtQAAARgKAIQgKAJgMABIgBAAgAGuCvIAAhXIgVA9IgKAAIgUg9IAAAAIAABXIgVAAIAAiPIAUAAIAaBLIABAAIAZhLIAUAAIAACPgAEcCvIAAiPIA9AAIAAAUIgoAAIAAApIAjAAIAAAUIgjAAIAAApIAoAAIAAAVgADnCvIAAh7IgXAAIAAgUIBDAAIAAAUIgXAAIAAB7gABOCvIAAg9IgchSIAWAAIAQA5IABAAIAQg5IAWAAIgcBSIAAA9gAhuCvIAAh7IgXAAIAAgUIBDAAIAAAUIgXAAIAAB7gAihCvIgfhWIgBAAIAABWIgUAAIAAiPIAUAAIAeBWIABAAIAAhWIAUAAIAACPgAkeCvIAAiPIA9AAIAAAUIgpAAIAAApIAjAAIAAAUIgjAAIAAApIApAAIAAAVgAlGCvIAAhXIgUA9IgKAAIgUg9IgBAAIAABXIgUAAIAAiPIATAAIAbBLIAAAAIAahLIAUAAIAACPgAnXCvIAAiPIA9AAIAAAUIgpAAIAAApIAjAAIAAAUIgjAAIAAApIApAAIAAAVgApLCvIgGgfIgbAAIgGAfIgVAAIAgiPIARAAIAgCPgAppB8IAVAAIgKgzIgBAAgAqkCvIgfhWIAAAAIAABWIgUAAIAAiPIATAAIAfBWIAAAAIAAhWIAVAAIAACPgAr1CvIgGgfIgcAAIgGAfIgUAAIAfiPIARAAIAgCPgAsTB8IAUAAIgKgzIAAAAgAtQCvIAAhXIgUA9IgKAAIgUg9IgBAAIAABXIgUAAIAAiPIATAAIAbBLIAAAAIAahLIAUAAIAACPgABLgeIgCAAIgBAAIgBAAQgMgBgKgIQgKgJAAgQIAAhMQAAgQAKgJQAKgIAMgBIABAAIABAAIACAAIABAAQAMABAKAIQAKAJAAAQIAABMQAAAQgKAJQgKAIgMABIgBAAgABAiXQgEAEAAAHIAABMQAAAHAEADQAEAEAFAAQAGAAAEgEQAEgDAAgHIAAhMQAAgHgEgEQgEgDgGAAQgFAAgEADgAibgnQgKgKAAgOIAAhuIAVAAIAABsQAAAHADAEQAEAEAFAAQAGAAADgEQAEgEAAgHIAAhsIAUAAIAABuQAAAOgJAKQgKAJgOAAQgNAAgKgJgAjXgeIgCAAIgBAAIgBAAQgMgBgKgIQgKgJAAgQIAAhMQAAgQAKgJQAKgIAMgBIABAAIABAAIACAAIABAAQAMABAKAIQAKAJAAAQIAABMQAAAQgKAJQgKAIgMABIgBAAgAjiiXQgEAEAAAHIAABMQAAAHAEADQAEAEAFAAQAGAAAEgEQAEgDAAgHIAAhMQAAgHgEgEQgEgDgGAAQgFAAgEADgAnwgeIgCAAIgBAAIgBAAQgMgBgKgIQgKgJAAgQIAAhMQAAgQAKgJQAKgIAMgBIABAAIABAAIACAAIABAAQAMABAKAIQAKAJAAAQIAABMQAAAQgKAJQgKAIgMABIgBAAgAn7iXQgEAEAAAHIAABMQAAAHAEADQAEAEAFAAQAGAAAEgEQAEgDAAgHIAAhMQAAgHgEgEQgEgDgGAAQgFAAgEADgAOCgfIgTg9IgMAAIAAA9IgUAAIAAiOIAhAAQASAAAJAKQAJALAAAUQAAAMgEAKQgEAIgKAGIAWBBgANjhuIAMAAQAKAAADgGQADgFAAgLQAAgLgDgFQgDgGgLAAIgLAAgAMFgfIAAiOIA9AAIAAATIgpAAIAAAqIAjAAIAAATIgjAAIAAApIApAAIAAAVgALfgfIgfhWIgBAAIAABWIgUAAIAAiOIAUAAIAeBVIABAAIAAhVIAUAAIAACOgAKKgfIgTg9IgMAAIAAA9IgUAAIAAiOIAhAAQASAAAJAKQAJALAAAUQAAAMgEAKQgEAIgKAGIAWBBgAJrhuIAMAAQAKAAADgGQADgFAAgLQAAgLgDgFQgDgGgLAAIgLAAgAI4gfIgGgfIgbAAIgGAfIgVAAIAgiOIARAAIAgCOgAIahRIAVAAIgKg0IgBAAgAG7gfIAAiOIA9AAIAAATIgoAAIAAAqIAjAAIAAATIgjAAIAAApIAoAAIAAAVgAFxgfIAAiOIAVAAIAAB5IAoAAIAAAVgAEmgfIgfhWIAAAAIAABWIgUAAIAAiOIATAAIAfBVIAAAAIAAhVIAVAAIAACOgAC/gfIgQhdIAAAAIgQBdIgTAAIgViOIAWAAIAKBaIAAAAIAQhaIAQAAIAQBcIABAAIAKhcIAVAAIgVCOgAgcgfIgTg9IgMAAIAAA9IgVAAIAAiOIAhAAQASAAAJAKQAJALAAAUQAAAMgEAKQgEAIgJAGIAWBBgAg7huIALAAQALAAADgGQADgFAAgLQAAgLgDgFQgDgGgMAAIgKAAgAk0gfIAAg8IgchSIAVAAIARA4IAAAAIARg4IAVAAIgcBSIAAA8gAm+gfIAAiOIAfAAQAIgBAHADQAGACAFAGQAGAFACAHQACAIAAANQAAAJgCAHQgBAHgDAGQgEAGgIAEQgHAEgLAAIgKAAIAAA4gAmphqIAKAAQAMAAACgGQACgHAAgLQAAgKgCgHQgCgHgMAAIgKAAgApdgfIAAiOIAVAAIAAB5IAoAAIAAAVgAqngfIAAiOIA9AAIAAATIgoAAIAAAqIAjAAIAAATIgjAAIAAApIAoAAIAAAVgArigfIgdiOIAVAAIARBjIAAAAIAQhjIAWAAIgdCOgAtCgfIAAiOIA9AAIAAATIgpAAIAAAqIAjAAIAAATIgjAAIAAApIApAAIAAAVgAuXgfIAAiOIAeAAQARgBAKALQAJAJAAASIAABCQAAAUgKAKQgKAJgSAAgAuDgyIAKAAQAIAAAEgFQADgEAAgJIAAhEQAAgJgDgFQgDgEgJAAIgKAAg");
	this.shape.setTransform(-0.9,3.1);

	this.instance = new lib.cloud4();
	this.instance.setTransform(-130,84.5,1,1,0,180,0);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc29 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("AhkDgQgJgHAAgNIAAgGIASAAIAAAGQAAAEADAEQADAEAGgBQAJABACgFQACgFAAgJQAAgIgBgDQgCgEgFgCIAAAAIgCgBIgNgFQgKgFgEgHQgFgHAAgMQAAgOAIgJQAHgKAQAAQAMAAAIAIQAIAIAAAKIAAAAIAAACIAAAIIgRAAIAAgEQAAgGgDgEQgCgFgIAAQgGAAgDAFQgCAFAAAGQAAAHABADQACADAFADIABAAIABAAIANAGQAMADADAJQADAHAAANQAAAPgHALQgGAKgRAAQgNAAgIgJgAjUDpIgCAAIgBgBQgJAAgJgIQgJgGAAgOIAAhAQAAgPAJgHQAJgIAJAAIABAAIACAAIABAAIABAAQAKAAAJAIQAIAHAAAPIAABAQAAAOgIAGQgJAIgKAAIgBABIgBAAgAjcCCQgEADAAAHIAABAQAAAGAEADQADACAFAAQAFAAADgCQADgDABgGIAAhAQgBgHgDgDQgDgDgFAAQgFAAgDADgAkdDpIgBAAIgBgBQgKAAgJgIQgIgGAAgOIAAhAQAAgPAIgHQAJgIAKAAIABAAIABAAIACAAIABAAQAKAAAIAIQAJAHAAAPIAABAQAAAOgJAGQgIAIgKAAIgBABIgCAAgAklCCQgDADgBAHIAABAQABAGADADQADACAFAAQAFAAADgCQAEgDAAgGIAAhAQAAgHgEgDQgDgDgFAAQgFAAgDADgAm9DpIgBAAIgCgBQgJAAgJgIQgJgGAAgOIAAhAQAAgPAJgHQAJgIAJAAIACAAIABAAIABAAIABAAQAKAAAIAIQAJAHAAAPIAAAFIgSAAIAAgFQAAgHgDgDQgDgDgFAAQgFAAgDADQgEADAAAHIAABAQAAAGAEADQADACAFAAQAFAAADgCQADgEAAgFIAAgYIgOAAIAAgOIAgAAIAAAmQAAAOgJAGQgIAIgKAAIgBABIgBAAgAioDoIAAh5IARAAIAABnIAjAAIAAASgAlmDoIAAhpIgUAAIAAgQIA6AAIAAAQIgVAAIAABpgAn6DoIgahJIAABJIgSAAIAAh5IARAAIAbBJIAAhJIARAAIAAB5gApHDoIAAh5IARAAIAAB5gApnDoIgbhJIAABJIgRAAIAAh5IAQAAIAbBJIAAhJIARAAIAAB5gAqvDoIgRg0IgKAAIAAA0IgRAAIAAh5IAbAAQAQAAAIAJQAHAJAAASQAAAKgDAIQgEAHgIAFIATA3gArKClIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgDgFgJAAIgJAAgAr1DoIgFgaIgYAAIgFAaIgRAAIAbh5IAOAAIAbB5gAsPC9IARAAIgIgrgAtgDoIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAufDoIAAh5IASAAIAABnIAiAAIAAASgAODA9IgBAAIgBAAQgKgBgIgIQgJgGAAgOIAAg+QAAgPAJgHQAIgIAKAAIABAAIABAAIABAAIACAAQAJAAAJAIQAJAHAAAPIAAAFIgSAAIAAgFQAAgHgEgDQgDgDgFAAQgFAAgDADQgDADAAAHIAAA+QAAAGADADQADACAFAAQAFAAADgCQAEgDAAgGIAAgYIgOAAIAAgMIAgAAIAAAkQAAAOgJAGQgJAIgJABIgCAAIgBAAgAFsA9IgBAAIgBAAQgKgBgIgIQgJgGAAgOIAAg+QAAgPAJgHQAIgIAKAAIABAAIABAAIACAAIAAAAQALAAAIAIQAIAHAAAPIAAA+QAAAOgIAGQgIAIgLABIAAAAIgCAAgAFkgoQgDADAAAHIAAA+QAAAGADADQAEACAEAAQAFAAAEgCQADgDAAgGIAAg+QAAgHgDgDQgEgDgFAAQgEAAgEADgAElA9IgDAAQgJgBgHgHQgIgGAAgOIAAhBQAAgMAIgIQAIgIAMAAQANAAAHAIQAJAIgBANIAAAGIgRAAIAAgFQAAgFgDgEQgDgEgFAAQgHAAgCAEQgBAFAAAGIAAA8QgBAEADAEQACADAGAAIABAAIACAAQADgBACgCQADgDAAgFIAAgHIARAAIAAAIQABALgJAIQgIAJgMAAIgBAAgAgJA9IgBAAIgBAAQgKgBgIgIQgIgGgBgOIAAg+QABgPAIgHQAIgIAKAAIABAAIABAAIACAAIABAAQAIAAAJAIQAIAHAAAPIAAAFIgRAAIAAgFQAAgHgCgDQgEgDgFAAQgEAAgEADQgDADAAAHIAAA+QAAAGADADQAEACAEAAQAFAAAEgCQACgDAAgGIAAgYIgNAAIAAgMIAeAAIAAAkQAAAOgIAGQgJAIgIABIgBAAIgCAAgAjbA9IgBAAIgBAAQgKgBgJgIQgJgGABgOIAAg+QgBgPAJgHQAJgIAKAAIABAAIABAAIABAAIABAAQAKAAAIAIQAJAHAAAPIAAA+QAAAOgJAGQgIAIgKABIgBAAIgBAAgAjjgoQgEADAAAHIAAA+QAAAGAEADQADACAFAAQAEAAAEgCQADgDAAgGIAAg+QAAgHgDgDQgEgDgEAAQgFAAgDADgAmPA0QgJgHAAgMIAAgHIASAAIAAAGQAAAEACAEQADADAHAAQAIAAACgEQACgFABgJQAAgIgCgDQgBgEgFgCIgBAAIgCgBIgMgFQgLgDgEgHQgEgIAAgLQAAgNAIgLQAHgJAPAAQANAAAIAIQAHAIAAAKIAAAAIAAABIAAAJIgQAAIAAgEQgBgGgDgEQgCgFgHAAQgHAAgDAFQgBAFAAAGQgBAGACAEQABADAGADIABAAIAAAAIANAFQAMAFADAGQADAHAAANQABAQgHAKQgGAKgRAAQgNAAgIgJgANHA8IgahHIAABHIgSAAIAAh3IARAAIAaBHIAAhHIARAAIAAB3gAL5A8IAAh3IASAAIAAB3gAK6A8IAAh3IASAAIAABlIAiAAIAAASgAJ7A8IAAh3IASAAIAABlIAiAAIAAASgAI8A8IAAh3IA0AAIAAAQIgiAAIAAAkIAdAAIAAAOIgdAAIAAAjIAiAAIAAASgAH3A8IAAh3IAaAAQAHAAAGACQAFABAFAFQAEAFACAHQACAGAAALIgCANQgBAGgDADQgDAGgGADQgGAEgKAAIgJAAIAAAvgAIIgBIAJAAQAKgBACgFQACgGAAgJQAAgJgBgFQgDgHgKAAIgJAAgAHWA8IAAhHIgBAAIgRAyIgIAAIgRgyIgBAAIAABHIgRAAIAAh3IARAAIAVA+IABAAIAWg+IARAAIAAB3gAC6A8IAAgzIgYhEIASAAIAOAwIAPgwIASAAIgYBEIAAAzgABqA8IAAh3IASAAIAABlIAjAAIAAASgABJA8IAAg2IgWAAIAAA2IgRAAIAAh3IARAAIAAA0IAWAAIAAg0IARAAIAAB3gAhHA8IAAh3IASAAIAAB3gAhoA8IAAg2IgWAAIAAA2IgSAAIAAh3IASAAIAAA0IAWAAIAAg0IAQAAIAAB3gAkkA8IAAhnIgVAAIAAgQIA6AAIAAAQIgUAAIAABngAnUA8IAAh3IASAAIAABlIAiAAIAAASgAntA8IgFgaIgYAAIgFAaIgRAAIAbh3IAOAAIAbB3gAoGARIAQAAIgIgpgAo6A8IAAh3IARAAIAAB3gApXA8IgQg0IgLAAIAAA0IgRAAIAAh3IAcAAQAPAAAHAJQAIAJAAARQAAALgDAHQgDAGgJAFIATA3gApygFIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgArCA8IAAh3IAzAAIAAAQIgiAAIAAAkIAeAAIAAAOIgeAAIAAAjIAiAAIAAASgArvA8IAAhnIgVAAIAAgQIA6AAIAAAQIgUAAIAABngAsUA8IgGgaIgYAAIgEAaIgRAAIAbh3IAOAAIAbB3gAsuARIARAAIgIgpgAtiA8IAAhHIAAAAIgRAyIgJAAIgRgyIAAAAIAABHIgSAAIAAh3IARAAIAXA+IAAAAIAWg+IARAAIAAB3gAGFh2QgJgIABgLIAAgHIARAAIAAAGQAAAEADAEQADADAHAAQAIAAACgEQACgFAAgJQAAgIgCgDQgBgEgEgCIgCAAIgBgBIgMgFQgLgEgFgIQgDgIAAgLQAAgOAHgKQAIgJAOAAQANAAAIAIQAHAIABAKIAAAAIAAABIAAAJIgRAAIAAgEQAAgGgDgEQgDgFgGAAQgHAAgDAFQgCAFAAAGQAAAGACAEQAAADAHADIAAAAIABAAIANAFQAMAFADAIQADAHAAANQAAAQgGAKQgHAKgQAAQgNAAgJgJgAD4h1QgJgJAAgLIAAheIASAAIAABcQAAAGACAEQADACAFAAQAFAAADgCQADgEAAgGIAAhcIARAAIAABeQAAALgIAJQgJAIgLAAQgMAAgHgIgADChtIAAAAIgBAAQgKgBgJgIQgIgHAAgNIAAhAQAAgPAIgHQAJgIAKAAIABAAIAAAAIACAAIABAAQAKAAAIAIQAJAHAAAPIAABAQAAANgJAHQgIAIgKABIgBAAIgCAAgAC6jUQgDADAAAHIAABAQAAAGADADQAEACAEAAQAFAAAEgCQAEgDgBgGIAAhAQABgHgEgDQgEgDgFAAQgEAAgEADgAB7htIgCAAQgKgBgHgHQgHgGgBgOIAAhDQAAgMAJgIQAHgIANAAQAMAAAIAIQAIAIAAANIAAAGIgSAAIAAgFQAAgFgDgEQgCgEgGAAQgGAAgCAEQgCAEAAAHIAAA+QAAAFACADQACADAGAAIACAAIACAAQACgBADgCQACgDAAgFIAAgHIASAAIAAAIQAAALgIAIQgJAJgLAAIgCAAgAjIh1QgJgJAAgLIAAheIASAAIAABcQAAAGACAEQADACAFAAQAFAAADgCQADgEAAgGIAAhcIARAAIAABeQAAALgIAJQgJAIgLAAQgMAAgHgIgAj+htIAAAAIgBAAQgKgBgJgIQgIgHAAgNIAAhAQAAgPAIgHQAJgIAKAAIABAAIAAAAIACAAIABAAQAKAAAIAIQAJAHAAAPIAAAFIgSAAIAAgFQABgHgEgDQgEgDgFAAQgEAAgEADQgDADAAAHIAABAQAAAGADADQAEACAEAAQAFAAAEgCQAEgEgBgFIAAgYIgNAAIAAgPIAfAAIAAAnQAAANgJAHQgIAIgKABIgBAAIgCAAgAs+htIAAAAIgBAAQgKgBgJgIQgIgHAAgNIAAhAQAAgPAIgHQAJgIAKAAIABAAIAAAAIACAAIABAAQAKAAAIAIQAJAHAAAPIAABAQAAANgJAHQgIAIgKABIgBAAIgCAAgAtGjUQgDADAAAHIAABAQAAAGADADQAEACAEAAQAFAAAEgCQAEgDgBgGIAAhAQABgHgEgDQgEgDgFAAQgEAAgEADgAuFhtIgCAAQgKgBgHgHQgHgGgBgOIAAhDQAAgMAIgIQAIgIANAAQAMAAAIAIQAIAIAAANIAAAGIgSAAIAAgFQAAgFgDgEQgCgEgGAAQgGAAgCAEQgCAEAAAHIAAA+QAAAFACADQACADAGAAIACAAIACAAQACgBADgCQACgDAAgFIAAgHIASAAIAAAIQAAALgIAIQgJAJgLAAIgCAAgAL3huIAAh5IAzAAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAkIAiAAIAAARgALahuIgQg0IgKAAIAAA0IgSAAIAAh5IAcAAQAPAAAIAJQAHAJAAARQAAALgCAHQgEAIgJAFIAUA3gALAixIAKAAQAIAAADgFQADgFgBgJQAAgJgCgFQgDgFgJAAIgJAAgAKVhuIgGgaIgXAAIgGAaIgRAAIAbh5IAPAAIAbB5gAJ7iZIARAAIgIgrgAI8huIgOhPIAAAAIgNBPIgQAAIgTh5IATAAIAJBNIAOhNIANAAIAOBOIABAAIAIhOIASAAIgSB5gAHFhuIAAh5IA0AAIAAAQIgiAAIAAAkIAdAAIAAAQIgdAAIAAAkIAiAAIAAARgAFkhuIgRg0IgKAAIAAA0IgSAAIAAh5IAcAAQAQAAAIAJQAHAJAAARQAAALgDAHQgEAIgIAFIATA3gAFJixIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAAmhuIgQg0IgLAAIAAA0IgPAAIAAh5IAaAAQAPAAAHAJQAIAJAAARQAAALgDAHQgDAIgJAFIATA3gAALixIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAgehuIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gAg4iZIASAAIgJgrgAiJhuIAAh5IASAAIAABoIAjAAIAAARgAlYhuIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAkIAjAAIAAARgAl1huIgQg0IgKAAIAAA0IgSAAIAAh5IAcAAQAQAAAHAJQAIAJgBARQAAALgDAHQgDAIgIAFIATA3gAmPixIAKAAQAIAAADgFQACgFAAgJQAAgJgDgFQgCgFgJAAIgJAAgAnuhuIAAhpIgUAAIAAgQIA6AAIAAAQIgUAAIAABpgAoVhuIgRg0IgKAAIAAA0IgSAAIAAh5IAcAAQAQAAAIAJQAHAJAAARQAAALgDAHQgEAIgIAFIATA3gAowixIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAqBhuIAAh5IA1AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAkIAjAAIAAARgAqzhuIgYh5IASAAIAOBVIAOhVIATAAIgaB5gArlhuIgbhJIAABJIgRAAIAAh5IAQAAIAbBJIAAhJIARAAIAAB5g");
	this.shape.setTransform(15.6,5.2);

	// Layer 1
	this.instance = new lib.cloud4();
	this.instance.setTransform(130,84.5,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc28 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AOaCaQgIgJAAgMIAAgGIARAAIAAAFQAAAFADAEQADAEAHAAQAIAAACgFQACgFAAgIQAAgJgBgDQgCgDgEgCIgBgBIgCAAIgMgGQgLgEgEgIQgEgHAAgMQAAgNAHgKQAIgKAPAAQAMAAAIAIQAIAIAAAKIAAABIAAABIAAAIIgRAAIAAgEQAAgGgDgFQgCgEgHAAQgHAAgDAFQgCAFAAAGQAAAGACAEQABADAGACIAAABIABAAIANAGQAMAEADAHQADAJAAANQAAAPgGAKQgHAKgQAAQgNAAgJgIgAMWCaQgIgJAAgMIAAgGIARAAIAAAFQAAAFADAEQADAEAHAAQAIAAACgFQACgFAAgIQAAgJgBgDQgCgDgEgCIgBgBIgCAAIgMgGQgLgEgEgIQgEgHAAgMQAAgNAHgKQAIgKAPAAQAMAAAIAIQAIAIAAAKIAAABIAAABIAAAIIgRAAIAAgEQAAgGgDgFQgCgEgHAAQgHAAgDAFQgCAFAAAGQAAAGACAEQABADAGACIAAABIABAAIANAGQAMAEADAHQADAJAAANQAAAPgGAKQgHAKgQAAQgNAAgJgIgAKJCaQgJgJAAgLIAAheIASAAIAABcQAAAGADADQADADAEABQAFgBADgDQADgDAAgGIAAhcIARAAIAABeQAAALgIAJQgIAIgMAAQgLAAgIgIgAJUCiIgBAAIgBgBQgKAAgJgHQgIgIAAgOIAAhAQAAgNAIgIQAJgHAKgBIABAAIABAAIABAAIABAAQAKABAJAHQAIAIAAANIAABAQAAAOgIAIQgJAHgKAAIgBABIgBAAgAJMA7QgEADAAAGIAABAQAAAHAEACQADADAFABQAFgBADgDQAEgCAAgHIAAhAQAAgGgEgDQgDgDgFAAQgFAAgDADgAIMChIgCAAQgJgBgIgFQgHgHAAgPIAAhCQAAgMAIgIQAHgIANAAQAMAAAIAIQAIAIAAANIAAAHIgRAAIAAgGQAAgFgDgEQgDgEgFAAQgHAAgCAEQgCAFAAAFIAAA+QAAAFADAEQACAEAGAAIABAAIACAAQADgCACgCQADgDAAgGIAAgFIARAAIAAAHQAAALgIAJQgIAIgMAAIgCgBgAFLCaQgJgJAAgLIAAheIASAAIAABcQAAAGADADQADADAEABQAFgBADgDQADgDAAgGIAAhcIARAAIAABeQAAALgIAJQgIAIgMAAQgLAAgIgIgAEWCiIgBAAIgBgBQgKAAgJgHQgIgIAAgOIAAhAQAAgNAIgIQAJgHAKgBIABAAIABAAIABAAIABAAQAKABAJAHQAIAIAAANIAABAQAAAOgIAIQgJAHgKAAIgBABIgBAAgAEOA7QgEADAAAGIAABAQAAAHAEACQADADAFABQAFgBADgDQAEgCAAgHIAAhAQAAgGgEgDQgDgDgFAAQgFAAgDADgAlnCiIgBAAIgBgBQgKAAgJgHQgIgIAAgOIAAhAQAAgNAIgIQAJgHAKgBIABAAIABAAIABAAIABAAQAKABAJAHQAIAIAAANIAABAQAAAOgIAIQgJAHgKAAIgBABIgBAAgAlvA7QgEADAAAGIAABAQAAAHAEACQADADAFABQAFgBADgDQAEgCAAgHIAAhAQAAgGgEgDQgDgDgFAAQgFAAgDADgANWChIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAL1ChIgQgzIgLAAIAAAzIgRAAIAAh5IAcAAQAPAAAIAJQAHAJAAASQAAAKgDAIQgDAHgJAFIATA3gALaBeIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAG3ChIgQgzIgLAAIAAAzIgRAAIAAh5IAcAAQAPAAAIAJQAHAJAAASQAAAKgDAIQgDAHgJAFIATA3gAGcBeIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgADIChIAAgzIgYhGIATAAIAOAwIAOgwIASAAIgYBGIAAAzgAB8ChIgQgzIgLAAIAAAzIgRAAIAAh5IAcAAQAPAAAIAJQAHAJAAASQAAAKgDAIQgDAHgJAFIATA3gABhBeIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAARChIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAjIAjAAIAAASgAgfChIgZh5IASAAIAOBUIAOhUIARAAIgXB5gAhUChIAAh5IARAAIAAB5gAiUChIAAh5IASAAIAABnIAiAAIAAASgAjTChIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAkbChIAAh5IAaAAQAOAAAIAJQAIAHAAAPIAAA5QAAAQgIAJQgJAIgPAAgAkKCRIAIAAQAIAAADgEQADgEAAgHIAAg7QAAgHgDgEQgDgEgIAAIgIAAgAmwChIAAhpIgUAAIAAgQIA5AAIAAAQIgUAAIAABpgAoIChIAAhpIgUAAIAAgQIA5AAIAAAQIgUAAIAABpgApTChIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgApzChIgahJIAABJIgSAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAq7ChIgQgzIgKAAIAAAzIgSAAIAAh5IAcAAQAQAAAHAJQAIAJAAASQAAAKgDAIQgEAHgIAFIATA3gArVBeIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgDgFgJAAIgJAAgAsmChIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAtTChIAAhpIgUAAIAAgQIA6AAIAAAQIgUAAIAABpgAt+ChIgbhJIAABJIgRAAIAAh5IAQAAIAbBJIAAhJIARAAIAAB5gAvMChIAAh5IASAAIAAB5gADzgmIgBAAIgBgBQgKAAgIgIQgJgGAAgOIAAhAQAAgPAJgHQAIgIAKAAIABAAIABAAIACAAIABAAQAKAAAIAIQAJAHAAAPIAABAQAAAOgJAGQgIAIgKAAIgBABIgCAAgADriNQgDADAAAHIAABAQAAAGADADQAEACAEAAQAFAAAEgCQADgDAAgGIAAhAQAAgHgDgDQgEgDgFAAQgEAAgEADgAhfgmIgBAAIgBgBQgKAAgJgIQgIgGAAgOIAAhAQAAgPAIgHQAJgIAKAAIABAAIABAAIABAAIABAAQAKAAAJAIQAIAHAAAPIAABAQAAAOgIAGQgJAIgKAAIgBABIgBAAgAhniNQgEADAAAHIAABAQAAAGAEADQADACAFAAQAFAAADgCQAEgDAAgGIAAhAQAAgHgEgDQgDgDgFAAQgFAAgDADgAo+gvQgJgHAAgMIAAgHIASAAIAAAGQAAAEADAEQACADAHAAQAJAAACgEQACgFAAgJQAAgIgCgDQgBgEgFgCIgBAAIgBgBIgNgFQgLgFgEgHQgEgIAAgLQAAgNAIgLQAHgJAPAAQANAAAIAIQAHAIABAKIAAAAIAAABIAAAJIgRAAIAAgEQAAgGgDgEQgDgFgHAAQgHAAgCAFQgCAFAAAGQAAAGABAEQABADAGADIABAAIABAAIANAFQALAFAEAIQADAHAAANQAAAQgHAKQgGAKgRAAQgNAAgIgJgAH/gnIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAHegnIAAg2IgWAAIAAA2IgRAAIAAh5IARAAIAAA0IAWAAIAAg0IARAAIAAB5gAGKgnIAAhpIgUAAIAAgQIA5AAIAAAQIgUAAIAABpgAEggnIAAh5IA0AAIAAAQIgjAAIAAAlIAeAAIAAAQIgeAAIAAA0gACbgnIgQg0IgKAAIAAA0IgSAAIAAh5IAcAAQAQAAAHAJQAIAJAAARQAAALgDAHQgEAIgIAFIATA3gACBhqIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgDgFgJAAIgJAAgAAwgnIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAADgnIgLhPIgBAAIgNBPIgQAAIgSh5IASAAIAJBNIAOhNIANAAIAMBOIABAAIAIhOIASAAIgSB5gAjAgnIAAh5IAaAAQAHAAAFACQAGABAEAFQAFAFACAHQABAGAAALIgBANQgBAGgDAFQgDAGgGADQgGAEgKAAIgJAAIAAAvgAivhmIAJAAQAKgBACgFQACgGAAgJQAAgJgCgGQgCgGgKAAIgJAAgAkfgnIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAlAgnIAAg2IgWAAIAAA2IgRAAIAAh5IARAAIAAA0IAWAAIAAg0IARAAIAAB5gAmUgnIAAhpIgUAAIAAgQIA5AAIAAAQIgUAAIAABpgAn+gnIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAjIAjAAIAAASgAplgnIAAh5IARAAIAAB5gAqHgnIAAhJIgBAAIgRA0IgIAAIgRg0IgBAAIAABJIgRAAIAAh5IARAAIAWBAIAAAAIAWhAIARAAIAAB5gArmgnIAAh5IARAAIAAB5gAsBgnIgPgpIgPApIgjAAIgFgaIgYAAIgFAaIgRAAIAbh5IAOAAIAbB4IAYg+IgWg6IASAAIANAkIANgkIATAAIgXA6IAZA/gAtchSIARAAIgIgrgAuPgnIAAhJIgBAAIgRA0IgIAAIgRg0IgBAAIAABJIgRAAIAAh5IARAAIAWBAIAAAAIAWhAIARAAIAAB5g");
	this.shape.setTransform(-1.8,1.3);

	// Layer 1
	this.instance = new lib.cloud3();
	this.instance.setTransform(-152,-74.2,1.178,1.178);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-152,-74.2,304,148.5);


(lib.mc27 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AOeDIQgLgKAAgPIAAgIIAWAAIAAAHQAAAGADAEQADAFAJAAQALAAACgGQACgGAAgKQAAgLgBgEQgCgEgGgDIgBAAIgCgBIgQgGQgNgGgFgJQgFgKAAgOQAAgRAJgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAALIgWAAIAAgFQAAgHgDgGQgEgFgIAAQgJAAgDAGQgCAGAAAHQAAAIABAEQACAEAHAEIABAAIABAAIAQAHQAOAFAFAKQAEAJAAARQAAASgIANQgJAMgUAAQgQAAgKgKgAH4DSIgBAAIgBAAIgCAAQgMgBgLgJQgKgJAAgRIAAhPQAAgRAKgJQALgKAMAAIACgBIABAAIABAAIACABQAMAAALAKQAKAJAAARIAABPQAAARgKAJQgLAJgMABIgCAAgAHtBUQgFADAAAIIAABPQAAAIAFADQAEAEAGAAQAGAAAEgEQAFgDAAgIIAAhPQAAgIgFgDQgEgEgGAAQgGAAgEAEgAERDIQgLgKAAgPIAAgIIAWAAIAAAHQAAAGADAEQADAFAJAAQALAAACgGQACgGAAgKQAAgLgBgEQgCgEgGgDIgBAAIgCgBIgQgGQgNgGgFgJQgFgKAAgOQAAgRAJgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAALIgWAAIAAgFQAAgHgDgGQgEgFgIAAQgJAAgDAGQgCAGAAAHQAAAIABAEQACAEAHAEIABAAIABAAIAQAHQAOAFAFAKQAEAJAAARQAAASgIANQgJAMgUAAQgQAAgKgKgAC7DIQgKgKAAgOIAAh0IAVAAIAAByQAAAHAEAEQAEAEAFAAQAGAAAEgEQAEgEAAgHIAAhyIAVAAIAAB0QAAAOgKAKQgKAKgPAAQgOAAgKgKgAB7DSIgBAAIgBAAIgCAAQgMgBgLgJQgKgJAAgRIAAhPQAAgRAKgJQALgKAMAAIACgBIABAAIABAAIACABQAMAAALAKQAKAJAAARIAABPQAAARgKAJQgLAJgMABIgCAAgABwBUQgFADAAAIIAABPQAAAIAFADQAEAEAGAAQAGAAAEgEQAFgDAAgIIAAhPQAAgIgFgDQgEgEgGAAQgGAAgEAEgAn7DIQgLgKAAgPIAAgIIAWAAIAAAHQAAAGADAEQADAFAJAAQALAAACgGQACgGAAgKQAAgLgBgEQgCgEgGgDIgBAAIgCgBIgQgGQgNgGgFgJQgFgKAAgOQAAgRAJgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAALIgWAAIAAgFQAAgHgDgGQgEgFgIAAQgJAAgDAGQgCAGAAAHQAAAIABAEQACAEAHAEIABAAIABAAIAQAHQAOAFAFAKQAEAJAAARQAAASgIANQgJAMgUAAQgQAAgKgKgANgDRIAAiBIgZAAIAAgUIBHAAIAAAUIgZAAIAACBgAMyDRIgGggIgeAAIgGAgIgVAAIAhiVIASAAIAhCVgAMSCdIAWAAIgLg2IAAAAgALSDRIAAhbIgVBBIgLAAIgVhBIAAAAIAABbIgWAAIAAiVIAVAAIAbBPIABAAIAbhPIAVAAIAACVgAJkDRIgUhAIgNAAIAABAIgVAAIAAiVIAiAAQATAAAKALQAJALAAAWQAAAMgEAKQgEAJgKAGIAXBEgAJDB/IAMAAQAMAAADgHQADgGAAgLQAAgLgEgGQgDgGgMAAIgLAAgAGHDRIAAiVIBAAAIAAAUIgrAAIAAAtIAlAAIAAAUIglAAIAABAgAAtDRIAAiVIAVAAIAACVgAAJDRIgShAIgNAAIAABAIgVAAIAAiVIAiAAQARAAAKALQAJALAAAWQAAAMgEAKQgEAJgKAGIAXBEgAgWB/IAMAAQAKAAADgHQADgGAAgLQAAgLgEgGQgCgGgLAAIgLAAgAhLDRIgGggIgeAAIgGAgIgVAAIAhiVIASAAIAhCVgAhrCdIAWAAIgLg2IAAAAgAi/DRIgeiVIAWAAIARBoIABAAIARhoIAWAAIgeCVgAkkDRIghhaIgBAAIAABaIgVAAIAAiVIAVAAIAgBaIAAAAIAAhaIAWAAIAACVgAmFDRIAAiVIAWAAIAACVgApPDRIAAiVIAVAAIAACAIArAAIAAAVgApvDRIgGggIgeAAIgGAgIgVAAIAhiVIASAAIAhCVgAqPCdIAWAAIgLg2IAAAAgArPDRIAAiVIAVAAIAACVgArzDRIgUhAIgNAAIAABAIgVAAIAAiVIAiAAQATAAAKALQAJALAAAWQAAAMgEAKQgEAJgKAGIAXBEgAsUB/IAMAAQAMAAADgHQADgGAAgLQAAgLgEgGQgDgGgMAAIgLAAgAt3DRIAAiVIBAAAIAAAUIgqAAIAAAsIAlAAIAAAUIglAAIAAAsIAqAAIAAAVgAuuDRIAAiBIgZAAIAAgUIBHAAIAAAUIgZAAIAACBgAvcDRIgGggIgeAAIgGAgIgVAAIAhiVIASAAIAhCVgAv8CdIAWAAIgLg2IAAAAgAw8DRIAAhbIgVBBIgLAAIgVhBIAAAAIAABbIgWAAIAAiVIAVAAIAbBPIABAAIAbhPIAVAAIAACVgAg/hBQgFAEgGACQgFACgHAAIgBAAIgBAAQgNgBgKgJQgKgJAAgSIAAhOQAAgSAKgJQAKgJANgBIABAAIABAAIACAAIABAAQANABAKAJQAKAJAAASIAABOIAAAJIgDAHIANALIgKANgAhgi4QgEAEAAAIIAABOQAAAIAEAEQAFAEAFAAIADgBIACAAIgKgIIALgNIAJAHIAAhPQAAgIgEgEQgFgEgGAAQgFAAgFAEgARlg5IgBAAIgBAAIgCAAQgMgBgLgJQgKgJAAgSIAAhOQAAgSAKgJQALgJAMgBIACAAIABAAIABAAIACAAQAMABALAJQAKAJAAASIAAAHIgVAAIAAgHQAAgIgFgEQgEgEgGAAQgGAAgEAEQgFAEAAAIIAABOQAAAIAFAEQAEAEAGAAQAGAAAEgEQAFgEAAgIIAAgcIgSAAIAAgTIAnAAIAAAvQAAASgKAJQgLAJgMABIgCAAgAgVhDQgKgKAAgPIAAh0IAWAAIAAByQAAAIAEAEQADAEAEAAQAGAAADgEQAEgEAAgIIAAhyIAWAAIAAB0QAAAPgLAKQgKAKgOAAQgMAAgLgKgAlZg5IgBAAIgBAAIgCAAQgMgBgLgJQgKgJAAgSIAAhOQAAgSAKgJQALgJAMgBIACAAIABAAIABAAIACAAQAMABALAJQAKAJAAASIAAAHIgVAAIAAgHQAAgIgFgEQgEgEgGAAQgGAAgEAEQgFAEAAAIIAABOQAAAIAFAEQAEAEAGAAQAGAAAEgEQAFgEAAgIIAAgcIgSAAIAAgTIAnAAIAAAvQAAASgKAJQgLAJgMABIgCAAgAqsg5IgDgBQgLgBgJgHQgJgIAAgSIAAhTQAAgOAJgKQAKgKAPAAQAPAAAKAKQAKAKAAAQIAAAIIgWAAIAAgHQAAgGgDgFQgEgFgGAAQgIAAgCAFQgDAFAAAIIAABMQAAAHADAEQADAFAHAAIACgBIABAAQAEgBADgDQADgDAAgIIAAgHIAWAAIAAAJQAAAOgKALQgKAKgOAAIgDAAgAsbhDQgKgKAAgPIAAh0IAWAAIAAByQAAAIAEAEQADAEAGAAQAGAAADgEQAEgEAAgIIAAhyIAWAAIAAB0QAAAPgLAKQgKAKgOAAQgOAAgLgKgAu0g5IgBAAIgBAAIgCAAQgMgBgLgJQgKgJAAgSIAAhOQAAgSAKgJQALgJAMgBIACAAIABAAIABAAIACAAQAMABALAJQAKAJAAASIAABOQAAASgKAJQgLAJgMABIgCAAgAu/i4QgFAEAAAIIAABOQAAAIAFAEQAEAEAGAAQAGAAAEgEQAFgEAAgIIAAhOQAAgIgFgEQgEgEgGAAQgGAAgEAEgAQag6IghhaIAAAAIAABaIgWAAIAAiWIAVAAIAgBaIABAAIAAhaIAVAAIAACWgAO6g6IAAiWIAVAAIAACWgAOSg6IghhaIAAAAIAABaIgWAAIAAiWIAVAAIAgBaIABAAIAAhaIAVAAIAACWgAM5g6IgUhAIgNAAIAABAIgVAAIAAiWIAiAAQATAAAKALQAJALAAAWQAAANgEAJQgEAKgKAGIAXBEgAMYiNIAMAAQAMAAADgGQADgGAAgLQAAgMgEgGQgDgGgMAAIgLAAgALjg6IgGggIgeAAIgGAgIgVAAIAhiWIASAAIAhCWgALDhvIAWAAIgLg2IAAAAgAJgg6IAAiWIBAAAIAAAUIgrAAIAAAsIAlAAIAAAUIglAAIAAAsIArAAIAAAWgAISg6IAAiWIAWAAIAACAIAqAAIAAAWgAHFg6IAAiWIBAAAIAAAUIgrAAIAAAsIAlAAIAAAUIglAAIAAAsIArAAIAAAWgAFgg6IAAg/IgdhVIAAASIgZAAIAACCIgVAAIAAiCIgZAAIAAgUIBeAAIARA8IAAAAIARg8IAXAAIgdBXIAAA/gADcg6IAAiWIAWAAIAACWgACOg6IAAiWIAWAAIAACAIAqAAIAAAWgABug6IgGggIgdAAIgGAgIgWAAIAiiWIARAAIAiCWgABPhvIAVAAIgKg2IgBAAgAjzg6IAAhDIgbAAIAABDIgVAAIAAiWIAVAAIAABAIAbAAIAAhAIAVAAIAACWgAmng6IAAiWIAVAAIAACWgAnRg6IAAhDIgbAAIAABDIgVAAIAAiWIAVAAIAABAIAbAAIAAhAIAVAAIAACWgAp2g6IAAiWIBAAAIAAAUIgqAAIAAAsIAlAAIAAAUIglAAIAAAsIAqAAIAAAWgAt+g6IAAiWIAfAAQATAAAJALQAKAKAAASIAABGQAAAVgKAKQgKAKgTAAgAtphOIAKAAQAJAAAEgFQAEgEAAgKIAAhIQAAgJgEgFQgDgFgKAAIgKAAgAv7g6IgUhAIgNAAIAABAIgVAAIAAiWIAiAAQATAAAKALQAJALAAAWQAAANgEAJQgEAKgKAGIAXBEgAwciNIAMAAQAMAAADgGQADgGAAgLQAAgMgEgGQgDgGgMAAIgLAAgAyHg6IAAiWIAgAAQAJAAAHADQAHACAFAGQAGAGACAHQACAIAAAOIgBARQgBAHgEAGQgFAHgHAEQgIAEgLAAIgLAAIAAA7gAxxiJIAKAAQANAAACgHQADgHAAgLQAAgKgCgIQgCgIgNAAIgLAAgAjLhqIAAgVIBAAAIAAAVg");
	this.shape.setTransform(-6,-6);

	// Layer 1
	this.instance = new lib.cloud5();
	this.instance.setTransform(-160.5,73.5,1,1,0,180,0);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-160.5,-73.5,321,147);


(lib.mc26 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AELEdQgLgKAAgPIAAgIIAWAAIAAAHQAAAFADAFQADAFAJAAQAKAAACgGQADgGAAgLQAAgKgCgEQgBgEgHgDIgBAAIgCgBIgPgGQgNgGgGgKQgEgJAAgOQAAgRAJgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAAKIgWAAIAAgFQAAgGgDgGQgEgGgIAAQgJABgDAFQgCAHgBAHQABAIABAEQACAEAHAEIAAAAIABAAIARAHQAOAEAEAKQAFAKAAAQQgBATgHANQgJAMgUAAQgQAAgKgKgAoiEnIgDAAQgKgBgJgIQgKgIABgSIAAhSQAAgPAJgKQAKgKAPAAQAPAAAKAKQAKAKgBAQIAAAIIgVAAIAAgHQAAgGgDgFQgEgFgGAAQgIABgDAEQgDAFAAAIIAABNQAAAGADAEQAEAFAHAAIACAAIABAAQAEgCADgDQADgDAAgHIAAgIIAVAAIAAAJQABAPgKAKQgKAKgOAAIgEAAgAu3EdQgLgKAAgPIAAgIIAWAAIAAAHQAAAFAEAFQACAFAJAAQALAAACgGQACgGABgLQgBgKgBgEQgCgEgGgDIgBAAIgCgBIgPgGQgOgGgFgKQgEgJgBgOQABgRAIgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAAKIgVAAIAAgFQgBgGgDgGQgDgGgJAAQgIABgDAFQgDAHAAAHQAAAIACAEQABAEAIAEIAAAAIABAAIARAHQAOAEAEAKQAEAKAAAQQAAATgIANQgJAMgUAAQgPAAgLgKgAC2EmIAAiWIBBAAIAAAUIgrAAIAAAsIAlAAIAAAVIglAAIAAArIArAAIAAAWgAB/EmIAAiCIgYAAIAAgUIBGAAIAAAUIgYAAIAACCgABREmIgGggIgeAAIgFAgIgWAAIAiiWIARAAIAiCWgAAyDyIAVAAIgKg3IgBAAgAgNEmIAAiWIAUAAIAACWgAhbEmIAAiWIAVAAIAACAIArAAIAAAWgAiGEmIAAiWIAWAAIAACWgAjUEmIAAiWIBBAAIAAAUIgrAAIAAAuIAlAAIAAAUIglAAIAABAgAkhEmIAAiWIBAAAIAAAUIgrAAIAAAuIAlAAIAAAUIglAAIAABAgAlBEmIgGggIgeAAIgFAgIgWAAIAhiWIASAAIAiCWgAlgDyIAVAAIgLg3IAAAAgAnrEmIAAiWIBAAAIAAAUIgqAAIAAAsIAlAAIAAAVIglAAIAAArIAqAAIAAAWgAprEmIAAiWIAWAAIAACWgAqqEmIgeiWIAWAAIARBpIABAAIARhpIAXAAIgfCWgArkEmIgUhAIgNAAIAABAIgVAAIAAiWIAiAAQATABAKAKQAJALAAAWQAAANgFAKQgEAJgJAGIAWBEgAsFDUIAMAAQAMAAACgHQADgGAAgLQABgLgEgHQgDgFgMgBIgLAAgAtoEmIAAiWIBAAAIAAAUIgrAAIAAAsIAmAAIAAAVIgmAAIAAArIArAAIAAAWgAGNBBQgLgJAAgPIAAgIIAWAAIAAAGQAAAGAEAFQACAEAJAAQALAAACgFQACgHABgKQgBgLgBgEQgCgDgGgEIgBAAIgCgBIgPgGQgOgDgFgKQgEgJgBgPQABgQAIgNQAJgLATAAQAPAAAKAKQAKAKABAMIAAAAIAAABIAAALIgVAAIAAgFQgBgHgDgFQgDgGgJAAQgIAAgDAGQgDAGAAAHQAAAIACAFQABAEAIADIAAAAIABAAIARAHQAOAFAEAIQAEAKAAAQQAAATgIAMQgJANgUAAQgPAAgLgLgAj6BMIgCAAIAAAAIgCAAQgNgCgKgJQgKgIAAgSIAAhMQAAgSAKgJQAKgJANgBIACAAIAAAAIACAAIABAAQANABALAJQAKAJgBASIAABMQABASgKAIQgLAJgNACIgBAAgAkFgxQgFADAAAJIAABMQAAAIAFAEQAEADAFAAQAGAAAFgDQAFgEAAgIIAAhMQAAgJgFgDQgFgEgGAAQgFAAgEAEgAouBMIgBAAIgBAAIgBAAQgNgCgLgJQgKgIABgSIAAhMQgBgSAKgJQALgJANgBIABAAIABAAIABAAIACAAQAMABALAJQAKAJAAASIAABMQAAASgKAIQgLAJgMACIgCAAgAo5gxQgEADgBAJIAABMQABAIAEAEQAFADAFAAQAGAAAEgDQAFgEAAgIIAAhMQAAgJgFgDQgEgEgGAAQgFAAgFAEgAK1BKIAAiTIAgAAQATAAAJAKQAKALAAARIAABFQAAAVgKAJQgLAKgSAAgALLA2IAKAAQAJABADgFQAEgEABgLIAAhGQAAgIgEgFQgEgFgJAAIgKAAgAKPBKIgihYIAAAAIAABYIgVAAIAAiTIAUAAIAhBYIAAAAIAAhYIAVAAIAACTgAI4BKIgFgfIgeAAIgGAfIgWAAIAiiTIASAAIAhCTgAIZAWIAVAAIgKg0IAAAAgAFPBKIAAh/IgYAAIAAgUIBHAAIAAAUIgZAAIAAB/gAEaBKIghhYIAAAAIAABYIgWAAIAAiTIAVAAIAgBYIAAAAIAAhYIAWAAIAACTgACXBKIAAiTIBAAAIAAAUIgrAAIAAAsIAlAAIAAASIglAAIAAAsIArAAIAAAVgABJBKIAAiTIAWAAIAAB+IAqAAIAAAVgAApBKIgGgfIgdAAIgGAfIgUAAIAfiTIASAAIAhCTgAAJAWIAWAAIgLg0IAAAAgAhCBKIAAh/IgYAAIAAgUIBHAAIAAAUIgaAAIAAB/gAjFBKIAAiTIBAAAIAAAUIgqAAIAAAtIAlAAIAAASIglAAIAABAgAllBKIgYhCIgMAWIAAAsIgVAAIAAiTIAVAAIAABEIAAAAIAghEIAVAAIgdA8IAjBXgAnCBKIgThAIgOAAIAABAIgVAAIAAiTIAjAAQATAAAJALQAJALAAAWQAAAMgEAKQgEAHgKAHIAXBDgAnjgGIANAAQALAAADgGQADgHAAgKQAAgMgEgGQgDgGgLAAIgMAAgAqIBKIgRheIgBAAIgQBeIgUAAIgWiTIAWAAIALBdIARhdIARAAIASBfIAKhfIAXAAIgXCTgAsFBKIAAh/IgZAAIAAgUIBHAAIAAAUIgZAAIAAB/gAtgBKIAAiTIA/AAIAAAUIgqAAIAAAsIAlAAIAAASIglAAIAAAsIAqAAIAAAVgAuHBKIghhYIgBAAIAABYIgWAAIAAiTIAWAAIAfBYIABAAIAAhYIAWAAIAACTgAKeiOIgCAAIgBAAIgBAAQgMgBgLgJQgKgJAAgSIAAhOQAAgRAKgKQALgJAMgBIABAAIABAAIACAAIACAAQAMABAKAJQAKAKAAARIAABOQAAASgKAJQgKAJgMABIgCAAgAKSkNQgEAEAAAIIAABOQAAAJAEADQAFAEAFAAQAHAAAEgEQAEgDAAgJIAAhOQAAgIgEgEQgEgDgHAAQgFAAgFADgAH3iOIgBAAIgBAAIgCAAQgNgBgKgJQgKgJAAgSIAAhOQAAgRAKgKQAKgJANgBIACAAIABAAIABAAIABAAQANABALAJQAKAKAAARIAAAHIgVAAIAAgHQAAgIgFgEQgFgDgFAAQgGAAgEADQgFAEAAAIIAABOQAAAJAFADQAEAEAGAAQAFAAAFgEQAFgDAAgJIAAgcIgSAAIAAgSIAnAAIAAAuQAAASgKAJQgLAJgNABIgBAAgAEDiYQgKgKAAgPIAAh0IAVAAIAAByQAAAIAFAEQADAEAGAAQAFAAAEgEQAEgEAAgIIAAhyIAWAAIAAB0QAAAPgLAKQgKAKgOAAQgOAAgLgKgADEiOIgCAAIgBAAIgCAAQgMgBgKgJQgLgJABgSIAAhOQgBgRALgKQAKgJAMgBIACAAIABAAIACAAIABAAQAMABALAJQAKAKAAARIAABOQAAASgKAJQgLAJgMABIgBAAgAC4kNQgEAEgBAIIAABOQABAJAEADQAEAEAGAAQAGAAAFgEQAEgDAAgJIAAhOQAAgIgEgEQgFgDgGAAQgGAAgEADgABDiOIgCAAIgBAAIgBAAQgMgBgLgJQgKgJAAgSIAAhOQAAgRAKgKQALgJAMgBIABAAIABAAIACAAIACAAQAMABAKAJQAKAKAAARIAABOQAAASgKAJQgKAJgMABIgCAAgAA3kNQgEAEAAAIIAABOQAAAJAEADQAFAEAFAAQAHAAAEgEQAEgDAAgJIAAhOQAAgIgEgEQgEgDgHAAQgFAAgFADgAiaiYQgLgKAAgPIAAgIIAVAAIAAAGQABAGADAFQADAFAJAAQAKAAACgGQADgGAAgLQAAgKgCgEQgBgEgHgDIgBAAIgCgBIgPgHQgNgFgGgKQgEgJAAgOQAAgRAJgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAAKIgWAAIAAgEQAAgIgEgFQgDgGgIABQgJAAgDAFQgCAHgBAHQABAIABAEQACAEAHADIAAAAIABABIARAHQAOAFAEAJQAFAKAAAQQgBATgHANQgJAMgUAAQgQAAgKgKgAjwiYQgLgKAAgPIAAgIIAWAAIAAAGQAAAGAEAFQACAFAJAAQALAAACgGQACgGABgLQgBgKgBgEQgCgEgGgDIgBAAIgCgBIgPgHQgOgFgFgKQgEgJgBgOQABgRAIgMQAJgMATAAQAPAAAKAKQAKAKABANIAAAAIAAABIAAAKIgVAAIAAgEQgBgIgDgFQgDgGgJABQgIAAgDAFQgDAHAAAHQAAAIACAEQABAEAIADIAAAAIABABIARAHQAOAFAEAJQAEAKAAAQQAAATgIANQgJAMgUAAQgPAAgLgKgAl6iOIgDgBQgLgBgKgHQgIgIgBgSIAAhTQAAgOAKgKQAKgKAPAAQAPAAAKAKQAJALAAAPIAAAIIgVAAIAAgHQAAgGgEgFQgDgFgGABQgIAAgDAEQgCAGAAAHIAABMQAAAHACAEQADAFAIAAIABAAIACAAQAEgBADgEQADgDAAgIIAAgHIAVAAIAAAJQAAAOgJALQgKAKgPAAIgCAAgAnQiOIgDgBQgKgBgKgHQgJgIAAgSIAAhTQAAgOAKgKQAJgKAQAAQAPAAAJAKQAKALAAAPIAAAIIgWAAIAAgHQAAgGgDgFQgEgFgFABQgJAAgCAEQgDAGAAAHIAABMQAAAHADAEQADAFAIAAIABAAIABAAQAFgBADgEQACgDAAgIIAAgHIAWAAIAAAJQAAAOgKALQgJAKgPAAIgDAAgAOCiPIAAiWIAWAAIAACBIAqAAIAAAVgANiiPIgFggIgeAAIgGAgIgWAAIAiiWIARAAIAiCWgANDjEIAVAAIgKg2IgBAAgALUiPIAAiWIAfAAQAVAAAIAMQAJAMAAAPIAAAFQAAAMgEAFQgDAGgIAEQAIAEAEAHQADAHAAAMIAAAJQAAATgKALQgJAKgVAAgALpikIAJAAQANgBADgGQADgIAAgKQAAgMgDgGQgEgFgMgBIgJAAgALpjnIAJAAQAMAAADgFQAEgGAAgKQAAgKgEgGQgFgEgKAAIgJAAgAItiPIAAiWIAVAAIAACBIAqAAIAAAVgAGJiPIgUhAIgNAAIAABAIgVAAIAAiWIAiAAQATAAAKAMQAJAKAAAWQAAANgFAKQgDAJgKAGIAWBEgAFojhIAMAAQAMgBACgGQADgGAAgLQAAgLgDgHQgDgFgMAAIgLAAgAgXiPIAAiBIgYAAIAAgVIBFAAIAAAVIgXAAIAACBgAlEiPIAAiWIBAAAIAAAVIgrAAIAAAsIAlAAIAAAUIglAAIAAAsIArAAIAAAVgAoPiPIgGggIgeAAIgFAgIgWAAIAhiWIASAAIAiCWgAoujEIAVAAIgLg2IAAAAgAq5iPIAAiWIBAAAIAAAVIgqAAIAAAsIAkAAIAAAUIgkAAIAAAsIAqAAIAAAVgAr3iPIgfiWIAXAAIARBpIABAAIARhpIAWAAIgeCWgAsviPIgFggIgeAAIgGAgIgWAAIAiiWIARAAIAiCWgAtOjEIAVAAIgKg2IgBAAgAuNiPIAAhCIgbAAIAABCIgVAAIAAiWIAVAAIAABBIAbAAIAAhBIAVAAIAACWg");
	this.shape.setTransform(15,20.8);

	this.instance = new lib.cloud1();
	this.instance.setTransform(157,98,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-157,-98,314,196);


(lib.mc25 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AtSDCIgCAAIgBAAIgCAAQgNgBgLgJQgLgKAAgSIAAhSQAAgSALgKQALgJANgBIACAAIABAAIACAAIABAAQANABALAJQALAKAAASIAABSQAAASgLAKQgLAJgNABIgBAAgAtfA+QgEAEAAAIIAABSQAAAJAEADQAFAFAGAAQAGAAAFgFQAEgDAAgJIAAhSQAAgIgEgEQgFgEgGAAQgGAAgFAEgAxnDCIgBAAIgCAAIgBAAQgNgBgLgJQgLgKAAgSIAAhSQAAgSALgKQALgJANgBIABAAIACAAIABAAIABAAQAOABAKAJQALAKAAASIAABSQAAASgLAKQgKAJgOABIgBAAgAxzA+QgFAEAAAIIAABSQAAAJAFADQAEAFAHAAQAGAAAEgFQAFgDAAgJIAAhSQAAgIgFgEQgEgEgGAAQgHAAgEAEgAzGDCIgDAAQgLgBgKgIQgKgJAAgSIAAhWQAAgQALgKQAKgKAQAAQAQAAAKAKQAKALAAAQIAAAJIgWAAIAAgIQAAgGgEgFQgEgFgGAAQgJAAgCAFQgDAGAAAHIAABQQAAAHADAEQADAGAIAAIABgBIACAAQAEgBADgEQAEgDAAgIIAAgHIAWAAIAAAJQAAAPgKALQgLAKgPAAIgDAAgAhoDBIgHghIgeAAIgHAhIgWAAIAjicIASAAIAjCcgAiJCKIAWAAIgLg4IAAAAgAjaDBIAAiHIgaAAIAAgVIBKAAIAAAVIgaAAIAACHgAkLDBIgGghIgfAAIgGAhIgXAAIAjicIATAAIAjCcgAksCKIAXAAIgLg4IgBAAgAmgDBIAAicIAhAAQATAAAKALQAKALAAASIAABJQAAAXgKAKQgLAKgUAAgAmKCtIALAAQAJAAAEgGQAEgEAAgKIAAhMQAAgJgEgFQgEgFgJAAIgLAAgAobDBIAAicIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAWgApVDBIAAiHIgaAAIAAgVIBLAAIAAAVIgaAAIAACHgAqFDBIgHghIgeAAIgHAhIgWAAIAjicIASAAIAjCcgAqmCKIAWAAIgLg4IAAAAgAriDBIgVhCIgNAAIAABCIgXAAIAAicIAkAAQAUAAAKALQAKAMAAAXQAAANgEAKQgFAKgKAGIAYBHgAsEBrIANAAQALAAADgGQAEgHAAgLQAAgNgEgFQgDgHgNAAIgLAAgAvSDBIAAicIAiAAQAJAAAHACQAHACAGAHQAGAGACAIQACAJAAANQAAALgBAHQgBAIgEAGQgFAIgIAEQgIAFgMgBIgLAAIAAA9gAu7BvIALAAQANAAADgHQACgHAAgMQAAgLgCgIQgCgIgOAAIgLAAgAv2DBIgVhCIgOAAIAABCIgWAAIAAicIAkAAQAUAAAKALQAKAMAAAXQAAANgFAKQgEAKgLAGIAYBHgAwZBrIANAAQAMAAADgGQADgHAAgLQAAgNgDgFQgEgHgMAAIgMAAgAyqgqQgFAEgGABQgGADgHAAIgBAAIgCAAQgNgBgLgKQgLgJAAgTIAAhSQAAgSALgKQALgKANgBIACAAIABAAIACAAIABAAQANABALAKQALAKAAASIAABSIgBAJIgDAHIAOAMIgLANgAzNinQgEAEAAAIIAABSQAAAIAEAFQAFADAGAAIADAAIACgBIgKgIIALgNIAJAHIAAhTQAAgIgEgEQgFgEgGAAQgGAAgFAEgARTgtQgLgLAAgPIAAgJIAWAAIAAAHQAAAGAEAGQAEAEAIAAQAMAAACgGQACgGAAgLQAAgLgBgEQgCgEgGgEIgCAAIgCgBIgQgHQgOgFgFgKQgFgKAAgOQAAgTAKgMQAJgNAUAAQAQAAAKALQAKAKABANIAAABIAAABIAAALIgXAAIAAgFQAAgIgDgGQgEgFgIAAQgKAAgDAGQgCAHAAAHQAAAJABAEQACAEAHADIABABIABAAIARAHQAPAGAFAKQAEAJAAASQAAAUgJAMQgIAOgWAAQgQAAgLgLgAOKgiIgBAAIgCAAIgBAAQgNgBgLgKQgLgJAAgTIAAhSQAAgSALgKQALgKANgBIABAAIACAAIABAAIABAAQAOABAKAKQALAKAAASIAABSQAAATgLAJQgKAKgOABIgBAAgAN+inQgFAEAAAIIAABSQAAAIAFAFQAEADAHAAQAGAAAEgDQAFgFAAgIIAAhSQAAgIgFgEQgEgEgGAAQgHAAgEAEgAMtgiIgCAAIgBAAIgCAAQgNgBgLgKQgLgJAAgTIAAhSQAAgSALgKQALgKANgBIACAAIABAAIACAAIABAAQANABALAKQALAKAAASIAAAIIgXAAIAAgIQAAgIgEgEQgFgEgGAAQgGAAgFAEQgEAEAAAIIAABSQAAAIAEAFQAFADAGAAQAGAAAFgDQAEgFAAgIIAAgdIgRAAIAAgUIAoAAIAAAxQAAATgLAJQgLAKgNABIgBAAgAHagiIgDgBQgMgBgJgIQgKgJAAgSIAAhXQAAgPAKgKQAKgLARAAQAPAAAKALQALAKAAARIAAAJIgXAAIAAgIQAAgGgEgFQgDgFgHAAQgIAAgDAFQgDAFAAAJIAABPQAAAHAEAFQADAEAHAAIACAAIACAAQAEgBADgDQADgEAAgIIAAgHIAXAAIAAAKQAAAOgLALQgKALgPAAIgDAAgAhGgiIgDgBQgLgBgKgIQgKgJAAgSIAAhXQAAgPALgKQAKgLAQAAQAQAAAKALQAKAKAAARIAAAJIgWAAIAAgIQAAgGgEgFQgEgFgGAAQgJAAgCAFQgDAFAAAJIAABPQAAAHADAFQADAEAIAAIABAAIACAAQAEgBADgDQAEgEAAgIIAAgHIAWAAIAAAKQAAAOgKALQgLALgPAAIgDAAgAnCgtQgLgLAAgPIAAgJIAXAAIAAAHQAAAGADAGQAEAEAJAAQALAAACgGQADgGAAgLQAAgLgCgEQgCgEgGgEIgBAAIgCgBIgQgHQgPgFgFgKQgEgKAAgOQAAgTAJgMQAKgNATAAQAQAAAKALQALAKAAANIAAABIAAABIAAALIgWAAIAAgFQAAgIgEgGQgDgFgJAAQgJAAgDAGQgDAHAAAHQAAAJACAEQACAEAHADIABABIABAAIARAHQAPAGAEAKQAEAJAAASQAAAUgIAMQgJAOgVAAQgRAAgLgLgAvegiIgDgBQgLgBgKgIQgKgJAAgSIAAhXQAAgPALgKQAKgLAQAAQAQAAAKALQAKAKAAARIAAAJIgWAAIAAgIQAAgGgEgFQgEgFgGAAQgJAAgCAFQgDAFAAAJIAABPQAAAHADAFQADAEAIAAIABAAIACAAQAEgBADgDQAEgEAAgIIAAgHIAWAAIAAAKQAAAOgKALQgLALgPAAIgDAAgAx+gtQgLgLAAgOIAAh6IAXAAIAAB3QAAAIAEAFQAEADAGAAQAFAAAEgDQAEgFAAgIIAAh3IAXAAIAAB6QAAAOgLALQgKALgPAAQgPAAgLgLgASmgjIAAidIBDAAIAAAVIgtAAIAAAuIAnAAIAAAVIgnAAIAAAuIAtAAIAAAXgAQggjIAAidIAWAAIAACdgAP7gjIgVhEIgOAAIAABEIgWAAIAAidIAkAAQAUAAAKALQAKAMAAAXQAAAOgFAJQgEAKgLAGIAYBIgAPYh6IANAAQAMAAADgHQADgGAAgMQAAgLgDgHQgEgGgMAAIgMAAgAK1gjIAAidIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAJ7gjIAAiIIgaAAIAAgVIBLAAIAAAVIgaAAIAACIgAJLgjIgHgiIgeAAIgHAiIgWAAIAjidIASAAIAjCdgAIqhbIAWAAIgLg4IAAAAgAEzgjIAAidIAhAAQATAAAKAKQAKALAAAUIAABJQAAAVgKALQgLALgUAAgAFJg5IALAAQAJAAAEgEQAEgFAAgKIAAhLQAAgKgEgFQgEgFgJAAIgLAAgAEKgjIgjhfIAAAAIAABfIgXAAIAAidIAWAAIAiBfIAAAAIAAhfIAXAAIAACdgACwgjIgHgiIgeAAIgHAiIgWAAIAjidIASAAIAjCdgACPhbIAWAAIgLg4IAAAAgAAjgjIAAhGIgbAAIAABGIgVAAIAAidIAVAAIAABDIAbAAIAAhDIAXAAIAACdgAiLgjIgVhEIgNAAIAABEIgXAAIAAidIAkAAQAUAAAKALQAKAMAAAXQAAAOgEAJQgFAKgKAGIAYBIgAith6IANAAQALAAADgHQAEgGAAgMQAAgLgEgHQgDgGgNAAIgLAAgAjlgjIgHgiIgeAAIgHAiIgWAAIAjidIASAAIAjCdgAkGhbIAWAAIgLg4IAAAAgAlvgjIAAidIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAoagjIAAidIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAo+gjIgVhEIgOAAIAABEIgWAAIAAidIAkAAQAUAAAKALQAKAMAAAXQAAAOgFAJQgEAKgLAGIAYBIgAphh6IANAAQAMAAADgHQADgGAAgMQAAgLgDgHQgEgGgMAAIgMAAgArhgjIAAhDIgfhaIAYAAIASA+IABAAIASg+IAYAAIgfBaIAABDgAtHgjIAAidIAWAAIAACGIAtAAIAAAXgAtpgjIgZhGIgMAWIAAAwIgXAAIAAidIAXAAIAABHIAAAAIAhhHIAWAAIgeA+IAkBfgAwrgjIAAidIAXAAIAACdg");
	this.shape.setTransform(3.7,-3.4);

	this.instance = new lib.cloud5();
	this.instance.setTransform(160.5,73.5,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-160.5,-73.5,321,147);


(lib.mc24 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AH2BFQgLgLAAgQIAAgJIAWAAIAAAIQAAAGAEAFQAEAFAIAAQAMAAACgGQACgHAAgLQAAgLgBgEQgCgEgGgDIgBgBIgCAAIgQgHQgPgDgFgKQgEgLgBgOQAAgSAKgMQAJgNAUAAQAQAAAKALQALAKAAAMIAAABIAAABIAAAMIgXAAIAAgGQAAgIgDgFQgEgFgIAAQgKAAgCAGQgDAGAAAHQAAAJACAFQABAEAIADIABAAIAAAAIASAHQAOAGAFAIQAEAKAAARQAAAUgIANQgJANgWAAQgQAAgLgKgAFvBFQgLgLAAgQIAAgJIAXAAIAAAIQAAAGAEAFQADAFAJAAQAMAAACgGQACgHAAgLQAAgLgBgEQgDgEgGgDIgBgBIgCAAIgQgHQgPgDgEgKQgFgLAAgOQAAgSAJgMQAKgNATAAQARAAAJALQALAKABAMIAAABIAAABIAAAMIgXAAIAAgGQAAgIgDgFQgEgFgJAAQgJAAgDAGQgCAGAAAHQAAAJABAFQACAEAHADIABAAIABAAIARAHQAPAGAEAIQAEAKABARQAAAUgJANQgJANgVAAQgQAAgMgKgAEWBFQgLgLAAgQIAAgJIAXAAIAAAIQAAAGADAFQAEAFAIAAQAMAAACgGQADgHgBgLQABgLgCgEQgCgEgGgDIgBgBIgCAAIgQgHQgPgDgFgKQgEgLgBgOQAAgSAKgMQAJgNAUAAQAQAAAKALQALAKAAAMIAAABIAAABIAAAMIgXAAIAAgGQAAgIgDgFQgDgFgJAAQgKAAgDAGQgCAGAAAHQAAAJACAFQABAEAIADIAAAAIABAAIASAHQAOAGAFAIQAEAKAAARQAAAUgIANQgJANgWAAQgQAAgLgKgAhvBFQgKgLAAgPIAAh3IAWAAIAAB1QAAAHAEAFQAEAEAGAAQAFAAAFgEQADgFAAgHIAAh1IAXAAIAAB3QAAAPgKALQgLAKgPAAQgPAAgLgKgANcBOIAAiFIgaAAIAAgVIBKAAIAAAVIgaAAIAACFgAMkBOIgihcIgBAAIAABcIgXAAIAAiaIAXAAIAhBcIABAAIAAhcIAWAAIAACagALKBOIgHgiIgeAAIgGAiIgXAAIAjiaIASAAIAjCagAKpAXIAXAAIgMg2IAAAAgAJYBOIAAiFIgaAAIAAgVIBKAAIAAAVIgZAAIAACFgAHDBOIAAiaIAWAAIAACagADuBOIgHgiIgeAAIgGAiIgXAAIAjiaIASAAIAjCagADNAXIAXAAIgMg2IAAAAgAA7BOIAAiaIAXAAIAACDIAsAAIAAAXgAAaBOIgGgiIgdAAIgHAiIgWAAIAjiaIARAAIAjCagAgFAXIAUAAIgKg2IgBAAgAi0BOIAAiFIgaAAIAAgVIBLAAIAAAVIgaAAIAACFgAjnBOIgVhDIgNAAIAABDIgXAAIAAiaIAkAAQAUgBAKAMQAKALAAAYQAAANgFAJQgEAJgLAFIAYBIgAkJgHIAMAAQAMABADgHQADgGAAgMQAAgMgDgGQgEgGgMAAIgLAAgAlNBOIAAiaIAXAAIAACagAmOBOIggiaIAXAAIASBrIABAAIAShrIAYAAIghCagAnxBOIgHgiIgeAAIgHAiIgWAAIAjiaIATAAIAiCagAoSAXIAXAAIgLg2IgBAAgAqkBOIAAiaIBEAAIAAAVIgtAAIAAAtIAnAAIAAATIgnAAIAAAuIAtAAIAAAXgArIBOIgVhDIgOAAIAABDIgWAAIAAiaIAkAAQAUgBAKAMQAKALAAAYQAAANgEAJQgFAJgLAFIAZBIgArrgHIAOAAQALABADgHQAEgGAAgMQAAgMgEgGQgDgGgNAAIgMAAgAstBOIAAiaIAWAAIAACagAtZBOIAAhGIgcAAIAABGIgXAAIAAiaIAXAAIAABCIAcAAIAAhCIAWAAIAACag");
	this.shape.setTransform(-10.5,0.2);

	this.instance = new lib.cloud3();
	this.instance.setTransform(-150,-73.2,1.163,1.163);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-150,-73.2,300,146.5);


(lib.mc23 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AUACvIgBAAIgBAAIgCAAQgMgBgKgIQgJgJAAgQIAAhLQAAgQAJgKQAKgIAMgBIACAAIABAAIABAAIABAAQAMABAKAIQAKAKAAAQIAAAHIgUAAIAAgHQAAgIgEgDQgFgEgFAAQgGAAgEAEQgEADAAAIIAABLQAAAHAEADQAEAFAGAAQAFAAAFgFQAEgDAAgHIAAgbIgRAAIAAgSIAlAAIAAAtQAAAQgKAJQgKAIgMABIgBAAgAidCmQgKgKAAgOIAAgHIAVAAIAAAFQAAAHADAEQADAEAIABQALAAACgHQACgFAAgKQAAgKgCgEQgBgEgGgDIgBAAIgCgBIgOgGQgOgFgEgJQgFgIAAgOQAAgQAJgMQAJgLARAAQAPAAAJAJQAKAKAAALIAAACIAAAKIgUAAIAAgFQAAgGgDgFQgEgFgHgBQgJAAgDAGQgCAGAAAGQAAAIABAFQACADAHADIABAAIAAABIAQAGQAOAFADAJQAEAKAAAPQAAASgHAMQgIAMgUgBQgPAAgKgJgAovCvIgDAAQgKgBgJgIQgJgGAAgSIAAhOQAAgOAJgKQAKgJAOAAQAPAAAJAJQAJAKAAAPIAAAIIgUAAIAAgHQAAgGgEgEQgDgFgGAAQgHAAgDAFQgCAFAAAHIAABIQAAAHACAEQADAEAHABIACgBIABAAQAEgBADgDQADgEAAgGIAAgHIAUAAIAAAIQAAAOgJAKQgKAKgNgBIgDAAgAsSCmQgKgKAAgOIAAhuIAVAAIAABsQAAAIADADQAEAFAFAAQAGAAADgFQAEgDAAgIIAAhsIAUAAIAABuQAAAOgJAKQgKAJgOAAQgNAAgKgJgAtOCvIgCAAIgBAAIgBAAQgMgBgKgIQgKgJAAgQIAAhLQAAgQAKgKQAKgIAMgBIABAAIABAAIACAAIABAAQAMABAKAIQAKAKAAAQIAABLQAAAQgKAJQgKAIgMABIgBAAgAtZA3QgEADAAAIIAABLQAAAHAEADQAEAFAFAAQAGAAAEgFQAEgDAAgHIAAhLQAAgIgEgDQgEgEgGAAQgFAAgEAEgAS4CuIgfhVIgBAAIAABVIgUAAIAAiOIAUAAIAeBWIABAAIAAhWIAUAAIAACOgARcCuIAAiOIAVAAIAACOgAQnCuIAAh7IgYAAIAAgTIBEAAIAAATIgYAAIAAB7gAPPCuIAAiOIA9AAIAAATIgoAAIAAAqIAjAAIAAATIgjAAIAAAqIAoAAIAAAUgAOxCuIgXg/IgLAVIAAAqIgVAAIAAiOIAVAAIAABBIAAAAIAehBIAUAAIgcA5IAhBVgANZCuIgTg9IgMAAIAAA9IgVAAIAAiOIAhAAQASAAAJAKQAJALAAAVQAAAMgEAJQgEAJgJAGIAWBAgAM6BfIALAAQALABADgGQADgGAAgKQAAgLgDgGQgDgGgMAAIgKAAgAMHCuIgGgeIgcAAIgGAeIgUAAIAfiOIARAAIAgCOgALpB8IAUAAIgKgzIAAAAgAKsCuIAAhWIgUA9IgKAAIgUg9IgBAAIAABWIgUAAIAAiOIATAAIAbBMIAAAAIAahMIAUAAIAACOgAH2CuIAAiOIAUAAIAAB6IApAAIAAAUgAHNCuIAAiOIAVAAIAACOgAGuCuIgGgeIgbAAIgGAeIgVAAIAgiOIARAAIAgCOgAGQB8IAVAAIgKgzIgBAAgAFUCuIAAhWIgVA9IgKAAIgUg9IAAAAIAABWIgVAAIAAiOIAUAAIAaBMIABAAIAZhMIAUAAIAACOgADCCuIAAiOIA9AAIAAATIgoAAIAAAqIAjAAIAAATIgjAAIAAAqIAoAAIAAAUgAB/CuIgGgeIgcAAIgGAeIgUAAIAfiOIARAAIAgCOgABhB8IAUAAIgKgzIAAAAgAAjCuIAAiOIAVAAIAACOgAgXCuIgdiOIAWAAIAQBjIABAAIAOhjIAWAAIgcCOgAjYCuIAAh7IgXAAIAAgTIBDAAIAAATIgXAAIAAB7gAkLCuIgfhVIgBAAIAABVIgUAAIAAiOIAUAAIAeBWIABAAIAAhWIAUAAIAACOgAmICuIAAiOIA9AAIAAATIgpAAIAAAqIAjAAIAAATIgjAAIAAAqIApAAIAAAUgAmxCuIAAiOIAVAAIAACOgAn7CuIAAiOIAUAAIAAB6IApAAIAAAUgAqTCuIgTg9IgMAAIAAA9IgVAAIAAiOIAhAAQASAAAJAKQAJALAAAVQAAAMgEAJQgEAJgJAGIAWBAgAqyBfIALAAQALABADgGQADgGAAgKQAAgLgDgGQgDgGgMAAIgKAAgAurCuIAAg8IgchSIAVAAIARA5IAAAAIARg5IAVAAIgcBSIAAA8gAwLCuIAAg/IgZAAIAAA/IgUAAIAAiOIAUAAIAAA9IAZAAIAAg9IAVAAIAACOgAxtCuIAAh7IgYAAIAAgTIBEAAIAAATIgYAAIAAB7gAyjCuIAAiOIAUAAIAACOgAzYCuIgQhcIAAAAIgQBcIgTAAIgViOIAWAAIAKBaIAAAAIAQhaIAQAAIAQBcIABAAIAKhcIAVAAIgVCOgASggeIgBAAIgBAAIgCAAQgMgBgKgJQgJgIAAgRIAAhLQAAgQAJgJQAKgJAMgBIACAAIABAAIABAAIABAAQAMABAKAJQAKAJAAAQIAABLQAAARgKAIQgKAJgMABIgBAAgASViWQgEADAAAHIAABLQAAAIAEADQAEAEAGAAQAFAAAFgEQAEgDAAgIIAAhLQAAgHgEgDQgFgEgFAAQgGAAgEAEgAONgeIgDgBQgKgBgJgHQgJgHAAgSIAAhNQAAgPAJgJQAKgKAOAAQAPAAAJAKQAJAJAAAQIAAAIIgUAAIAAgHQAAgGgEgFQgDgEgGAAQgHAAgDAFQgCAEAAAHIAABJQAAAGACAEQADAFAHAAIACAAIABAAQAEgBADgEQADgDAAgHIAAgHIAUAAIAAAJQAAAOgJAJQgKAKgNAAIgDAAgAKigoQgKgJAAgOIAAhvIAVAAIAABtQAAAHADAEQAEAEAFAAQAGAAADgEQAEgEAAgHIAAhtIAUAAIAABvQAAAOgJAJQgKAKgOAAQgNAAgKgKgAGIgeIgCAAIgBAAIgBAAQgMgBgKgJQgKgIAAgRIAAhLQAAgQAKgJQAKgJAMgBIABAAIABAAIACAAIABAAQAMABAKAJQAKAJAAAQIAABLQAAARgKAIQgKAJgMABIgBAAgAF9iWQgEADAAAHIAABLQAAAIAEADQAEAEAFAAQAGAAAEgEQAEgDAAgIIAAhLQAAgHgEgDQgEgEgGAAQgFAAgEAEgAExgeIgCgBQgLgBgJgHQgIgHAAgSIAAhNQAAgPAJgJQAJgKAPAAQAOAAAJAKQAKAJAAAQIAAAIIgVAAIAAgHQAAgGgDgFQgEgEgFAAQgIAAgCAFQgDAEAAAHIAABJQAAAGADAEQADAFAGAAIACAAIACAAQADgBADgEQADgDAAgHIAAgHIAVAAIAAAJQAAAOgKAJQgJAKgOAAIgDAAgAixgeIgBAAIgBAAIgCAAQgMgBgKgJQgJgIAAgRIAAhLQAAgQAJgJQAKgJAMgBIACAAIABAAIABAAIABAAQAMABAKAJQAKAJAAAQIAABLQAAARgKAIQgKAJgMABIgBAAgAi8iWQgEADAAAHIAABLQAAAIAEADQAEAEAGAAQAFAAAFgEQAEgDAAgIIAAhLQAAgHgEgDQgFgEgFAAQgGAAgEAEgArEgoQgKgKAAgOIAAgHIAVAAIAAAGQAAAGADAEQADAFAIAAQALAAACgGQACgGAAgKQAAgJgCgEQgBgEgGgDIgBgBIgCAAIgOgGQgOgGgEgIQgFgJAAgOQAAgQAJgLQAJgMARAAQAPAAAJAKQAKAJAAAMIAAABIAAALIgUAAIAAgFQAAgHgDgFQgEgFgHAAQgJAAgDAGQgCAFAAAHQAAAIABAEQACADAHADIABABIAAAAIAQAGQAOAFADAKQAEAJAAAPQAAATgHALQgIAMgUAAQgPAAgKgKgAtzgeIgBAAIgBAAIgCAAQgMgBgKgJQgJgIAAgRIAAhLQAAgQAJgJQAKgJAMgBIACAAIABAAIABAAIABAAQAMABAKAJQAKAJAAAQIAABLQAAARgKAIQgKAJgMABIgBAAgAt+iWQgEADAAAHIAABLQAAAIAEADQAEAEAGAAQAFAAAFgEQAEgDAAgIIAAhLQAAgHgEgDQgFgEgFAAQgGAAgEAEgAUGgfIgfhWIgBAAIAABWIgUAAIAAiPIAUAAIAeBXIABAAIAAhXIAUAAIAACPgARVgfIAAiPIAVAAIAACPgAQggfIAAh7IgYAAIAAgUIBEAAIAAAUIgYAAIAAB7gAP0gfIgGgeIgcAAIgGAeIgUAAIAfiPIARAAIAgCPgAPWhRIAUAAIgKgzIAAAAgANHgfIAAiPIAUAAIAACPgAMggfIgfhWIAAAAIAABWIgUAAIAAiPIATAAIAfBXIAAAAIAAhXIAVAAIAACPgAJxgfIAAhXIgVA9IgKAAIgUg9IAAAAIAABXIgVAAIAAiPIAUAAIAaBMIABAAIAZhMIAUAAIAACPgAICgfIAAhXIgVA9IgKAAIgUg9IAAAAIAABXIgVAAIAAiPIAUAAIAaBMIABAAIAZhMIAUAAIAACPgAClgfIAAiPIA9AAIAAAUIgpAAIAAAqIAjAAIAAATIgjAAIAAAqIApAAIAAAUgAB/gfIgfhWIgBAAIAABWIgUAAIAAiPIAUAAIAeBXIABAAIAAhXIAUAAIAACPgAAjgfIAAiPIAVAAIAACPgAglgfIAAiPIAUAAIAAB7IAnAAIAAAUgAhLgfIgfhWIgBAAIAABWIgUAAIAAiPIAUAAIAeBXIABAAIAAhXIAUAAIAACPgAkzgfIAAg8IgchTIAWAAIAQA5IABAAIAQg5IAWAAIgcBTIAAA8gAmbgfIAAiPIAeAAQASAAAJAKQAJAKAAARIAABDQAAAUgJAKQgKAJgSAAgAmGgyIAJAAQAJAAADgFQAEgEAAgJIAAhFQAAgIgDgEQgEgFgJAAIgJAAgAm5gfIgGgeIgcAAIgGAeIgUAAIAfiPIARAAIAgCPgAnXhRIAUAAIgKgzIAAAAgAo2gfIAAiPIA9AAIAAAUIgpAAIAAAqIAjAAIAAATIgjAAIAAAqIApAAIAAAUgAprgfIAAh7IgYAAIAAgUIBEAAIAAAUIgYAAIAAB7gAtAgfIAAiPIAeAAQAJABAGACQAHACAFAFQAFAGACAIQACAHAAANQAAAJgBAHQgBAHgEAFQgEAIgHADQgHAFgLAAIgLAAIAAA3gAsshqIAKAAQAMAAADgGQACgHAAgLQAAgKgCgHQgCgHgMAAIgLAAgAvfgfIAAiPIAUAAIAAB7IApAAIAAAUgAwpgfIAAiPIA9AAIAAAUIgpAAIAAAqIAjAAIAAATIgjAAIAAAqIApAAIAAAUgAxlgfIgdiPIAWAAIAQBkIABAAIAQhkIAWAAIgeCPgAzFgfIAAiPIA9AAIAAAUIgoAAIAAAqIAjAAIAAATIgjAAIAAAqIAoAAIAAAUgA0agfIAAiPIAeAAQASAAAJAKQAJAKAAARIAABDQAAAUgJAKQgKAJgSAAgA0FgyIAJAAQAJAAADgFQAEgEAAgJIAAhFQAAgIgDgEQgEgFgJAAIgJAAg");
	this.shape.setTransform(-5.7,1.4);

	this.instance = new lib.cloud5();
	this.instance.setTransform(-160.5,-73.5);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-160.5,-73.5,321,147);


(lib.mc22 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("AIkDDIgDgBQgMgBgJgIQgKgIAAgTIAAhXQAAgPAKgKQAKgLARAAQAPAAAKALQALALAAAQIAAAJIgXAAIAAgHQAAgIgEgEQgDgFgHAAQgIAAgDAFQgDAFAAAJIAABQQAAAGAEAFQADAEAHAAIACAAIACAAQAEgBADgDQADgEAAgHIAAgIIAXAAIAAAKQAAAOgLALQgKALgPAAIgDAAgAFnDAIgLgHIgBgCIgCgDIASgNIAFADQADACAEAAIADAAIADgBQADgBADgEQADgFAAgIIAAh0IAWAAIAAB4QAAAPgKAMQgKAKgSABQgJAAgGgDgAExDDIgCAAIgBAAIgCAAQgNgBgLgKQgLgJAAgSIAAhTQAAgSALgKQALgKANgBIACAAIABAAIACAAIABAAQANABALAKQALAKAAASIAABTQAAASgLAJQgLAKgNABIgBAAgAEkA+QgEADAAAJIAABTQAAAHAEAFQAFADAGAAQAGAAAFgDQAEgFAAgHIAAhTQAAgJgEgDQgFgEgGAAQgGAAgFAEgAhqDDIgDgBQgLgBgKgIQgKgIAAgTIAAhXQAAgPALgKQAKgLAQAAQAQAAAKALQAKALAAAQIAAAJIgWAAIAAgHQAAgIgEgEQgEgFgGAAQgJAAgCAFQgDAFAAAJIAABQQAAAGADAFQADAEAIAAIABAAIACAAQAEgBADgDQAEgEAAgHIAAgIIAWAAIAAAKQAAAOgKALQgLALgPAAIgDAAgAnmC4QgLgLAAgPIAAgJIAXAAIAAAIQAAAGADAFQAEAEAJAAQALABACgHQADgGAAgLQAAgLgCgFQgCgDgGgEIgBAAIgCgBIgQgHQgPgFgFgKQgEgKAAgOQAAgSAJgNQAKgNATAAQAQAAAKALQALALAAAMIAAABIAAABIAAALIgWAAIAAgFQAAgIgEgGQgDgFgJAAQgJAAgDAGQgDAHAAAHQAAAJACAEQACAEAHADIABABIABAAIARAHQAPAFAEALQAEAKAAARQAAATgIANQgJANgVABQgRAAgLgLgAJsDCIAAiIIgaAAIAAgVIBKAAIAAAVIgaAAIAACIgAGyDCIAAidIBDAAIAAAVIgtAAIAAAuIAnAAIAAAVIgnAAIAAAuIAtAAIAAAXgADmDCIgVhEIgNAAIAABEIgXAAIAAidIAkAAQAUAAAKAMQAKALAAAXQAAAOgEAJQgFAKgKAGIAYBIgADEBrIANAAQALAAADgHQAEgGAAgMQAAgLgEgHQgDgGgNAAIgLAAgABUDCIAAidIAiAAQAJAAAHADQAHACAGAGQAFAGACAIQADAIAAAPQAAAJgCAIQgBAIgEAGQgEAHgIAFQgIAEgMAAIgMAAIAAA+gABqBvIALAAQAOAAACgHQADgHAAgMQAAgLgDgIQgCgIgNAAIgMAAgAAADCIAAhGIgaAAIAABGIgXAAIAAidIAXAAIAABDIAaAAIAAhDIAWAAIAACdgAivDCIgVhEIgNAAIAABEIgXAAIAAidIAkAAQAUAAAKAMQAKALAAAXQAAAOgEAJQgFAKgKAGIAYBIgAjRBrIANAAQALAAADgHQAEgGAAgMQAAgLgEgHQgDgGgNAAIgLAAgAkJDCIgHgiIgeAAIgHAiIgWAAIAjidIASAAIAjCdgAkqCLIAWAAIgLg5IAAAAgAmTDCIAAidIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAo+DCIAAidIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgApiDCIgVhEIgOAAIAABEIgWAAIAAidIAkAAQAUAAAKAMQAKALAAAXQAAAOgFAJQgEAKgLAGIAYBIgAqFBrIANAAQAMAAADgHQADgGAAgMQAAgLgDgHQgEgGgMAAIgMAAgAkRgjIgDAAQgLgBgKgIQgKgJAAgSIAAhWQAAgQALgKQAKgLAQABQAQgBAKALQAKALAAAQIAAAJIgWAAIAAgHQAAgIgEgEQgEgFgGAAQgJAAgCAFQgDAGAAAHIAABQQAAAHADAEQADAGAIAAIABgBIACAAQAEgBADgEQAEgDAAgIIAAgHIAWAAIAAAJQAAAPgKALQgLAKgPAAIgDAAgAnmgtQgKgLAAgPIAAh5IAWAAIAAB3QAAAIAEAEQAEAFAGAAQAGAAAEgFQAEgEAAgIIAAh3IAWAAIAAB5QAAAPgKALQgLAKgPAAQgPAAgLgKgAIfgkIAAiHIgaAAIAAgVIBKAAIAAAVIgaAAIAACHgAG+gkIAAicIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAWgAGdgkIgahFIgMAXIAAAuIgWAAIAAicIAWAAIAABIIABAAIAghIIAXAAIgfA/IAlBdgAE8gkIgVhCIgNAAIAABCIgXAAIAAicIAkAAQAUAAAKALQAKAMAAAXQAAANgEAKQgFAKgKAGIAYBHgAEah6IANAAQALAAADgGQAEgHAAgLQAAgNgEgFQgDgHgNAAIgLAAgADigkIgHghIgeAAIgHAhIgWAAIAjicIASAAIAjCcgADBhbIAWAAIgLg4IAAAAgAB+gkIAAhfIAAAAIgXBDIgLAAIgWhDIAAAAIAABfIgXAAIAAicIAWAAIAdBTIAAAAIAdhTIAVAAIAACcgAgZgkIgHghIgeAAIgHAhIgWAAIAjicIASAAIAjCcgAg6hbIAWAAIgLg4IAAAAgAimgkIAAhFIgbAAIAABFIgXAAIAAicIAXAAIAABDIAbAAIAAhDIAXAAIAACcgAlagkIgjhdIAAAAIAABdIgXAAIAAicIAWAAIAiBeIAAAAIAAheIAXAAIAACcgAoSgkIgGghIgfAAIgGAhIgXAAIAjicIATAAIAjCcgAozhbIAXAAIgLg4IgBAAgAqbgkIAAicIAWAAIAACGIAtAAIAAAWg");
	this.shape.setTransform(-3.8,85.3);

	// Layer 1
	this.instance = new lib.cloud2();
	this.instance.setTransform(-123.5,-157);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-123.5,-157,247,314);


(lib.mc21 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("ACeDRQgHgHAAgLIAAgHIAQAAIAAAGQAAAEADAEQACADAHAAQAHAAADgEQABgFAAgIIgBgLQgCgDgEgCIgBgBIgBAAIgMgFQgLgEgDgIQgEgGABgLQAAgNAGgJQAHgKAPAAQALABAIAHQAHAIABAJIAAABIAAABIAAAIIgRAAIAAgFQAAgFgCgEQgDgEgGAAQgHAAgCAEQgDAFAAAFQAAAHACADQABADAFACIABAAIABABIANAFQAKAEADAHQAEAIAAAMQAAAPgHAJQgGAKgPAAQgMAAgJgJgAkdDaIgBAAIgBgBQgKAAgIgIQgIgGAAgNIAAg9QAAgNAIgGQAIgIAKgBIABAAIABAAIAAAAIABAAQALABAHAIQAIAGAAANIAAA9QAAANgIAGQgHAIgLAAIgBABIAAAAgAklB4QgEAEABAFIAAA9QgBAGAEADQADACAFAAQAEAAAEgCQACgDAAgGIAAg9QAAgFgCgEQgEgCgEAAQgFAAgDACgAqfDRQgIgHAAgLIAAgHIAQAAIAAAGQABAEACAEQACADAHAAQAIAAACgEQACgFAAgIIgCgLQgBgDgEgCIgBgBIgCAAIgMgFQgKgEgDgIQgEgGAAgLQAAgNAHgJQAGgKAPAAQALABAIAHQAIAIAAAJIAAABIAAABIAAAIIgQAAIAAgFQAAgFgDgEQgDgEgGAAQgHAAgCAEQgCAFAAAFQAAAHABADQABADAGACIAAAAIABABIANAFQAKAEAEAHQADAIAAAMQAAAPgGAJQgGAKgQAAQgMAAgIgJgAseDRQgIgHAAgLIAAgHIARAAIAAAGQgBAEADAEQACADAHAAQAIAAACgEQABgFAAgIIgBgLQgBgDgEgCIgBgBIgCAAIgMgFQgKgEgDgIQgEgGAAgLQAAgNAHgJQAHgKAOAAQALABAIAHQAHAIABAJIAAABIAAABIAAAIIgRAAIAAgFQAAgFgCgEQgDgEgGAAQgHAAgCAEQgCAFAAAFQAAAHABADQACADAFACIABAAIAAABIANAFQALAEADAHQADAIAAAMQAAAPgGAJQgGAKgQAAQgMAAgIgJgABeDZIAAhzIAyAAIAAAQIghAAIAAAiIAcAAIAAAOIgcAAIAAAiIAhAAIAAARgAA/DZIAAhGIAAAAIgQAxIgJAAIgQgxIAAAAIAABGIgRAAIAAhzIAQAAIAWA9IAVg9IAQAAIAABzgAgYDZIAAhzIAQAAIAABzgAhDDZIAAhjIgTAAIAAgQIA2AAIAAAQIgTAAIAABjgAhsDZIgZhFIAABFIgRAAIAAhzIAQAAIAZBGIAAhGIAQAAIAABzgAjADZIgMhKIAAAAIgMBKIgQAAIgRhzIASAAIAIBJIAAAAIAMhJIANAAIANBKIAIhKIASAAIgRBzgAl8DZIAAhzIAYAAQAOAAAHAJQAIAIAAANIAAA1QAAAQgIAIQgHAIgPAAgAlsDJIAIAAQAHAAADgDQACgEAAgHIAAg3QAAgGgCgFQgDgDgHAAIgIAAgAm5DZIAAhGIgBAAIgQAxIgIAAIgQgxIgBAAIAABGIgQAAIAAhzIAQAAIAVA9IAVg9IAQAAIAABzgAouDZIAAhzIAxAAIAAAQIghAAIAAAiIAdAAIAAAOIgdAAIAAAiIAhAAIAAARgApYDZIAAhjIgTAAIAAgQIA2AAIAAAQIgSAAIAABjgArTDZIAAgxIgWhCIARAAIANAuIABAAIANguIARAAIgXBCIAAAxgAnEAxQgHgHAAgMIAAgGIAQAAIAAAGQAAAEADAEQACADAHAAQAHAAADgEQABgFAAgIIgBgLQgCgDgEgCIgBAAIgBgBIgMgFQgLgCgDgHQgEgIABgKQAAgNAGgKQAHgIAPgBQALAAAIAIQAHAIABAJIAAAAIAAABIAAAIIgRAAIAAgDQAAgGgCgEQgDgEgGAAQgHAAgCAFQgDAEAAAGQAAAGACADQABADAFACIABAAIABABIANAFQAKAEADAFQAEAHAAANQAAAOgHAKQgGAJgPAAQgMABgJgJgAFHA4IAAhgIgTAAIAAgQIA2AAIAAAQIgSAAIAABggAEfA4IgZhCIAABCIgRAAIAAhwIAQAAIAZBDIAAhDIAQAAIAABwgAC7A4IAAhwIAwAAIAAAQIggAAIAAAiIAcAAIAAAMIgcAAIAAAiIAgAAIAAAQgACLA4IgXhwIASAAIAMBOIAOhOIARAAIgXBwgAA/A4IAAhwIAwAAIAAAQIggAAIAAAiIAcAAIAAAMIgcAAIAAAiIAgAAIAAAQgAAkA4IgPgwIgKAAIAAAwIgPAAIAAhwIAZAAQAOAAAIAJQAGAIAAARQAAAKgCAHQgEAFgHAEIASA0gAALgFIAJAAQAJAAACgEQADgFAAgJQAAgIgDgEQgCgFgKAAIgIAAgAhFA4IAAhwIAZAAQAHAAAFADQAFABAEAEQAEAFACAGQABAGABAKIgBANQgBAGgDACQgEAGgFADQgGADgIAAIgJAAIAAAsgAg0gCIAIAAQAKAAABgEQACgGAAgJQAAgHgCgGQgBgGgJAAIgJAAgAinA4IAAhwIAZAAQANABAIAHQAHAJAAANIAAA0QABAPgJAIQgHAHgOAAgAiWApIAIAAQAGAAAEgDQACgEAAgHIAAg1QAAgGgCgFQgEgDgGAAIgIAAgAjEA4IgahCIAABCIgQAAIAAhwIAQAAIAZBDIAAhDIAQAAIAABwgAkFA4IgGgYIgVAAIgGAYIgQAAIAahwIAOAAIAZBwgAkdARIAQAAIgIgogAmIA4IAAhwIAyAAIAAAQIghAAIAAAiIAcAAIAAAMIgcAAIAAAiIAhAAIAAAQgAnpA4IAAhwIARAAIAABwgAoIA4IAAhDIgBAAIgQAvIgIAAIgQgvIgBAAIAABDIgQAAIAAhwIAQAAIAVA7IAVg7IAQAAIAABwgApiA4IAAhwIARAAIAABwgAqBA4IgYhCIAABCIgRAAIAAhwIAQAAIAYBDIAAhDIARAAIAABwgArKA4IAAhwIARAAIAABwgArpA4IAAhDIgBAAIgQAvIgIAAIgQgvIgBAAIAABDIgQAAIAAhwIAQAAIAVA7IAVg7IAQAAIAABwgAMLhlIgBAAIgBAAQgJAAgIgIQgIgGAAgOIAAg7QAAgNAIgIQAIgGAJgCIABAAIABAAIACAAIABAAQAJACAIAGQAIAIAAANIAAA7QAAAOgIAGQgIAIgJAAIgBAAIgCAAgAMEjFQgDACgBAHIAAA7QABAHADACQADADAEAAQAFAAADgDQAEgCAAgHIAAg7QAAgHgEgCQgDgDgFAAQgEAAgDADgAH7hlIgBAAIgBAAQgJAAgIgIQgIgGAAgOIAAg7QAAgNAIgIQAIgGAJgCIABAAIABAAIABAAIABAAQAKACAIAGQAIAIAAANIAAA7QAAAOgIAGQgIAIgKAAIgBAAIgBAAgAH0jFQgDACAAAHIAAA7QAAAHADACQADADAEAAQAFAAADgDQADgCAAgHIAAg7QAAgHgDgCQgDgDgFAAQgEAAgDADgAEjhtQgIgHAAgLIAAhYIAQAAIAABWQAAAGADADQAEADADAAQAFAAADgDQACgDAAgGIAAhWIARAAIAABYQAAALgIAHQgIAIgLAAQgLAAgHgIgADfhtQgIgHAAgMIAAgFIARAAIAAAEQAAAFACADQADAEAGAAQAIAAACgFQACgEgBgIIgBgLQgBgDgFgCIgBAAIgBgBIgLgFQgLgEgEgHQgDgIAAgKQAAgNAHgKQAHgIAOgBQALAAAIAIQAHAIABAJIAAAAIAAABIAAAIIgRAAIAAgDQABgGgDgEQgCgEgHAAQgGAAgCAFQgDAEAAAGQAAAGACADQABADAFADIABAAIABAAIAMAFQAKAEAEAHQADAHAAANQAAAOgGAKQgHAJgPAAQgMAAgIgIgAAXhlIgDAAQgHgBgIgFQgFgHAAgOIAAg+QAAgMAFgHQAIgIAMAAQALABAIAHQAHAIAAAMIAAAGIgQAAIAAgFQAAgFgDgDQgDgEgEAAQgHAAgBAEQgCADgBAHIAAA5QAAAFADAEQACADAGAAIABAAIABAAQADgBACgDQADgCAAgGIAAgFIAQAAIAAAHQAAALgHAHQgIAIgLAAIgCAAgAjUhlIgCAAQgIgBgHgFQgIgHAAgOIAAg+QAAgMAIgHQAHgIAMAAQALABAIAHQAHAIAAAMIAAAGIgQAAIAAgFQAAgFgDgDQgDgEgEAAQgGAAgCAEQgCADAAAHIAAA5QAAAFACAEQADADAFAAIABAAIACAAQACgBADgDQACgCAAgGIAAgFIAQAAIAAAHQAAALgHAHQgIAIgLAAIgCAAgAqEhlIgBAAIAAAAQgKAAgIgIQgIgGAAgOIAAg7QAAgNAIgIQAIgGAKgCIAAAAIABAAIABAAIABAAQAKACAIAGQAIAIAAANIAAA7QAAAOgIAGQgIAIgKAAIgBAAIgBAAgAqLjFQgDACAAAHIAAA7QAAAHADACQADADAEAAQAFAAADgDQADgCAAgHIAAg7QAAgHgDgCQgDgDgFAAQgEAAgDADgALHhmIAAhiIgTAAIAAgPIA2AAIAAAPIgSAAIAABigAJ0hmIAAhiIgTAAIAAgPIA2AAIAAAPIgTAAIAABigAJOhmIgOgwIgKAAIAAAwIgRAAIAAhxIAbAAQAOgBAHAJQAHAJAAAQQAAAKgDAHQgDAHgIAEIASA0gAI2ilIAJAAQAJABACgFQACgFAAgJQAAgIgCgEQgDgFgJAAIgIAAgAGghmIAAhxIAZAAQAGAAAFACQAFABAFAEQAEAFACAGQABAGAAAKIAAANQgBAGgEAEQgDAGgGADQgFADgJAAIgIAAIAAAsgAGxihIAIAAQAJgBACgFQACgFAAgJQAAgIgCgFQgCgGgJAAIgIAAgAFfhmIAAhxIAZAAQAHAAAFACQAEABAFAEQAEAFACAGQABAGAAAKIAAANQgCAGgDAEQgDAGgGADQgFADgJAAIgIAAIAAAsgAFwihIAIAAQAJgBACgFQACgFAAgJQAAgIgCgFQgBgGgKAAIgIAAgACBhmIAAhxIAQAAIAABhIAhAAIAAAQgABphmIgFgYIgWAAIgFAYIgQAAIAZhxIAOAAIAZBxgABRiNIAQAAIgIgqgAgfhmIAAhxIARAAIAABxgAg9hmIgahEIAABEIgQAAIAAhxIAQAAIAZBEIAAhEIAQAAIAABxgAiGhmIAAgyIgUAAIAAAyIgQAAIAAhxIAQAAIAAAwIAUAAIAAgwIARAAIAABxgAkmhmIAAhxIAwAAIAAAPIggAAIAAAhIAcAAIAAAQIgcAAIAAAhIAgAAIAAAQgAlRhmIAAhiIgTAAIAAgPIA2AAIAAAPIgSAAIAABigAm1hmIAAhxIAyAAIAAAPIghAAIAAAhIAcAAIAAAQIgcAAIAAAhIAhAAIAAAQgAn4hmIAAhxIAYAAQAOAAAHAHQAIAIgBAOIAAA2QAAAPgHAIQgIAHgPAAgAnoh1IAHAAQAIAAACgEQADgDAAgHIAAg3QAAgGgDgEQgCgEgIAAIgHAAgAoZhmIAAhxIARAAIAABxgApIhmIgYhxIASAAIANBPIANhPIASAAIgYBxgAq5hmIgOgwIgLAAIAAAwIgQAAIAAhxIAbAAQAOgBAHAJQAHAJAAAQQAAAKgDAHQgDAHgIAEIASA0gArSilIAKAAQAJABACgFQACgFAAgJQAAgIgCgEQgDgFgJAAIgJAAgAsjhmIAAhxIAYAAQAIAAAEACQAGABAEAEQAEAFABAGQACAGAAAKIgBANQgBAGgCAEQgEAGgFADQgGADgJAAIgJAAIAAAsgAsTihIAIAAQALgBABgFQACgFAAgJQAAgIgCgFQgCgGgJAAIgJAAg");
	this.shape.setTransform(8.4,0.4,1.102,1.102);

	// Layer 1
	this.instance = new lib.cloud4();
	this.instance.setTransform(-136.3,85.6,1.049,1.049,0,180,0);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-136.3,-91.6,272.6,177.2);


(lib.mc20 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AGECxQgKgJAAgOIAAgHIATAAIAAAGQAAAFAEAEQADAFAHAAQAKgBACgFQACgFAAgKQAAgJgBgDQgCgEgFgDIgBgBIgCAAIgOgFQgMgGgFgIQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAJAIABAMIAAABIAAAKIgUAAIAAgEQAAgHgDgGQgDgEgHAAQgIAAgDAFQgDAGAAAHQAAAHACAEQACADAGADIABABIAAAAIAPAFQANAFAEAJQAEAIAAAPQAAASgIALQgHALgTAAQgOAAgJgJgAD4C6IgCAAIgBAAQgLgBgJgIQgKgIAAgQIAAhIQAAgPAKgIQAJgJALAAIABAAIACAAIABAAIABAAQAMAAAJAJQAJAIAAAPIAAAIIgTAAIAAgIQAAgGgEgEQgEgDgFAAQgGAAgDADQgEAEAAAGIAABIQAAAHAEADQADAEAGAAQAFAAAEgEQAEgDAAgHIAAgaIgQAAIAAgRIAjAAIAAArQAAAQgJAIQgJAIgMABIgBAAIgBAAgAjQC6IgDAAQgKgCgIgGQgIgIAAgQIAAhKQAAgOAIgIQAJgJAOAAQAOAAAJAJQAIAJAAAOIAAAIIgTAAIAAgHQAAgGgDgEQgDgEgGAAQgHAAgCAEQgDAFAAAHIAABFQAAAFADAEQACAFAHAAIABAAIACAAQADgCADgDQADgDAAgGIAAgHIATAAIAAAJQAAAMgIAKQgJAJgNAAIgDAAgAlXCxQgJgJAAgOIAAgHIATAAIAAAGQAAAFADAEQADAFAIAAQAJgBADgFQACgFAAgKQAAgJgCgDQgBgEgGgDIgBgBIgCAAIgNgFQgNgGgEgIQgEgIAAgNQAAgQAIgKQAIgLARAAQAOAAAJAJQAIAIABAMIAAABIAAAKIgTAAIAAgEQAAgHgDgGQgEgEgHAAQgIAAgDAFQgCAGAAAHQAAAHABAEQACADAGADIABABIABAAIAPAFQANAFADAJQAEAIAAAPQAAASgHALQgIALgSAAQgOAAgKgJgAp+CxQgKgJAAgOIAAgHIATAAIAAAGQAAAFAEAEQADAFAHAAQAKgBACgFQACgFAAgKQAAgJgBgDQgCgEgFgDIgBgBIgCAAIgOgFQgMgGgFgIQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAJAIABAMIAAABIAAAKIgUAAIAAgEQAAgHgDgGQgDgEgHAAQgIAAgDAFQgDAGAAAHQAAAHACAEQACADAGADIABABIAAAAIAPAFQANAFAEAJQAEAIAAAPQAAASgIALQgHALgTAAQgOAAgJgJgAFaC5IgehRIAAAAIAABRIgTAAIAAiHIASAAIAdBSIABAAIAAhSIATAAIAACHgACyC5IAAiHIATAAIAACHgACUC5IgFgdIgbAAIgFAdIgUAAIAeiHIARAAIAeCHgAB4CJIATAAIgJgwIgBAAgAAWC5IAAiHIAdAAQAIAAAGACQAHACAFAFQAFAGABAGQACAIAAAMIgBAPQgBAGgDAGQgEAGgHAEQgHAEgKAAIgKAAIAAA1gAAqByIAJAAQAMAAACgHQACgGAAgKQAAgKgCgGQgCgHgLAAIgKAAgAgNC5IAAhSIgBAAIgTA6IgJAAIgTg6IgBAAIAABSIgTAAIAAiHIASAAIAZBIIABAAIAYhIIARAAIAACHgAhuC5IgGgdIgaAAIgGAdIgTAAIAeiHIAQAAIAeCHgAiLCJIATAAIgJgwIAAAAgAmjC5IAAiHIA6AAIAAASIgnAAIAAAnIAiAAIAAATIgiAAIAAAoIAnAAIAAATgAnqC5IAAiHIAUAAIAAB0IAmAAIAAATgAoHC5IgFgdIgbAAIgFAdIgUAAIAeiHIARAAIAeCHgAojCJIATAAIgJgwIgBAAgAIhgvIgDgBQgKgBgIgHQgIgHAAgRIAAhJQAAgOAIgJQAJgJAOAAQAOAAAJAJQAIAKAAANIAAAIIgTAAIAAgHQAAgFgDgFQgDgDgGAAQgHgBgCAFQgDAFAAAGIAABFQAAAGADAEQACAEAHAAIABAAIACAAQADgBADgDQADgDAAgHIAAgGIATAAIAAAIQAAANgIAJQgJAKgNAAIgDAAgAANgvIgCAAIgBgBQgKgBgIgHQgKgJAAgQIAAhHQAAgQAKgIQAIgIAKgBIABAAIACAAIABAAIABAAQAMABAJAIQAJAIAAAQIAAAHIgTAAIAAgHQAAgHgEgDQgEgDgFAAQgGAAgDADQgEADAAAHIAABHQAAAHAEAEQADADAGAAQAFAAAEgDQAEgEAAgHIAAgZIgQAAIAAgRIAjAAIAAAqQAAAQgJAJQgJAHgMABIgBABIgBAAgAkvgvIgCgBQgKgBgIgHQgJgHAAgRIAAhJQAAgOAJgJQAJgJAOAAQANAAAJAJQAJAKAAANIAAAIIgTAAIAAgHQAAgFgEgFQgDgDgFAAQgIgBgCAFQgCAFAAAGIAABFQAAAGACAEQADAEAGAAIACAAIABAAQAEgBADgDQADgDAAgHIAAgGIATAAIAAAIQAAANgJAJQgJAKgNAAIgDAAgAnng4QgJgJAAgOIAAhpIATAAIAABnQAAAHAEADQADAEAFAAQAFAAAEgEQADgDAAgHIAAhnIAUAAIAABpQAAAOgKAJQgJAIgNABQgNgBgJgIgAJfgwIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAHogwIgGgeIgaAAIgGAeIgTAAIAeiIIAQAAIAeCIgAHLhgIATAAIgJgxIAAAAgAFqgwIAAiIIAdAAQAIAAAGADQAGABAFAGQAFAFACAHQACAHAAAMIgBAQQgBAGgEAFQgEAHgGAEQgHAEgLAAIgKAAIAAA1gAF9h3IAKAAQALAAACgHQADgGAAgLQAAgJgCgHQgCgGgMAAIgKAAgAFEgwIAAhTIAAAAIgTA7IgKAAIgTg7IAAAAIAABTIgUAAIAAiIIATAAIAZBIIAAAAIAZhIIATAAIAACIgADagwIAAiIIATAAIAACIgABqgwIAAg8IgYAAIAAA8IgUAAIAAiIIAUAAIAAA6IAYAAIAAg6IATAAIAACIgAg3gwIAAiIIATAAIAACIgAhdgwIAAg8IgYAAIAAA8IgUAAIAAiIIAUAAIAAA6IAYAAIAAg6IATAAIAACIgAjSgwIAAg8IgYAAIAAA8IgTAAIAAiIIATAAIAAA6IAYAAIAAg6IAUAAIAACIgAlvgwIgdhSIgBAAIAABSIgTAAIAAiIIATAAIAdBSIAAAAIAAhSIAUAAIAACIgAoNgwIgGgeIgaAAIgGAeIgTAAIAeiIIAQAAIAeCIgAoqhgIATAAIgJgxIAAAAgAqEgwIAAiIIATAAIAAB0IAnAAIAAAUgACPhbIAAgUIA5AAIAAAUg");
	this.shape.setTransform(-8.5,-2.1);

	// Layer 1
	this.instance = new lib.cloud3();
	this.instance.setTransform(-141.6,-69.1,1.098,1.098);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-141.6,-69.1,283.2,138.3);


(lib.mc19 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AFlEnQgKgKAAgNIAAgHIATAAIAAAGQAAAFADAEQAEAEAHAAQAKAAACgFQACgFAAgKQAAgJgCgEQgBgEgGgCIgBgBIgBAAIgOgGQgNgFgEgIQgEgJAAgNQAAgPAIgLQAJgLAQAAQAOAAAJAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgEgGAAQgIAAgEAFQgCAGAAAGQAAAIABADQACAEAGADIABAAIABAAIAPAGQANAFAEAJQADAIAAAPQABARgIALQgIAMgSAAQgOAAgJgJgADYEwIgBAAIgBgBQgLAAgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgJALgBIABAAIABAAIACAAIABAAQALABAKAJQAJAIAAAPIAABIQAAAPgJAJQgKAIgLAAIgBABIgCAAgADPC9QgEADAAAHIAABIQAAAHAEADQAEADAFAAQAGAAADgDQAFgDAAgHIAAhIQAAgHgFgDQgDgDgGAAQgFAAgEADgAAMEnQgKgJAAgOIAAhpIAUAAIAABnQgBAHAEAEQAEADAEAAQAFAAAEgDQADgEAAgHIAAhnIAUAAIAABpQAAAOgJAJQgKAJgNAAQgNAAgIgJgAh0EwIgCAAIgBgBQgLAAgJgIQgJgJAAgPIAAhIQAAgPAJgIQAJgJALgBIABAAIACAAIABAAIACAAQALABAJAJQAKAIAAAPIAABIQAAAPgKAJQgJAIgLAAIgCABIgBAAgAh9C9QgEADAAAHIAABIQAAAHAEADQADADAGAAQAGAAADgDQAEgDAAgHIAAhIQAAgHgEgDQgDgDgGAAQgGAAgDADgAjaEnQgKgKAAgNIAAgHIATAAIAAAGQAAAFADAEQAEAEAHAAQAKAAACgFQACgFAAgKQAAgJgBgEQgCgEgGgCIgBgBIgBAAIgOgGQgNgFgEgIQgEgJAAgNQAAgPAIgLQAJgLAQAAQAOAAAJAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgEgGAAQgIAAgEAFQgCAGAAAGQAAAIABADQADAEAFADIABAAIABAAIAPAGQANAFAEAJQADAIAAAPQAAARgHALQgIAMgSAAQgOAAgJgJgApMEnQgJgJAAgOIAAhpIATAAIAABnQAAAHADAEQAEADAFAAQAFAAADgDQAEgEAAgHIAAhnIAUAAIAABpQAAAOgKAJQgJAJgNAAQgNAAgJgJgAqHEwIgDgBQgJgBgJgHQgIgHAAgQIAAhKQAAgOAIgJQAKgJAOAAQANAAAJAJQAIAKAAAOIAAAHIgSAAIAAgGQgBgGgDgEQgDgEgFAAQgIAAgCAEQgCAFgBAHIAABFQABAFACAEQADAEAGAAIACAAIABAAQAEgBADgDQADgDAAgGIAAgHIASAAIAAAJQAAAMgIAJQgJAKgNAAIgDAAgAsxEnQgJgKAAgNIAAgHIATAAIAAAGQAAAFAEAEQACAEAIAAQAJAAADgFQACgFAAgKQAAgJgCgEQgBgEgFgCIgBgBIgDAAIgNgGQgMgFgFgIQgEgJAAgNQAAgPAIgLQAIgLARAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgEgIAAQgHAAgDAFQgDAGAAAGQAAAIACADQABAEAHADIABAAIAAAAIAPAGQANAFADAJQAFAIAAAPQAAARgIALQgHAMgTAAQgOAAgKgJgAE6EvIgdhSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIAAAAIAAhSIAUAAIAACIgACTEvIAAiIIATAAIAACIgABgEvIAAh1IgWAAIAAgTIBAAAIAAATIgXAAIAAB1gAhCEvIAAiIIATAAIAAB0IAmAAIAAAUgAk8EvIAAg5IgahPIAUAAIAQA2IAAAAIAQg2IAUAAIgaBPIAAA5gAl/EvIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gAmzEvIAAiIIATAAIAACIgAnUEvIgSg6IgLAAIAAA6IgTAAIAAiIIAfAAQARAAAIALQAJAJAAAUQAAAMgDAIQgFAJgJAFIAVA+gAnxDkIALAAQAKAAADgGQACgFABgKQAAgKgDgGQgEgFgKAAIgKAAgArqEvIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAATIghAAIAAAnIAnAAIAAAUgAuMEvIAAh1IgWAAIAAgTIBAAAIAAATIgXAAIAAB1gAu/EvIAAiIIATAAIAACIgAOhBEIgCAAIgBAAQgLgBgKgIQgJgIAAgQIAAhFQAAgQAJgIQAKgIALgBIABAAIACAAIABAAIABAAQALABAJAIQAKAIAAAQIAAAHIgUAAIAAgHQAAgHgDgDQgEgEgFAAQgGAAgDAEQgFADAAAHIAABFQAAAHAFAEQADADAGAAQAFAAAEgDQADgEAAgHIAAgZIgPAAIAAgPIAjAAIAAAoQAAAQgKAIQgJAIgLABIgBAAIgBAAgAEeA7QgKgJAAgNIAAhnIAUAAIAABlQAAAHAEADQADAEAFAAQAFAAAEgEQADgDAAgHIAAhlIATAAIAABnQAAANgJAJQgJAJgNAAQgNAAgJgJgADjBEIgCAAIgBAAQgLgBgJgIQgJgIAAgQIAAhFQAAgQAJgIQAJgIALgBIABAAIACAAIABAAIACAAQALABAJAIQAJAIAAAQIAABFQAAAQgJAIQgJAIgLABIgCAAIgBAAgADagsQgEADAAAHIAABFQAAAHAEAEQADADAGAAQAGAAADgDQAEgEAAgHIAAhFQAAgHgEgDQgDgEgGAAQgGAAgDAEgAAeBEIgCAAIgBAAQgLgBgKgIQgHgIAAgQIAAhFQAAgQAHgIQAKgIALgBIABAAIACAAIABAAIABAAQAMABAIAIQAKAIAAAQIAAAHIgUAAIAAgHQAAgHgDgDQgEgEgFAAQgGAAgDAEQgFADAAAHIAABFQAAAHAFAEQADADAGAAQAFAAAEgDQADgEAAgHIAAgZIgPAAIAAgPIAjAAIAAAoQAAAQgKAIQgIAIgMABIgBAAIgBAAgAhIA7QgJgJAAgNIAAhnIATAAIAABlQAAAHAEADQADAEAGAAQAFAAADgEQADgDAAgHIAAhlIAUAAIAABnQAAANgKAJQgJAJgMAAQgNAAgKgJgAiCBEIgCAAIgBAAQgLgBgKgIQgJgIAAgQIAAhFQAAgQAJgIQAKgIALgBIABAAIACAAIABAAIABAAQAMABAIAIQAKAIAAAQIAABFQAAAQgKAIQgIAIgMABIgBAAIgBAAgAiLgsQgFADAAAHIAABFQAAAHAFAEQADADAGAAQAFAAAEgDQADgEAAgHIAAhFQAAgHgDgDQgEgEgFAAQgGAAgDAEgAnuA7QgJgJAAgOIAAgHIATAAIAAAGQAAAFAEAFQADAEAHAAQAKAAACgFQACgGAAgJQAAgKgCgDQgBgEgFgDIgCAAIgCgBIgNgFQgMgDgFgJQgEgIAAgNQAAgQAIgKQAIgLARAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgEgFgHAAQgIAAgCAGQgDAFAAAHQAAAHACAEQABADAHADIABABIAAAAIAPAGQANAEADAHQAFAJAAAPQAAARgIALQgHALgTAAQgOAAgKgJgANdBDIgdhPIgBAAIAABPIgTAAIAAiFIATAAIAdBQIAAAAIAAhQIATAAIAACFgAMGBDIAAiFIAUAAIAACFgAK0BDIAAiFIAeAAQAQAAAIAJQAJAKAAAQIAAA9QAAATgJAJQgJAJgSAAgALIAxIAJAAQAIAAAEgEQADgEAAgJIAAg/QAAgIgDgEQgDgFgJAAIgJAAgAKXBDIgFgdIgaAAIgGAdIgTAAIAdiFIARAAIAeCFgAJ7AUIATAAIgJgvIAAAAgAIhBDIAAiFIA5AAIAAASIgmAAIAAAoIAiAAIAAAQIgiAAIAAAoIAmAAIAAATgAHaBDIAAiFIATAAIAAByIAnAAIAAATgAGXBDIgSg6IgMAAIAAA6IgUAAIAAiFIAgAAQARAAAIAKQAJAKAAAUQAAALgDAJQgFAGgIAGIAUA9gAF5gGIALAAQAKAAADgFQADgGgBgKQAAgKgCgFQgDgGgLAAIgKAAgAB6BDIAAg8IgYAAIAAA8IgTAAIAAiFIATAAIAAA6IAYAAIAAg6IAUAAIAACFgAjCBDIgSg6IgMAAIAAA6IgTAAIAAiFIAgAAQAQAAAJAKQAJAKgBAUQAAALgDAJQgEAGgJAGIAVA9gAjggGIAMAAQAJAAADgFQADgGAAgKQAAgKgDgFQgDgGgLAAIgKAAgAkZBDIAAg8IgYAAIAAA8IgTAAIAAiFIATAAIAAA6IAYAAIAAg6IAUAAIAACFgAl2BDIAAhzIgXAAIAAgSIBAAAIAAASIgVAAIAABzgAoUBDIgSg6IgLAAIAAA6IgTAAIAAiFIAfAAQARAAAIAKQAJAKAAAUQAAALgDAJQgFAGgJAGIAVA9gAoxgGIALAAQAKAAADgFQACgGABgKQAAgKgEgFQgDgGgKAAIgKAAgAqLBDIAAiFIA6AAIAAASIgnAAIAAAoIAiAAIAAAQIgiAAIAAAoIAnAAIAAATgAq9BDIAAhzIgsAAIAABzIgTAAIAAhzIgWAAIAAgSIB/AAIAAASIgXAAIAABzgAsmBDIgGgdIgaAAIgGAdIgTAAIAeiFIAQAAIAfCFgAtDAUIAUAAIgJgvIgBAAgAt8BDIAAhQIgBAAIgSA4IgKAAIgTg4IAAAAIAABQIgUAAIAAiFIATAAIAYBGIABAAIAYhGIATAAIAACFgAEDilIgBAAIgBgBQgLAAgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgJALgBIABAAIABAAIABAAIACAAQALABAKAJQAJAIAAAPIAABIQAAAPgJAJQgKAIgLAAIgCABIgBAAgAD6kYQgDADAAAHIAABIQAAAHADADQAEADAFAAQAGAAADgDQAFgDAAgHIAAhIQAAgHgFgDQgDgDgGAAQgFAAgEADgAi5ilIgCAAIgBgBQgLAAgKgIQgJgJAAgPIAAhIQAAgPAJgIQAKgJALgBIABAAIACAAIABAAIABAAQALABAJAJQAKAIAAAPIAABIQAAAPgKAJQgJAIgLAAIgBABIgBAAgAjCkYQgFADAAAHIAABIQAAAHAFADQADADAGAAQAFAAAEgDQADgDAAgHIAAhIQAAgHgDgDQgEgDgFAAQgGAAgDADgAovilIgCgBQgLgBgHgHQgJgHAAgQIAAhKQAAgOAJgJQAIgJAOAAQAOAAAJAJQAJAKAAAOIAAAHIgTAAIAAgGQAAgGgEgEQgDgEgGAAQgHAAgCAEQgCAFgBAHIAABFQABAFACAEQACAEAHAAIABAAIACAAQADgBADgDQAEgDAAgGIAAgHIATAAIAAAJQAAAMgJAJQgJAKgNAAIgDAAgAsCilIgCAAIgBgBQgLAAgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgJALgBIABAAIACAAIABAAIACAAQALABAJAJQAJAIAAAPIAABIQAAAPgJAJQgJAIgLAAIgCABIgBAAgAsLkYQgEADAAAHIAABIQAAAHAEADQADADAGAAQAGAAADgDQAEgDAAgHIAAhIQAAgHgEgDQgDgDgGAAQgGAAgDADgAKXimIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAJtimIgFgdIgaAAIgGAdIgUAAIAeiIIARAAIAeCIgAJRjWIATAAIgJgxIgBAAgAIYimIAAg8IgZAAIAAA8IgTAAIAAiIIATAAIAAA6IAZAAIAAg6IATAAIAACIgAG6imIAAh1IgWAAIAAgTIA/AAIAAATIgVAAIAAB1gAFlimIgdhSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIATAAIAACIgAC9imIAAiIIAUAAIAACIgACLimIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gABhimIgFgdIgbAAIgFAdIgUAAIAeiIIAQAAIAeCIgABEjWIATAAIgJgxIAAAAgAALimIAAhTIAAAAIgSA7IgJAAIgTg7IgBAAIAABTIgTAAIAAiIIASAAIAaBIIAAAAIAXhIIATAAIAACIgAhXimIgSg6IgMAAIAAA6IgTAAIAAiIIAgAAQAQAAAJALQAJAJgBAUQABAMgEAIQgEAJgJAFIAVA+gAh1jxIALAAQALAAADgGQACgFAAgKQAAgKgDgGQgDgFgLAAIgKAAgAkgimIAAiIIA7AAIAAATIgnAAIAAApIAhAAIAAASIghAAIAAA6gAlEimIgdhSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIAAAAIAAhSIAUAAIAACIgAmaimIAAiIIATAAIAACIgAnximIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAqSimIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAATIghAAIAAAnIAnAAIAAAUgArDimIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gAtBimIgSg6IgMAAIAAA6IgUAAIAAiIIAgAAQARAAAIALQAJAJAAAUQAAAMgEAIQgEAJgIAFIAUA+gAtfjxIALAAQAKAAADgGQADgFgBgKQAAgKgCgGQgDgFgLAAIgKAAgAvAimIAAiIIAdAAQAIAAAGADQAGACAGAFQAFAFABAHQACAIAAAMIgBAPQgBAGgDAGQgEAGgHAEQgHAEgKAAIgKAAIAAA1gAusjtIAJAAQALAAACgHQADgGAAgKQAAgKgCgGQgCgHgLAAIgKAAg");
	this.shape.setTransform(12,-22.4);

	this.instance = new lib.cloud1();
	this.instance.setTransform(157,-98,1,1,0,0,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-157,-98,314,196);


(lib.mc18 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ANNCaIgBAAIgBAAIgBAAQgKgBgIgHQgHgGAAgNIAAg7QAAgNAHgGQAIgIAKAAIABAAIABAAIABAAIABAAQAJAAAHAIQAIAGAAANIAAA7QAAANgIAGQgHAHgJABIgBAAgANEA7QgDADAAAGIAAA7QAAAGADACQAEADAEAAQAEAAAEgDQADgCAAgGIAAg7QAAgGgDgDQgEgCgEAAQgEAAgEACgAJyCSQgIgHAAgLIAAgGIAQAAIAAAFQAAAEADADQACAEAGAAQAIAAACgFQACgEAAgIQAAgHgCgDQgBgDgEgDIgBAAIgBgBIgMgEQgKgFgDgGQgEgIAAgKQAAgMAHgJQAHgJANAAQAMAAAHAIQAHAHABAJIAAAJIgQAAIAAgEQAAgFgDgEQgCgFgGABQgHAAgCAEQgCAFAAAFIABAJQABADAGADIABAAIAMAFQALADADAIQADAHAAAMQAAAOgGAKQgGAIgQABQgLAAgIgIgAA3CaIgCAAQgIgCgHgFQgHgGAAgNIAAg9QAAgMAHgGQAIgIALAAQALAAAIAIQAHAGAAAMIAAAHIgQAAIAAgGQAAgFgDgDQgCgDgFAAQgGAAgCADQgCAEAAAFIAAA6QAAAEACADQADAEAFAAIABAAIACAAQACgBACgDQADgCAAgFIAAgGIAQAAIAAAHQAAALgHAHQgIAIgLAAIgCAAgAiiCaIgBAAIgBAAIgBAAQgJgBgIgHQgHgGAAgNIAAg7QAAgNAHgGQAIgIAJAAIABAAIABAAIABAAIACAAQAJAAAHAIQAIAGAAANIAAA7QAAANgIAGQgHAHgJABIgCAAgAiqA7QgDADAAAGIAAA7QAAAGADACQADADAEAAQAFAAADgDQADgCAAgGIAAg7QAAgGgDgDQgDgCgFAAQgEAAgDACgAlYCaIgBAAIgBAAIgBAAQgKgBgHgHQgIgGAAgNIAAg7QAAgNAIgGQAHgIAKAAIABAAIABAAIABAAIABAAQAJAAAIAIQAHAGAAANIAAA7QAAANgHAGQgIAHgJABIgBAAgAlhA7QgDADAAAGIAAA7QAAAGADACQAEADAEAAQAEAAAEgDQADgCAAgGIAAg7QAAgGgDgDQgEgCgEAAQgEAAgEACgAP2CZIAAhwIAYAAQANAAAHAIQAIAIAAAOIAAA0QAAAPgIAIQgHAHgOAAgAQGCKIAIAAQAGAAADgEQADgDAAgHIAAg1QAAgHgCgEQgDgDgHAAIgIAAgAPcCZIgPgwIgJAAIAAAwIgQAAIAAhwIAaAAQAOAAAHAJQAHAJAAAQQAAAJgDAHQgDAHgIAFIARAygAPEBcIAJAAQAIAAADgFQACgFAAgHQAAgJgDgFQgCgEgJAAIgIAAgAOcCZIgEgYIgWAAIgFAYIgPAAIAYhwIANAAIAZBwgAOFByIAQAAIgIgogALvCZIAAhwIAYAAQAPAAAGAJQAHAJAAAKIAAAFQAAAIgDAFQgDAEgFAEQAFADADAFQADAFAAAIIAAAHQAAAPgHAHQgHAIgQAAgAL/CJIAHAAQAKAAACgGQACgEAAgJQAAgHgDgGQgCgDgJAAIgHAAgAL/BXIAIAAQAHABADgFQADgDAAgIQAAgHgDgFQgDgDgIAAIgHAAgALQCZIAAgyIgTAAIAAAyIgQAAIAAhwIAQAAIAAAwIATAAIAAgwIAQAAIAABwgAJWCZIgFgYIgWAAIgEAYIgQAAIAYhwIAOAAIAYBwgAI+ByIAQAAIgIgogAHrCZIAAhwIAYAAQANAAAHAIQAIAIAAAOIAAA0QAAAPgIAIQgHAHgOAAgAH7CKIAIAAQAGAAADgEQADgDAAgHIAAg1QAAgHgDgEQgCgDgHAAIgIAAgAGUCZIAAhwIAQAAIAABgIAgAAIAAAQgAF9CZIgFgYIgWAAIgEAYIgQAAIAZhwIANAAIAZBwgAFmByIAPAAIgIgogAE7CZIgPgwIgKAAIAAAwIgQAAIAAhwIAaAAQAOAAAHAJQAHAJAAAQQAAAJgDAHQgDAHgIAFIASAygAEiBcIAJAAQAJAAACgFQACgFAAgHQAAgJgCgFQgCgEgJAAIgJAAgADpCZIAAhgIgSAAIAAgQIA1AAIAAAQIgTAAIAABggADBCZIgYhDIAABDIgQAAIAAhwIAPAAIAYBEIAAhEIAQAAIAABwgABfCZIAAhwIAwAAIAAAQIgfAAIAAAgIAbAAIAAAPIgbAAIAAAhIAfAAIAAAQgAg1CZIAAhwIAwAAIAAAQIggAAIAAAgIAcAAIAAAPIgcAAIAAAhIAgAAIAAAQgAhSCZIgYhDIAABDIgRAAIAAhwIAQAAIAYBEIAAhEIAQAAIAABwgAj4CZIAAhEIgBAAIgPAwIgIAAIgQgwIAAAAIAABEIgRAAIAAhwIAQAAIAUA8IABAAIAUg8IAPAAIAABwgAmNCZIgPgwIgJAAIAAAwIgRAAIAAhwIAaAAQAOAAAHAJQAHAJAAAQQAAAJgDAHQgDAHgIAFIASAygAmlBcIAIAAQAJAAACgFQACgFAAgHQAAgJgCgFQgCgEgJAAIgIAAgAnvCZIAAhwIAwAAIAAAQIggAAIAAAiIAbAAIAAAOIgbAAIAAAwgApGCZIAAhwIAwAAIAAAQIggAAIAAAgIAcAAIAAAPIgcAAIAAAhIAgAAIAAAQgApgCZIgPgwIgKAAIAAAwIgQAAIAAhwIAaAAQAOAAAHAJQAHAJAAAQQAAAJgDAHQgDAHgIAFIARAygAp5BcIAJAAQAJAAACgFQACgFAAgHQAAgJgCgFQgCgEgJAAIgJAAgArCCZIAAhwIAvAAIAAAQIgfAAIAAAgIAbAAIAAAPIgbAAIAAAhIAfAAIAAAQgArhCZIAAgyIgUAAIAAAyIgQAAIAAhwIAQAAIAAAwIAUAAIAAgwIAPAAIAABwgAsuCZIgMhJIgBAAIgMBJIgOAAIgRhwIARAAIAIBHIANhHIAMAAIANBJIAAAAIAIhJIARAAIgRBwgAuOCZIAAgvIgWhBIARAAIANAtIAAAAIANgtIARAAIgWBBIAAAvgAu6CZIgZhDIAABDIgQAAIAAhwIAQAAIAXBEIAAhEIARAAIAABwgAv6CZIgFgYIgVAAIgFAYIgQAAIAZhwIANAAIAZBwgAwRByIAPAAIgIgogAKvg5IAPAAIAAARIgPALgACygwQgIgHAAgLIAAgGIAQAAIAAAFQAAAEADAEQADADAFAAQAJAAABgEQACgEAAgJQAAgHgBgDQgBgDgEgCIgBgBIgCAAIgLgEQgLgFgDgGQgEgIAAgKQAAgNAHgJQAHgIANgBQAMAAAHAIQAHAHABAJIAAAJIgQAAIAAgDQAAgGgCgDQgDgFgGAAQgGAAgCAFQgDAEAAAGQAAAGACADQABACAFADIABAAIAMAGQALADADAHQADAIAAALQAAAPgGAJQgGAJgQAAQgLAAgIgIgAgogoIgBAAIgBAAIgBAAQgJAAgIgIQgHgGAAgMIAAg8QAAgNAHgGQAIgHAJgBIABAAIABAAIABAAIABAAQAKABAHAHQAIAGAAANIAAA8QAAAMgIAGQgHAIgKAAIgBAAgAgxiGQgDADAAAFIAAA8QAAAFADADQAEACAEAAQAEAAAEgCQADgDAAgFIAAg8QAAgFgDgDQgEgDgEAAQgEAAgEADgAnngwQgIgHAAgLIAAhWIAQAAIAABVQAAAGADADQADACAEAAQAEAAADgCQADgDAAgGIAAhVIAQAAIAABWQAAALgIAHQgHAIgLAAQgKAAgIgIgAoWgoIgBAAIgBAAIgBAAQgKAAgIgIQgHgGAAgMIAAg8QAAgNAHgGQAIgHAKgBIABAAIABAAIABAAIABAAQAJABAHAHQAIAGAAANIAAA8QAAAMgIAGQgHAIgJAAIgBAAgAofiGQgDADAAAFIAAA8QAAAFADADQADACAFAAQAEAAADgCQAEgDAAgFIAAg8QAAgFgEgDQgDgDgEAAQgFAAgDADgArsgoIgBAAIgBAAIgBAAQgKAAgHgIQgIgGAAgMIAAg8QAAgNAIgGQAHgHAKgBIABAAIABAAIABAAIABAAQAJABAIAHQAHAGAAANIAAAGIgPAAIAAgGQAAgFgDgDQgEgDgEAAQgEAAgEADQgDADAAAFIAAA8QAAAFADADQAEACAEAAQAEAAAEgCQADgDAAgFIAAgWIgNAAIAAgOIAcAAIAAAkQAAAMgHAGQgIAIgJAAIgBAAgAJ1goIAAhwIAwAAIAAAPIgfAAIAAAhIAbAAIAAAPIgbAAIAAAgIAfAAIAAARgAJXgoIAAhEIgBAAIgPAwIgIAAIgQgwIAAAAIAABEIgQAAIAAhwIAPAAIAVA7IAAAAIAUg7IAPAAIAABwgAH/goIAAhwIARAAIAABwgAHWgoIAAhhIgTAAIAAgNIgVA/IAAAvIgQAAIAAgvIgWhBIAQAAIANAsIABAAIAMgsIBGAAIAAAPIgTAAIAABhgAFxgoIgYhDIAABDIgQAAIAAhwIAQAAIAXBDIAAhDIARAAIAABwgAEygoIgFgZIgVAAIgFAZIgQAAIAZhwIANAAIAZBwgAEbhQIAPAAIgIgogAB0goIAAhwIAwAAIAAAPIggAAIAAAhIAbAAIAAAPIgbAAIAAAgIAgAAIAAARgAA6goIAAhwIAwAAIAAAPIggAAIAAAhIAcAAIAAAPIgcAAIAAAgIAgAAIAAARgAANgoIAAgvIgUhBIAPAAIANAsIAAAAIANgsIARAAIgWBBIAAAvgAh9goIAAhwIARAAIAABfIAfAAIAAARgAi8goIAAhwIAYAAQAHAAAEACQAGACAEAEQAEAEABAGQACAGAAAKIgBANQgBAFgDAFQgDAFgGADQgFADgJAAIgIAAIAAAsgAishjIAIAAQAJAAACgFQACgFAAgJQAAgHgCgGQgBgGgKAAIgIAAgAjbgoIAAhEIAAAAIgQAwIgIAAIgPgwIgBAAIAABEIgQAAIAAhwIAQAAIAUA7IABAAIAUg7IAPAAIAABwgAlNgoIAAhwIAwAAIAAAPIgfAAIAAAhIAbAAIAAAPIgbAAIAAAgIAfAAIAAARgAmEgoIgPgwIgJAAIAAAwIgQAAIAAhwIAaAAQAOAAAHAIQAHAJAAAQQAAAJgDAIQgDAGgIAFIARAzgAmchmIAJAAQAIAAADgFQACgFAAgHQAAgJgDgEQgCgFgJAAIgIAAgApfgoIAAgvIgVhBIAQAAIANAsIABAAIAMgsIARAAIgVBBIAAAvgArFgoIAAhwIAwAAIAAAPIgfAAIAAAhIAbAAIAAAPIgbAAIAAAgIAfAAIAAARgAsegoIgFgZIgWAAIgFAZIgPAAIAYhwIAOAAIAYBwgAs2hQIAQAAIgIgogAtkgoIgYhDIAABDIgQAAIAAhwIAPAAIAYBDIAAhDIAQAAIAABwgAujgoIgFgZIgWAAIgEAZIgQAAIAZhwIANAAIAZBwgAu6hQIAPAAIgIgogAvqgoIAAhEIAAAAIgQAwIgIAAIgPgwIgBAAIAABEIgQAAIAAhwIAQAAIAUA7IAAAAIAVg7IAPAAIAABwg");
	this.shape.setTransform(-5.5,1.3);

	this.instance = new lib.cloud3();
	this.instance.setTransform(-157.6,-77,1.222,1.222);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-157.6,-77,315.4,154);


(lib.mc17 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("Ai1CiQgKgJAAgOIAAgHIATAAIAAAGQAAAFAEAEQADAFAHAAQAKAAACgGQACgFAAgJQAAgKgBgEQgCgEgFgCIgBgBIgCAAIgOgFQgMgGgFgIQgEgJAAgMQAAgQAIgKQAJgMAQABQAOgBAJAKQAJAIABAMIAAAAIAAAKIgUAAIAAgDQAAgHgDgGQgDgEgHAAQgIAAgDAGQgDAFAAAHQAAAHACADQACAEAGADIABABIAAAAIAPAGQANAEAEAJQAEAIAAAQQAAAQgIAMQgHALgTAAQgOAAgJgJgAmWCiQgKgJAAgNIAAhpIAUAAIAABmQAAAIADADQAEADAFABQAFgBADgDQAEgDAAgIIAAhmIATAAIAABpQAAANgJAJQgJAJgNAAQgNAAgJgJgAnRCrIgCAAIgBAAQgLgBgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgIALgBIABAAIACAAIABAAIABAAQAMABAJAIQAJAIAAAPIAABIQAAAPgJAJQgJAIgMABIgBAAIgBAAgAnaA4QgEAEAAAGIAABIQAAAHAEAEQADADAGAAQAFAAAEgDQAEgEAAgHIAAhIQAAgGgEgEQgEgDgFAAQgGAAgDADgAoiCrIgDAAQgKgBgIgIQgIgGAAgRIAAhKQAAgOAIgIQAJgKAOABQAOgBAJAKQAIAJAAAOIAAAHIgTAAIAAgGQAAgFgDgFQgDgEgGAAQgHAAgCAEQgDAFAAAHIAABFQAAAFADAEQACAFAHAAIABAAIACAAQADgBADgEQADgDAAgGIAAgGIATAAIAAAIQAAAMgIAJQgJAKgNAAIgDAAgAjtCqIAAh1IgXAAIAAgSIBAAAIAAASIgWAAIAAB1gAkeCqIgehRIAAAAIAABRIgTAAIAAiHIASAAIAdBRIABAAIAAhRIATAAIAACHgAqUCqIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAq+CqIgFgdIgbAAIgFAdIgUAAIAeiHIARAAIAeCHgAraB7IATAAIgJgyIgBAAgAsUCqIAAg8IgYAAIAAA8IgTAAIAAiHIATAAIAAA5IAYAAIAAg5IAUAAIAACHgAtxCqIAAh1IgXAAIAAgSIBAAAIAAASIgWAAIAAB1gADwgqQgKgIAAgOIAAhpIAUAAIAABnQAAAHADAEQAEADAFAAQAFAAADgDQAEgEAAgHIAAhnIATAAIAABpQAAAOgJAIQgJAKgNgBQgNABgJgKgABkghIgDAAQgKgBgIgHQgIgHAAgQIAAhLQAAgNAIgJQAJgJAOAAQAOAAAJAJQAIAKAAAOIAAAHIgTAAIAAgGQAAgGgDgEQgDgFgGAAQgHABgCAEQgDAEAAAIIAABFQAAAFADAEQACAEAHAAIABAAIACAAQADgBADgDQADgDAAgGIAAgHIATAAIAAAJQAAAMgIAJQgJAJgNAAIgDAAgAn4ghIgDAAQgKgBgIgHQgIgHAAgQIAAhLQAAgNAIgJQAJgJAOAAQAOAAAJAJQAIAKAAAOIAAAHIgTAAIAAgGQAAgGgDgEQgDgFgGAAQgHABgCAEQgDAEAAAIIAABFQAAAFADAEQACAEAHAAIABAAIACAAQADgBADgDQADgDAAgGIAAgHIATAAIAAAJQAAAMgIAJQgJAJgNAAIgDAAgANfgiIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAMugiIgdhRIgBAAIAABRIgTAAIAAiHIATAAIAdBSIAAAAIAAhSIAUAAIAACHgAK3giIAAiHIA6AAIAAASIgmAAIAAAoIAhAAIAAATIghAAIAAAnIAmAAIAAATgAJxgiIAAiHIATAAIAAB0IAnAAIAAATgAJUgiIgGgcIgaAAIgGAcIgTAAIAeiHIAQAAIAeCHgAI3hRIATAAIgJgxIAAAAgAHxgiIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAFrgiIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAE4giIAAiHIATAAIAACHgADGgiIgSg5IgLAAIAAA5IgUAAIAAiHIAgAAQARAAAIAKQAJAKAAAUQAAAMgEAIQgEAIgJAGIAVA9gACphsIALAAQAKgBADgFQACgGAAgKQAAgJgDgGQgDgFgKgBIgKAAgAABgiIAAiHIA6AAIAAASIgmAAIAAAoIAhAAIAAATIghAAIAAAnIAmAAIAAATgAgdgiIgSg5IgLAAIAAA5IgUAAIAAiHIAgAAQARAAAIAKQAJAKAAAUQAAAMgEAIQgEAIgJAGIAVA9gAg6hsIALAAQAKgBADgFQACgGAAgKQAAgJgDgGQgDgFgKgBIgKAAgAjCgiIAAiHIAdAAQAQAAAJAJQAJAKAAAQIAAA/QAAATgKAJQgJAKgRgBgAivg0IAJAAQAIAAAEgEQADgEAAgJIAAhBQAAgIgDgEQgDgEgJgBIgJAAgAjmgiIgehRIAAAAIAABRIgTAAIAAiHIASAAIAdBSIABAAIAAhSIATAAIAACHgAk0giIgFgcIgbAAIgFAcIgUAAIAeiHIARAAIAeCHgAlQhRIATAAIgJgxIgBAAgAm6giIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAoxgiIgGgcIgaAAIgGAcIgTAAIAeiHIAQAAIAeCHgApOhRIATAAIgJgxIAAAAgAqCgiIgSg5IgLAAIAAA5IgUAAIAAiHIAgAAQARAAAIAKQAJAKAAAUQAAAMgEAIQgEAIgJAGIAVA9gAqfhsIALAAQAKgBADgFQACgGAAgKQAAgJgDgGQgDgFgKgBIgKAAgArlgiIAAh1IgrAAIAAB1IgUAAIAAh1IgWAAIAAgSIB/AAIAAASIgWAAIAAB1gAtOgiIgFgcIgbAAIgFAcIgUAAIAeiHIARAAIAeCHgAtqhRIATAAIgJgxIgBAAg");
	this.shape.setTransform(0.4,4.5);

	this.instance = new lib.cloud4();
	this.instance.setTransform(130,84.5,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc16 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("AglCTQgKgKAAgNIAAgHIATAAIAAAGQAAAFAEAFQADADAHAAQAKAAACgEQACgGAAgKQAAgJgBgDQgCgFgFgCIgBAAIgCgBIgOgGQgMgEgFgJQgEgIAAgOQAAgPAIgKQAJgMAQAAQANABAIAIQAJAKABALIAAABIAAAKIgSAAIAAgFQAAgHgDgEQgDgFgHAAQgIAAgDAFQgDAGAAAGQAAAIACAEQACADAGADIABAAIAAAAIAPAGQALAFAEAJQAEAJAAAOQAAASgIAKQgHAMgRAAQgOAAgJgJgAhyCTQgKgKAAgNIAAgHIATAAIAAAGQAAAFAEAFQADADAHAAQAKAAACgEQACgGAAgKQAAgJgBgDQgCgFgFgCIgBAAIgCgBIgOgGQgMgEgFgJQgEgIAAgOQAAgPAIgKQAJgMAQAAQAOABAJAIQAJAKABALIAAABIAAAKIgUAAIAAgFQAAgHgDgEQgDgFgHAAQgIAAgDAFQgDAGAAAGQAAAIACAEQACADAGADIABAAIAAAAIAPAGQANAFAEAJQAEAJAAAOQAAASgIAKQgHAMgTAAQgOAAgJgJgAjwCcIgDAAQgKgCgIgGQgIgIAAgQIAAhKQAAgOAIgJQAJgIAOgBQAOABAJAIQAIAKAAAOIAAAIIgTAAIAAgHQAAgFgDgFQgDgEgGAAQgHAAgCAFQgDAEAAAHIAABFQAAAGADAEQACADAHAAIABAAIACAAQADgBADgCQADgDAAgHIAAgHIATAAIAAAJQAAANgIAJQgJAJgNAAIgDAAgAk+CcIgCAAQgKgCgIgGQgJgIAAgQIAAhKQAAgOAJgJQAJgIAOgBQANABAJAIQAJAKAAAOIAAAIIgTAAIAAgHQAAgFgEgFQgDgEgFAAQgIAAgCAFQgCAEAAAHIAABFQAAAGACAEQADADAGAAIACAAIABAAQAEgBADgCQADgDAAgHIAAgHIATAAIAAAJQAAANgJAJQgJAJgNAAIgDAAgAmhCTQgKgJAAgOIAAhpIAUAAIAABoQAAAGADAEQAEADAFAAQAFAAADgDQAEgEAAgGIAAhoIATAAIAABpQAAAOgJAJQgJAJgNAAQgNAAgJgJgAnyCTQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAFQADADAIAAQAJAAADgEQACgGAAgKQAAgJgCgDQgBgFgGgCIgBAAIgCgBIgNgGQgNgEgEgJQgEgIAAgOQAAgPAIgKQAIgMARAAQAOABAJAIQAIAKABALIAAABIAAAKIgTAAIAAgFQAAgHgDgEQgEgFgHAAQgIAAgDAFQgCAGAAAGQAAAIABAEQACADAGADIABAAIABAAIAPAGQANAFADAJQAEAJAAAOQAAASgHAKQgIAMgSAAQgOAAgKgJgAtNCcIgCAAIgBAAQgLgBgJgIQgKgIAAgQIAAhHQAAgQAKgIQAJgJALgBIABAAIACAAIABAAIABAAQAMABAJAJQAJAIAAAQIAABHQAAAQgJAIQgJAIgMABIgBAAIgBAAgAtWAqQgEACAAAIIAABHQAAAHAEADQADADAGAAQAFAAAEgDQAEgDAAgHIAAhHQAAgIgEgCQgEgEgFAAQgGAAgDAEgAvvCcIgCAAIgBAAQgLgBgJgIQgKgIAAgQIAAhHQAAgQAKgIQAJgJALgBIABAAIACAAIABAAIABAAQAMABAJAJQAJAIAAAQIAAAHIgTAAIAAgHQAAgIgEgCQgEgEgFAAQgGAAgDAEQgEACAAAIIAABHQAAAHAEADQADADAGAAQAFAAAEgDQAEgDAAgHIAAgaIgQAAIAAgRIAjAAIAAArQAAAQgJAIQgJAIgMABIgBAAIgBAAgAi/CbIAAiIIA6AAIAAATIgmAAIAAAoIAhAAIAAASIghAAIAAAnIAmAAIAAAUgApBCbIAAg8IgYAAIAAA8IgTAAIAAiIIATAAIAAA7IAYAAIAAg7IAUAAIAACIgAqeCbIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gArdCbIgPhYIgBAAIgOBYIgTAAIgUiIIAVAAIAKBWIAPhWIAPAAIAQBYIAAAAIAJhYIAVAAIgUCIgAuNCbIgSg6IgLAAIAAA6IgUAAIAAiIIAgAAQARAAAIALQAJAJAAAVQAAALgEAIQgEAJgJAGIAVA9gAuqBQIALAAQAKAAADgFQACgGAAgKQAAgKgDgGQgDgFgKAAIgKAAgAPYgbQgKgJAAgNIAAgHIATAAIAAAFQAAAGAEAEQADAEAHAAQAKAAACgFQACgGAAgJQAAgJgBgEQgCgEgFgCIgBgBIgCgBIgOgFQgMgFgFgJQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgEgHAAQgIAAgDAFQgDAGAAAGQAAAHACAEQACAEAGADIABAAIAAAAIAPAGQANAEAEAJQAEAJAAAPQAAARgIALQgHALgTAAQgOABgJgKgAOLgbQgKgJAAgNIAAgHIATAAIAAAFQAAAGAEAEQADAEAHAAQAKAAACgFQACgGAAgJQAAgJgBgEQgCgEgFgCIgBgBIgCgBIgOgFQgMgFgFgJQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgEgHAAQgIAAgDAFQgDAGAAAGQAAAHACAEQACAEAGADIABAAIAAAAIAPAGQANAEAEAJQAEAJAAAPQAAARgIALQgHALgTAAQgOABgJgKgAJ8gbQgKgJAAgNIAAgHIATAAIAAAFQAAAGAEAEQADAEAHAAQAKAAACgFQACgGAAgJQAAgJgBgEQgCgEgFgCIgBgBIgCgBIgOgFQgMgFgFgJQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgEgHAAQgIAAgDAFQgDAGAAAGQAAAHACAEQACAEAGADIABAAIAAAAIAPAGQANAEAEAJQAEAJAAAPQAAARgIALQgHALgTAAQgOABgJgKgAIugbQgJgJAAgNIAAhpIATAAIAABnQAAAHAEAEQADADAFAAQAFAAAEgDQADgEAAgHIAAhnIAUAAIAABpQAAANgKAJQgJAKgNgBQgNABgJgKgAF/gSIgCAAIgBAAQgLAAgJgJQgKgIAAgQIAAhHQAAgQAKgHQAJgJALgBIABAAIACAAIABAAIABAAQAMABAJAJQAJAHAAAQIAABHQAAAQgJAIQgJAJgMAAIgBAAIgBAAgAF2iEQgEADAAAHIAABHQAAAIAEADQADADAGAAQAFAAAEgDQAEgDAAgIIAAhHQAAgHgEgDQgEgDgFAAQgGAAgDADgAj5gSIgCAAIgBAAQgLAAgJgJQgKgIAAgQIAAhHQAAgQAKgHQAJgJALgBIABAAIACAAIABAAIABAAQAMABAJAJQAJAHAAAQIAABHQAAAQgJAIQgJAJgMAAIgBAAIgBAAgAkCiEQgEADAAAHIAABHQAAAIAEADQADADAGAAQAFAAAEgDQAEgDAAgIIAAhHQAAgHgEgDQgEgDgFAAQgGAAgDADgAp9gSIgCAAIgBAAQgLAAgJgJQgKgIAAgQIAAhHQAAgQAKgHQAJgJALgBIABAAIACAAIABAAIABAAQAMABAJAJQAJAHAAAQIAABHQAAAQgJAIQgJAJgMAAIgBAAIgBAAgAqGiEQgEADAAAHIAABHQAAAIAEADQADADAGAAQAFAAAEgDQAEgDAAgIIAAhHQAAgHgEgDQgEgDgFAAQgGAAgDADgAM+gTIAAiHIA6AAIAAATIgmAAIAAAnIAhAAIAAASIghAAIAAAoIAmAAIAAATgAMagTIgdhRIgBAAIAABRIgTAAIAAiHIATAAIAdBSIAAAAIAAhSIAUAAIAACHgALDgTIAAiHIAUAAIAACHgAHUgTIAAiHIAcAAQATAAAIAKQAIALAAAOIAAAEQAAALgEAFQgDAFgHAEQAHADADAHQAEAGAAALIAAAIQAAASgJAJQgJAKgTgBgAHngmIAJAAQALAAADgHQACgGAAgKQAAgJgDgGQgDgFgKAAIgJAAgAHnhjIAJAAQAKAAADgEQADgFAAgKQAAgIgDgFQgEgEgKAAIgIAAgAEtgTIAAh0IgWAAIAAgTIBAAAIAAATIgWAAIAAB0gACvgTIAAiHIAdAAQAIAAAGACQAHADAFAEQAFAGABAHQACAHAAANIgBAOQgBAHgDAGQgEAGgHAEQgHADgKAAIgKAAIAAA1gADDhaIAJAAQAMAAACgGQACgHAAgJQAAgKgCgGQgCgIgLABIgKAAgACSgTIgFgcIgbAAIgFAcIgUAAIAeiHIARAAIAeCHgAB2hCIATAAIgJgxIgBAAgAA8gTIAAhSIAAAAIgTA6IgKAAIgTg6IAAAAIAABSIgSAAIAAiHIARAAIAZBIIAAAAIAZhIIATAAIAACHgAh6gTIAAiHIAdAAQAQAAAJAJQAJAKAAAQIAABAQAAASgKAJQgJAKgRgBgAhnglIAJAAQAIAAAEgEQADgEAAgJIAAhBQAAgIgDgEQgDgEgJAAIgJAAgAiXgTIgGgcIgaAAIgGAcIgTAAIAeiHIAQAAIAeCHgAi0hCIATAAIgJgxIAAAAgAk5gTIgSg5IgLAAIAAA5IgUAAIAAiHIAgAAQARAAAIAKQAJAKAAAUQAAALgEAJQgEAIgJAGIAVA9gAlWhdIALAAQAKAAADgGQACgGAAgKQAAgJgDgGQgDgFgKAAIgKAAgAmqgTIgGgcIgaAAIgGAcIgTAAIAeiHIAQAAIAeCHgAnHhCIATAAIgJgxIAAAAgApMgTIAAiHIAdAAQAIAAAGACQAHADAFAEQAFAGABAHQACAHAAANIgBAOQgBAHgDAGQgEAGgHAEQgHADgKAAIgKAAIAAA1gAo4haIAJAAQAMAAACgGQACgHAAgJQAAgKgCgGQgCgIgLABIgKAAgArjgTIAAiHIATAAIAAB0IAnAAIAAATgAsqgTIAAiHIA6AAIAAATIgmAAIAAAnIAhAAIAAASIghAAIAAAoIAmAAIAAATgAtigTIgciHIAVAAIAPBeIABAAIAPheIAVAAIgcCHgAu+gTIAAiHIA6AAIAAATIgmAAIAAAnIAhAAIAAASIghAAIAAAoIAmAAIAAATgAwPgTIAAiHIAdAAQAQAAAJAJQAJAKAAAQIAABAQAAASgJAJQgKAKgRgBgAv7glIAJAAQAIAAADgEQAEgEAAgJIAAhBQAAgIgEgEQgDgEgIAAIgJAAg");
	this.shape.setTransform(1.9,88);

	this.instance = new lib.cloud2();
	this.instance.setTransform(-123.5,-157);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-123.5,-157,247,314);


(lib.mc15 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("AkdCUQgJgJAAgNIAAgGIASAAIAAAFQAAAFADAEQADAEAHAAQAJAAACgFQACgFAAgJIgBgNQgBgDgGgCIgBAAIgBgBIgNgGQgMgFgEgHQgEgJAAgMQAAgOAIgKQAHgLAQAAQANAAAJAIQAIAJABALIAAALIgTAAIAAgFQAAgGgDgFQgCgFgIABQgHgBgDAGQgCAFAAAGQAAAHACAEQABADAGADIABAAIABAAIAOAGQAMAFADAHQAEAJAAAOQAAAQgHALQgHALgSgBQgNAAgJgIgAmsCUQgJgJAAgNIAAgGIASAAIAAAFQAAAFADAEQADAEAHAAQAJAAACgFQACgFAAgJIgBgNQgBgDgGgCIgBAAIgBgBIgNgGQgMgFgEgHQgEgJAAgMQAAgOAIgKQAHgLAQAAQANAAAJAIQAIAJABALIAAALIgTAAIAAgFQAAgGgDgFQgCgFgIABQgHgBgDAGQgCAFAAAGQAAAHACAEQABADAGADIABAAIABAAIAOAGQAMAFADAHQAEAJAAAOQAAAQgHALQgHALgSgBQgNAAgJgIgAqKCcIgCAAIgBAAQgLgBgIgHQgJgIAAgOIAAhFQAAgOAJgIQAIgIALAAIABgBIACAAIABAAIABABQALAAAJAIQAIAIAAAOIAABFQAAAOgIAIQgJAHgLABIgBAAIgBAAgAqTAwQgEADAAAGIAABFQAAAGAEADQAEADAFAAQAFAAADgDQAEgDAAgGIAAhFQAAgGgEgDQgDgDgFAAQgFAAgEADgAt/CUQgJgIAAgNIAAhjIATAAIAABhQAAAGADAEQADADAFAAQAFAAADgDQADgEAAgGIAAhhIATAAIAABjQAAANgJAIQgJAIgMAAQgMAAgJgIgAvLCUQgJgJAAgNIAAgGIASAAIAAAFQAAAFADAEQADAEAHAAQAJAAACgFQACgFAAgJIgBgNQgBgDgGgCIgBAAIgBgBIgNgGQgMgFgEgHQgEgJAAgMQAAgOAIgKQAHgLAQAAQANAAAJAIQAIAJABALIAAALIgTAAIAAgFQAAgGgDgFQgCgFgIABQgHgBgDAGQgCAFAAAGQAAAHACAEQABADAGADIABAAIABAAIAOAGQAMAFADAHQAEAJAAAOQAAAQgHALQgHALgSgBQgNAAgJgIgAgbCcIAAhPIAAAAIgSA3IgJAAIgSg3IgBAAIAABPIgSAAIAAiAIASAAIAXBDIAYhDIASAAIAACAgAieCcIAAiAIA3AAIAAARIgkAAIAAAlIAfAAIAAASIgfAAIAAAlIAkAAIAAATgAjOCcIAAhvIgVAAIAAgRIA9AAIAAARIgVAAIAABvgAlXCcIAAg2IgahKIAUAAIAOAyIABAAIAPgyIATAAIgZBKIAAA2gAoDCcIAAhvIgVAAIAAgRIA9AAIAAARIgWAAIAABvgAotCcIgRg3IgLAAIAAA3IgTAAIAAiAIAeAAQAQAAAIAJQAIAKAAASQAAALgDAIQgEAIgIAFIATA7gApJBVIAKAAQAKAAACgGQADgFAAgJQAAgKgDgFQgDgGgKABIgJAAgAryCcIAAiAIAcAAQAIAAAFACQAGABAFAGQAEAFACAGQACAHAAALQAAAJgBAGQgBAGgDAFQgEAHgGADQgHAEgKgBIgJAAIAAAzgArfBYIAJAAQALAAACgGQACgGAAgKQAAgJgCgGQgCgHgLABIgJAAgAs7CcIAAiAIAbAAQAIAAAGACQAGABAEAGQAFAFACAGQACAHAAALQAAAJgCAGQAAAGgEAFQgEAHgGADQgGAEgKgBIgKAAIAAAzgAspBYIAJAAQALAAACgGQACgGAAgKQAAgJgCgGQgBgHgLABIgKAAgAK+gZIgBAAIgBAAQgLgBgJgHQgJgJAAgOIAAhEQAAgOAJgJQAJgHALgBIABgBIABAAIABAAIABABQALABAJAHQAJAJAAAOIAABEQAAAOgJAJQgJAHgLABIgBAAIgBAAgAK1iGQgDAEAAAGIAABEQAAAGADADQAEADAFABQAFgBAEgDQAEgDAAgGIAAhEQAAgGgEgEQgEgDgFAAQgFAAgEADgAIhghQgJgKAAgNIAAgGIASAAIAAAFQAAAFADAEQADAEAHABQAJgBACgEQACgFAAgKIgBgMQgBgEgGgCIgBAAIgBgBIgNgFQgMgFgEgIQgEgIAAgMQAAgPAIgKQAHgLAQAAQANAAAJAJQAIAJABAKIAAALIgTAAIAAgEQAAgHgDgEQgCgFgIAAQgHAAgDAGQgCAFAAAFQAAAHACAEQABAEAGADIABAAIABAAIAOAGQAMAEADAIQAEAIAAAOQAAARgHAKQgHALgSAAQgNAAgJgIgAHYghQgJgJAAgNIAAhjIASAAIAABhQAAAHADADQAEADAFABQAEgBAEgDQADgDAAgHIAAhhIASAAIAABjQAAANgIAJQgJAIgMAAQgNAAgIgIgAGggZIgCAAQgKgCgIgGQgIgGAAgQIAAhHQAAgNAJgIQAIgJANAAQANAAAIAJQAJAJAAAOIAAAGIgTAAIAAgFQAAgGgDgEQgDgEgFAAQgHAAgCAFQgCAEAAAGIAABBQAAAGACAEQADAEAGAAIABAAIACAAQADgCADgDQACgCAAgGIAAgHIATAAIAAAIQAAAMgJAJQgIAJgMAAIgDAAgAEgghQgJgKAAgNIAAgGIATAAIAAAFQAAAFADAEQADAEAHABQAJgBACgEQACgFAAgKIgBgMQgCgEgFgCIgBAAIgCgBIgNgFQgMgFgEgIQgEgIAAgMQAAgPAIgKQAIgLAQAAQANAAAIAJQAJAJAAAKIAAALIgSAAIAAgEQAAgHgDgEQgDgFgHAAQgIAAgCAGQgCAFAAAFQAAAHABAEQABAEAHADIAAAAIABAAIAOAGQAMAEAEAIQADAIAAAOQAAARgHAKQgHALgRAAQgOAAgJgIgADXghQgJgKAAgNIAAgGIATAAIAAAFQAAAFADAEQADAEAHABQAJgBACgEQACgFAAgKIgBgMQgCgEgFgCIgBAAIgCgBIgNgFQgMgFgEgIQgEgIAAgMQAAgPAIgKQAIgLAQAAQANAAAIAJQAJAJAAAKIAAALIgSAAIAAgEQAAgHgDgEQgDgFgHAAQgIAAgCAGQgCAFAAAFQAAAHABAEQABAEAHADIAAAAIABAAIAOAGQAMAEAEAIQADAIAAAOQAAARgHAKQgHALgRAAQgOAAgJgIgAjlghQgJgKAAgNIAAgGIATAAIAAAFQAAAFADAEQADAEAHABQAJgBACgEQACgFAAgKIgBgMQgCgEgFgCIgBAAIgCgBIgNgFQgMgFgEgIQgEgIAAgMQAAgPAIgKQAIgLAQAAQANAAAIAJQAJAJAAAKIAAALIgSAAIAAgEQAAgHgDgEQgDgFgHAAQgIAAgCAGQgCAFAAAFQAAAHABAEQABAEAHADIAAAAIABAAIAOAGQAMAEAEAIQADAIAAAOQAAARgHAKQgHALgRAAQgOAAgJgIgAoHghQgJgKAAgNIAAgGIATAAIAAAFQAAAFADAEQADAEAHABQAJgBACgEQACgFAAgKIgBgMQgCgEgFgCIgBAAIgCgBIgNgFQgMgFgEgIQgEgIAAgMQAAgPAIgKQAIgLAQAAQANAAAIAJQAJAJAAAKIAAALIgSAAIAAgEQAAgHgDgEQgDgFgHAAQgIAAgCAGQgCAFAAAFQAAAHABAEQABAEAHADIAAAAIABAAIAOAGQAMAEAEAIQADAIAAAOQAAARgHAKQgHALgRAAQgOAAgJgIgAuJghQgJgKAAgNIAAgGIATAAIAAAFQAAAFADAEQADAEAHABQAJgBACgEQACgFAAgKIgBgMQgCgEgFgCIgBAAIgCgBIgNgFQgMgFgEgIQgEgIAAgMQAAgPAIgKQAIgLAQAAQANAAAIAJQAJAJAAAKIAAALIgSAAIAAgEQAAgHgDgEQgDgFgHAAQgIAAgCAGQgCAFAAAFQAAAHABAEQABAEAHADIAAAAIABAAIAOAGQAMAEAEAIQADAIAAAOQAAARgHAKQgHALgRAAQgOAAgJgIgAPCgaIgRg3IgLAAIAAA3IgTAAIAAiAIAeAAQAQAAAIAJQAIAKAAASQAAALgDAJQgEAIgIAEIATA7gAOmhhIAKAAQAKAAACgFQADgFAAgKQAAgKgDgEQgDgGgKAAIgJAAgANRgaIAAiAIA3AAIAAARIglAAIAAAlIAgAAIAAASIggAAIAAAlIAlAAIAAATgAMtgaIAAhOIAAAAIgSA3IgJAAIgSg3IgBAAIAABOIgSAAIAAiAIASAAIAXBEIAYhEIASAAIAACAgAJwgaIAAhvIgVAAIAAgRIA9AAIAAARIgVAAIAABvgACPgaIAAiAIA3AAIAAARIgkAAIAAAlIAfAAIAAASIgfAAIAAAlIAkAAIAAATgABNgaIAAiAIASAAIAABtIAlAAIAAATgAApgaIAAhOIAAAAIgSA3IgJAAIgQg3IgBAAIAABOIgSAAIAAiAIASAAIAVBEIAYhEIASAAIAACAgAgxgaIgFgcIgZAAIgGAcIgSAAIAdiAIAPAAIAdCAgAhMhHIASAAIgJgugAihgaIAAiAIA3AAIAAARIglAAIAAAlIAgAAIAAASIggAAIAAAlIAlAAIAAATgAkngaIgGgcIgZAAIgFAcIgSAAIAciAIAQAAIAcCAgAlChHIASAAIgJgugAmagaIAAg5IgXAAIAAA5IgSAAIAAiAIASAAIAAA3IAXAAIAAg3IASAAIAACAgAowgaIAAiAIASAAIAACAgApzgaIAAiAIASAAIAABtIAlAAIAAATgArAgaIAAiAIAbAAQASAAAHAKQAIAKAAANIAAAEQAAAKgDAFQgEAFgGAEQAHADADAGQADAGAAAKIAAAHQAAARgIAJQgJAJgSAAgAqtgtIAIAAQALAAACgFQADgHAAgJQAAgJgDgGQgDgEgKAAIgIAAgAqthmIAIAAQAJAAADgEQAEgFAAgJQAAgIgEgEQgEgFgJAAIgHAAgArbgaIgGgcIgZAAIgFAcIgSAAIAciAIAQAAIAcCAgAr2hHIASAAIgJgugAs5gaIAAhvIgVAAIAAgRIA9AAIAAARIgWAAIAABvgAvRgaIAAiAIA3AAIAAARIgkAAIAAAlIAfAAIAAASIgfAAIAAAlIAkAAIAAATg");
	this.shape.setTransform(5.6,86.9);

	this.instance = new lib.cloud2();
	this.instance.setTransform(-123.5,-157);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-123.5,-157,247,314);


(lib.mc14 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("ANUBIIgCgBQgLgBgJgHQgJgHABgSIAAhMQAAgOAJgJQAJgKAOAAQAPAAAJAKQAJAJAAAPIAAAIIgUAAIAAgHQAAgGgEgEQgDgEgGAAQgHAAgDAEQgCAFAAAHIAABHQAAAGACAEQADAFAHAAIACgBIACAAQADgBADgDQADgDAAgHIAAgHIAUAAIAAAJQAAANgJAKQgJAKgOAAIgDAAgAiaA+QgJgJAAgOIAAhtIAUAAIAABrQAAAHAEAEQADAEAGAAQAGAAADgEQADgEAAgHIAAhrIAVAAIAABtQAAAOgKAJQgJAKgOAAQgNAAgLgKgAjWBIIgBAAIgBAAIgCAAQgMgBgJgJQgKgIAAgRIAAhJQAAgQAKgJQAJgJAMgBIACAAIABAAIABAAIACAAQALABAKAJQALAJgBAQIAABJQABARgLAIQgKAJgLABIgCAAgAjhgvQgDAEAAAHIAABJQAAAIADADQAFAEAFAAQAGAAAEgEQAEgDAAgIIAAhJQAAgHgEgEQgEgDgGAAQgFAAgFADgAoAA+QgJgKgBgOIAAgHIAVAAIAAAGQAAAGAEAEQADAFAHAAQALAAACgGQACgGAAgKQAAgKgBgEQgCgDgFgDIgCgBIgCAAIgOgGQgNgEgFgJQgFgIAAgOQABgQAIgLQAJgMARAAQAPAAAKAKQAJAJAAAMIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgEgFgHAAQgJAAgCAFQgDAGAAAHQAAAIACAEQACADAGADIABABIABAAIAPAGQAOAFAEAHQADAKAAAPQAAASgHAMQgIAMgUAAQgOAAgLgKgAsqBIIgCgBQgLgBgJgHQgIgHAAgSIAAhMQgBgOAKgJQAJgKAOAAQAPAAAJAKQAJAJAAAPIAAAIIgUAAIAAgHQAAgGgEgEQgDgEgGAAQgHAAgDAEQgCAFAAAHIAABHQAAAGACAEQAEAFAGAAIACgBIACAAQADgBADgDQADgDAAgHIAAgHIAUAAIAAAJQAAANgJAKQgKAKgNAAIgDAAgAO2BHIAAhAIgaAAIAABAIgUAAIAAiNIAUAAIAAA9IAaAAIAAg9IAUAAIAACNgAMYBHIgGgfIgbAAIgHAfIgUAAIAfiNIARAAIAhCNgAL6AVIAUAAIgKgyIAAAAgAKbBHIAAiNIA9AAIAAAUIgoAAIAAApIAjAAIAAASIgjAAIAAApIAoAAIAAAVgAJ6BHIgTg9IgNAAIAAA9IgUAAIAAiNIAhAAQASAAAJALQAJAKAAAVQAAAMgEAJQgEAHgJAGIAVBBgAJagGIAMAAQALAAACgGQADgGAAgKQAAgLgDgGQgCgFgMAAIgLAAgAHsBHIAAh5IgYAAIAAgUIBEAAIAAAUIgYAAIAAB5gAGUBHIAAiNIA9AAIAAAUIgoAAIAAApIAjAAIAAASIgjAAIAAApIAoAAIAAAVgAF2BHIgXg/IgLAUIAAArIgUAAIAAiNIAUAAIAABBIAAAAIAehBIAVAAIgcA5IAgBUgAEeBHIgTg9IgMAAIAAA9IgVAAIAAiNIAhAAQASAAAJALQAKAKgBAVQAAAMgDAJQgFAHgJAGIAWBBgAD/gGIALAAQALAAADgGQADgGAAgKQAAgLgDgGQgDgFgLAAIgLAAgADMBHIgGgfIgcAAIgFAfIgVAAIAgiNIARAAIAfCNgACuAVIAVAAIgKgyIgBAAgAByBHIAAhVIgVA7IgKAAIgUg7IgBAAIAABVIgUAAIAAiNIATAAIAbBKIAAAAIAahKIAUAAIAACNgAgbBHIgTg9IgLAAIAAA9IgVAAIAAiNIAhAAQASAAAJALQAJAKAAAVQAAAMgEAJQgEAHgKAGIAXBBgAg5gGIALAAQAKAAAEgGQADgGAAgKQAAgLgDgGQgEgFgLAAIgKAAgAkzBHIAAg8IgchRIAWAAIAQA5IABAAIAQg5IAWAAIgcBRIAAA8gAm0BHIAAiNIA8AAIAAAUIgoAAIAAApIAjAAIAAASIgjAAIAAApIAoAAIAAAVgAokBHIgGgfIgbAAIgGAfIgVAAIAfiNIARAAIAhCNgApCAVIAUAAIgKgyIAAAAgAqhBHIAAiNIA9AAIAAAUIgoAAIAAApIAjAAIAAASIgjAAIAAApIAoAAIAAAVgArCBHIgTg9IgNAAIAAA9IgUAAIAAiNIAhAAQASAAAJALQAJAKAAAVQAAAMgEAJQgEAHgJAGIAVBBgArigGIAMAAQALAAACgGQADgGAAgKQAAgLgDgGQgCgFgMAAIgLAAgAttBHIgghUIAAAAIAABUIgUAAIAAiNIATAAIAfBUIABAAIAAhUIAUAAIAACNgAvJBHIAAiNIAVAAIAACNg");
	this.shape.setTransform(-6,-4);

	// Layer 1
	this.instance = new lib.cloud4();
	this.instance.setTransform(-130,-84.5);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc13 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AGiDCIgCAAIgBAAIgBAAQgNgBgMgJQgLgKABgSIAAhSQgBgSALgKQAMgJANgBIABAAIABAAIACAAIABAAQAOABAKAJQALAKAAASIAABSQAAASgLAKQgKAJgOABIgBAAgAGWA+QgFADAAAJIAABSQAAAJAFADQAEAFAGAAQAGAAAFgFQAFgDAAgJIAAhSQAAgJgFgDQgFgEgGAAQgGAAgEAEgAB2C4QgKgLAAgPIAAh5IAWAAIAAB3QAAAIAEAEQAEAFAGAAQAFAAAFgFQADgEAAgIIAAh3IAXAAIAAB5QAAAPgKALQgLAKgPAAQgPAAgLgKgAAYC4QgKgLgBgQIAAgIIAXAAIAAAHQAAAHADAEQAEAGAJAAQAMgBABgGQADgGAAgLQAAgLgCgEQgBgFgHgCIgBgBIgCAAIgQgHQgPgGgEgKQgFgKAAgPQAAgSAJgMQAKgMAUAAQAQAAAKAKQAKALABAMIAAABIAAABIAAALIgXAAIAAgFQAAgIgDgFQgEgGgIAAQgKAAgDAHQgDAFABAIQgBAJACAEQACAEAHAEIABAAIABAAIARAHQAPAFAEAKQAFAKAAASQAAATgJAOQgIAMgWAAQgQAAgMgKgAmODCIgCAAIgBAAIgBAAQgNgBgMgJQgLgKABgSIAAhSQgBgSALgKQAMgJANgBIABAAIABAAIACAAIABAAQAOABAKAJQALAKAAASIAAAIIgWAAIAAgIQgBgJgEgDQgFgEgGAAQgGAAgEAEQgFADAAAJIAABSQAAAJAFADQAEAFAGAAQAGAAAFgFQAEgDABgJIAAgdIgSAAIAAgUIAoAAIAAAxQAAASgLAKQgKAJgOABIgBAAgAswC4QgKgLAAgPIAAh5IAWAAIAAB3QAAAIAEAEQAEAFAGAAQAFAAAFgFQADgEAAgIIAAh3IAXAAIAAB5QAAAPgKALQgLAKgPAAQgPAAgLgKgAuOC4QgKgLgBgQIAAgIIAXAAIAAAHQAAAHADAEQAEAGAJAAQAMgBABgGQADgGAAgLQAAgLgCgEQgBgFgHgCIgBgBIgCAAIgQgHQgPgGgEgKQgFgKAAgPQAAgSAJgMQAKgMAUAAQAQAAAKAKQAKALABAMIAAABIAAABIAAALIgXAAIAAgFQAAgIgDgFQgEgGgIAAQgKAAgDAHQgDAFABAIQgBAJACAEQACAEAHAEIABAAIABAAIARAHQAPAFAEAKQAFAKAAASQAAATgJAOQgIAMgWAAQgQAAgMgKgAwtDCIgBAAIgCAAIgCAAQgNgBgKgJQgMgKAAgSIAAhSQAAgSAMgKQAKgJANgBIACAAIACAAIABAAIABAAQANABALAJQALAKAAASIAABSQAAASgLAKQgLAJgNABIgBAAgAw6A+QgEADAAAJIAABSQAAAJAEADQAFAFAHAAQAGAAAEgFQAFgDgBgJIAAhSQABgJgFgDQgEgEgGAAQgHAAgFAEgAyMDCIgDAAQgMgBgJgIQgKgJAAgSIAAhWQAAgQALgKQAJgKARAAQAQAAAJAKQALAKAAARIAAAJIgXAAIAAgIQAAgGgDgFQgEgFgHAAQgIAAgDAFQgCAGAAAHIAABQQAAAHADAEQADAGAHAAIACgBIACAAQAEgBADgEQADgDAAgIIAAgHIAXAAIAAAJQAAAPgLALQgKAKgPAAIgDAAgAJGDBIAAiHIgaAAIAAgVIBLAAIAAAVIgaAAIAACHgAITDBIgWhCIgNAAIAABCIgXAAIAAicIAkAAQAVAAAKALQAJAMABAXQgBANgEAKQgFAKgKAGIAYBHgAHwBrIANAAQAMAAADgGQADgHAAgLQAAgNgDgFQgEgHgNAAIgLAAgAEjDBIAAicIAiAAQAIAAAIACQAHACAGAHQAFAGACAIQADAJgBANQABALgCAHQgBAIgEAGQgEAIgJAEQgIAFgLgBIgMAAIAAA9gAE5BvIALAAQAOAAACgHQACgHABgMQgBgLgCgIQgCgIgNAAIgMAAgADJDBIAAicIAiAAQAJAAAHACQAHACAGAHQAGAGACAIQACAJAAANQAAALgCAHQAAAIgFAGQgEAIgIAEQgIAFgMgBIgLAAIAAA9gADgBvIALAAQANAAADgHQACgHAAgMQAAgLgCgIQgDgIgNAAIgLAAgAhyDBIAAicIAgAAQATAAALALQAKALAAASIAABJQAAAXgKAKQgMAKgTAAgAhcCtIAKAAQAKAAAEgGQADgEAAgKIAAhMQABgJgEgFQgEgFgKAAIgKAAgAicDBIgihdIgBAAIAABdIgWAAIAAicIAWAAIAhBeIABAAIAAheIAXAAIAACcgAj2DBIgGghIgfAAIgGAhIgWAAIAjicIASAAIAjCcgAkWCKIAWAAIgLg4IAAAAgAndDBIgihdIgBAAIAABdIgXAAIAAicIAXAAIAhBeIAAAAIAAheIAXAAIAACcgApCDBIAAicIAXAAIAACcgAp8DBIAAiHIgaAAIAAgVIBKAAIAAAVIgaAAIAACHgArdDBIAAicIAXAAIAACGIAsAAIAAAWgAu9DBIgjhdIAAAAIAABdIgWAAIAAicIAVAAIAiBeIAAAAIAAheIAXAAIAACcgARtgtQgLgLAAgPIAAgJIAXAAIAAAHQAAAGADAGQAEAEAJAAQALAAACgGQACgGAAgLQAAgLgBgEQgCgEgGgEIgCAAIgBgBIgRgHQgOgFgFgKQgFgKABgOQgBgTAKgMQAKgNATAAQAQAAAKALQAKAKABANIAAABIAAABIAAALIgXAAIAAgFQAAgIgDgGQgEgFgIAAQgJAAgEAGQgCAHAAAHQAAAJABAEQADAEAGADIABABIABAAIARAHQAPAGAFAKQAEAJAAASQAAAUgJAMQgIAOgVAAQgRAAgLgLgAQUgtQgMgLAAgPIAAgJIAXAAIAAAHQAAAGADAGQAFAEAIAAQALAAACgGQADgGAAgLQAAgLgCgEQgBgEgHgEIgBAAIgCgBIgQgHQgPgFgFgKQgEgKAAgOQAAgTAKgMQAJgNAUAAQAPAAALALQAKAKAAANIAAABIAAABIAAALIgWAAIAAgFQAAgIgEgGQgDgFgIAAQgKAAgDAGQgCAHgBAHQABAJABAEQACAEAHADIABABIABAAIARAHQAPAGAFAKQADAJAAASQAAAUgIAMQgIAOgWAAQgRAAgKgLgALagtQgLgLAAgPIAAgJIAWAAIAAAHQABAGADAGQAEAEAJAAQALAAACgGQADgGAAgLQAAgLgCgEQgCgEgGgEIgCAAIgCgBIgQgHQgOgFgFgKQgFgKABgOQAAgTAJgMQAJgNAUAAQAQAAAKALQALAKAAANIAAABIAAABIAAALIgWAAIAAgFQAAgIgEgGQgDgFgJAAQgJAAgDAGQgDAHAAAHQAAAJABAEQADAEAGADIACABIABAAIARAHQAPAGAEAKQAEAJAAASQAAAUgJAMQgIAOgVAAQgRAAgLgLgAKBgtQgLgLAAgOIAAh6IAWAAIAAB3QAAAIAFAFQADADAHAAQAFAAAEgDQAEgFAAgIIAAh3IAXAAIAAB6QAAAOgLALQgLALgOAAQgPAAgLgLgAiYgiIgCAAIgCAAIgBAAQgNgBgLgKQgLgJAAgTIAAhSQAAgSALgKQALgKANgBIABAAIACAAIACAAIABAAQANABAKAKQAMAKAAASIAAAIIgXAAIAAgIQAAgIgEgEQgFgEgGAAQgHAAgEAEQgFAEABAIIAABSQgBAIAFAFQAEADAHAAQAGAAAFgDQAEgFAAgIIAAgdIgSAAIAAgUIApAAIAAAxQAAATgMAJQgKAKgNABIgBAAgAn9giIgCgBQgMgBgKgIQgJgJgBgSIAAhXQAAgPALgKQAKgLAQAAQAQAAAKALQAKAKAAARIAAAJIgWAAIAAgIQAAgGgEgFQgEgFgGAAQgJAAgCAFQgDAFAAAJIAABPQAAAHADAFQADAEAIAAIABAAIADAAQADgBADgDQAEgEAAgIIAAgHIAWAAIAAAKQAAAOgKALQgLALgPAAIgDAAgAO8gjIAAidIBCAAIAAAVIgsAAIAAAuIAnAAIAAAVIgnAAIAAAuIAsAAIAAAXgAOSgjIgihfIgBAAIAABfIgWAAIAAidIAWAAIAhBfIABAAIAAhfIAXAAIAACdgAMtgjIAAidIAXAAIAACdgAIYgjIAAidIAhAAQAWAAAJAMQAKAMgBAQIAAAFQAAANgDAFQgEAHgIAEQAIAEAEAIQADAGAAANIAAAJQABAVgLAKQgKAMgWAAgAIvg6IAKAAQAOAAACgIQADgHAAgMQAAgLgDgGQgEgGgMAAIgKAAgAIviAIAKAAQAMAAADgFQAEgGAAgLQAAgKgEgGQgFgFgLAAIgJAAgAGegjIAAidIAWAAIAACGIAtAAIAAAXgAFNgjIAAidIBCAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAELgjIgfidIAXAAIASBtIABAAIAShtIAXAAIggCdgACigjIAAidIBCAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgABQgjIAAidIAWAAIAACGIAtAAIAAAXgAgvgjIAAhGIgcAAIAABGIgWAAIAAidIAWAAIAABDIAcAAIAAhDIAXAAIAACdgAjrgjIAAidIAXAAIAACdgAkXgjIAAhGIgcAAIAABGIgWAAIAAidIAWAAIAABDIAcAAIAAhDIAWAAIAACdgAnEgjIAAidIBEAAIAAAVIgtAAIAAAuIAmAAIAAAVIgmAAIAAAuIAtAAIAAAXgApGgjIgjhfIAAAAIAABfIgXAAIAAidIAXAAIAhBfIABAAIAAhfIAWAAIAACdgArQgjIAAidIBDAAIAAAVIgtAAIAAAuIAnAAIAAAVIgnAAIAAAuIAtAAIAAAXgAr8gjIAAidIAWAAIAACdgAsigjIgVhEIgNAAIAABEIgXAAIAAidIAkAAQAVAAAJALQALAMAAAXQAAAOgFAJQgFAKgKAGIAYBIgAtEh6IANAAQALAAAEgHQADgGAAgMQAAgLgDgHQgEgGgMAAIgMAAgAusgjIAAidIBDAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAwGgjIAAidIAjAAQAJAAAHACQAHADAFAGQAGAGACAIQADAIgBAOQABAKgCAIQgBAHgEAHQgEAHgJAFQgHAEgMABIgMAAIAAA9gAvvh2IALAAQANAAADgHQACgHABgMQgBgLgCgIQgCgIgNAAIgMAAgAwngjIgUg1IgTA1IgYAAIAghTIgdhKIAXAAIARAuIARguIAYAAIgeBKIAgBTgAysgjIAAidIBCAAIAAAVIgsAAIAAAuIAmAAIAAAVIgmAAIAAAuIAsAAIAAAXgAgFhVIAAgWIBBAAIAAAWg");
	this.shape.setTransform(2.9,2);

	this.instance = new lib.cloud5();
	this.instance.setTransform(160.5,-73.5,1,1,0,0,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-160.5,-73.5,321,147);


(lib.mc12 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AGHFoQgNgNAAgTIAAgKIAbAAIAAAJQAAAHAEAFQAFAHAJgBQAOAAADgHQACgHAAgNQAAgNgCgFQgBgFgIgDIgCgBIgBgBIgTgIQgSgHgGgLQgFgMgBgRQAAgWAMgOQALgPAXAAQATAAAMAMQAMAMABAPIAAACIAAABIAAAMIgbAAIAAgFQAAgJgEgHQgEgGgKgBQgLABgDAHQgEAHAAAKQAAAJACAFQADAGAIADIABAAIABABIAUAIQASAGAFAMQAGAMgBAVQABAXgLAPQgKAQgaAAQgSAAgOgMgAC5FoQgOgNAAgTIAAgKIAbAAIAAAJQAAAHAFAFQAEAHAKgBQAOAAACgHQADgHAAgNQAAgNgCgFQgCgFgHgDIgCgBIgCgBIgTgIQgRgHgGgLQgGgMAAgRQAAgWAMgOQALgPAXAAQATAAAMAMQALAMACAPIAAACIAAABIAAAMIgbAAIAAgFQAAgJgEgHQgFgGgKgBQgKABgEAHQgEAHAAAKQABAJACAFQACAGAJADIABAAIABABIAUAIQASAGAFAMQAFAMAAAVQAAAXgLAPQgJAQgaAAQgTAAgNgMgAncF0IgCAAIgBAAQgQgCgNgKQgNgMAAgVIAAhiQAAgWANgLQANgLAQgBIABAAIACAAIABAAIADAAQAPABANALQANALgBAWIAAAJIgaAAIAAgJQAAgKgFgEQgGgEgHgBQgIABgEAEQgGAEAAAKIAABiQAAAKAGAEQAEAEAIAAQAHAAAGgEQAFgEAAgKIAAgjIgVAAIAAgYIAvAAIAAA7QABAVgNAMQgNAKgPACIgDAAIgBAAgAL8FzIAAhxIAAAAIgbBPIgMAAIgahPIgBAAIAABxIgaAAIAAi6IAZAAIAiBiIABAAIAhhiIAaAAIAAC6gAI/FzIAAi6IBPAAIAAAZIg0AAIAAA2IAtAAIAAAaIgtAAIAAA2IA0AAIAAAbgAH7FzIAAihIggAAIAAgZIBZAAIAAAZIgfAAIAAChgAEzFzIAAhPIgkhrIAcAAIAVBKIABAAIAVhKIAcAAIglBrIAABPgAA8FzIAAihIggAAIAAgZIBYAAIAAAZIgeAAIAAChgAgFFzIgohwIgBAAIAABwIgbAAIAAi6IAaAAIAoBwIAAAAIAAhwIAZAAIAAC6gAioFzIAAi6IBPAAIAAAZIg0AAIAAA2IAtAAIAAAaIgtAAIAAA2IA0AAIAAAbgAjbFzIAAhxIAAAAIgbBPIgNAAIgahPIgBAAIAABxIgaAAIAAi6IAZAAIAiBiIABAAIAihiIAaAAIAAC6gAmYFzIAAi6IBPAAIAAAZIg0AAIAAA2IAtAAIAAAaIgtAAIAAA2IA0AAIAAAbgAovFzIgIgoIgkAAIgIAoIgaAAIAqi6IAVAAIAqC6gApWEyIAbAAIgOhEIAAAAgAqiFzIgohwIgBAAIAABwIgaAAIAAi6IAZAAIAoBwIAAAAIAAhwIAbAAIAAC6gAsNFzIgHgoIgkAAIgIAoIgaAAIApi6IAWAAIApC6gAs0EyIAbAAIgNhEIgBAAgAuCFzIAAhxIgBAAIgaBPIgNAAIgahPIAAAAIAABxIgbAAIAAi6IAaAAIAiBiIAAAAIAihiIAZAAIAAC6gAJOBdIgCAAIgCAAQgPgBgNgLQgMgLAAgWIAAhgQAAgVAMgLQANgMAPgBIACAAIACAAIACAAIABAAQAQABAMAMQANALABAVIAABgQgBAWgNALQgMALgQABIgBAAIgCAAgAJBg9QgFAEAAAJIAABgQAAAKAFAFQAGAEAHAAQAHAAAFgEQAGgFAAgKIAAhgQAAgJgGgEQgFgFgHAAQgHAAgGAFgAFrBRQgNgMgBgUIAAgKIAbAAIAAAJQAAAHAFAGQAEAGAKAAQAOAAACgIQADgGAAgOQAAgNgCgEQgCgFgHgEIgCAAIgCgBIgTgJQgRgFgGgLQgGgLAAgSQAAgVAMgPQAKgPAXAAQAUABAMALQAMANABAPIAAABIAAABIAAANIgbAAIAAgFQAAgKgEgGQgEgHgLAAQgLAAgDAIQgDAHgBAJQAAAKACAFQADAFAJAEIABAAIABAAIAUAIQARAHAGAKQAFAMAAAUQAAAYgLAPQgKAPgZAAQgTAAgNgMgAEBBRQgNgMAAgTIAAiNIAaAAIAACLQAAAKAFAEQAFAFAHAAQAHAAAEgFQAFgEAAgKIAAiLIAbAAIAACNQAAATgNAMQgNAMgRAAQgSAAgMgMgACxBdIgEAAQgNgCgMgJQgMgKAAgWIAAhkQAAgSANgNQAMgLASgBQATAAAMANQAMANAAATIAAAKIgbAAIAAgJQAAgIgDgFQgFgGgIAAQgKAAgCAGQgEAGAAAJIAABdQAAAIAEAFQADAGAJAAIACAAIADAAQAEgBAEgFQADgEAAgJIAAgJIAbAAIAAALQAAASgMANQgNANgRgBIgDAAgAjWBRQgNgMABgUIAAgKIAbAAIAAAJQAAAHADAGQAFAGAKAAQAOAAADgIQACgGAAgOQAAgNgCgEQgCgFgHgEIgCAAIgCgBIgTgJQgRgFgGgLQgGgLAAgSQAAgVALgPQALgPAXAAQAUABAMALQALANACAPIAAABIAAABIAAANIgbAAIAAgFQAAgKgEgGQgEgHgLAAQgLAAgDAIQgDAHgBAJQABAKABAFQADAFAIAEIACAAIABAAIAUAIQASAHAEAKQAGAMAAAUQAAAYgKAPQgLAPgZAAQgTAAgOgMgAu5BdIgDAAQgOgCgLgJQgLgKAAgWIAAhkQgBgSAMgNQAMgLAUgBQASAAAMANQAMANAAATIAAAKIgbAAIAAgJQAAgIgDgFQgFgGgHAAQgLAAgDAGQgDAGAAAJIAABdQAAAIADAFQAEAGAJAAIACAAIACAAQAFgBAEgFQAEgEgBgJIAAgJIAbAAIAAALQAAASgMANQgMANgSgBIgEAAgAPGBcIgZhPIgQAAIAABPIgbAAIAAi3IArAAQAYAAAMAOQALANAAAbQAAAQgFAMQgFAKgNAGIAdBVgAOdgJIAPAAQAOABAEgIQADgHAAgPQAAgOgDgGQgFgIgOAAIgOAAgAMiBcIAAi3IBPAAIAAAZIg0AAIAAA2IAtAAIAAAXIgtAAIAAA3IA0AAIAAAagALvBcIAAhvIAAAAIgbBOIgNAAIgahOIgBAAIAABvIgaAAIAAi3IAZAAIAiBgIABAAIAihgIAaAAIAAC3gAHeBcIAAieIgfAAIAAgZIBYAAIAAAZIgfAAIAACegAgTBcIAAi3IAkAAQAXAAAMANQAMAMAAAXIAABUQAAAagNAMQgMANgYAAgAAFBDIAMAAQAMAAAEgGQAFgFgBgMIAAhXQAAgLgEgGQgEgGgMAAIgMAAgAh0BcIAAi3IBPAAIAAAZIg0AAIAAA2IAtAAIAAAXIgtAAIAAA3IA0AAIAAAagAkRBcIAAi3IAaAAIAAC3gAlyBcIAAi3IAbAAIAACdIA0AAIAAAagAmaBcIgHgoIgkAAIgIAoIgbAAIApi3IAXAAIAoC3gAnBAcIAaAAIgMhCIgBAAgAoIBcIgYhPIgQAAIAABPIgbAAIAAi3IArAAQAXAAAMAOQAMANAAAbQAAAQgGAMQgEAKgOAGIAdBVgAowgJIAOAAQAOABAEgIQAEgHAAgPQAAgOgEgGQgEgIgPAAIgNAAgAqQBcIAAieIgfAAIAAgZIBYAAIAAAZIgfAAIAACegArSBcIgphuIgBAAIAABuIgaAAIAAi3IAZAAIApBuIAAAAIAAhuIAbAAIAAC3gAt1BcIAAi3IBPAAIAAAZIg0AAIAAA2IAtAAIAAAXIgtAAIAAA3IA0AAIAAAagAKhi3IgBAAIgCgBQgPgBgNgKQgNgMAAgVIAAhiQAAgWANgKQANgMAPgBIACAAIABAAIACAAIACAAQAPABANAMQANAKAAAWIAAAJIgbAAIAAgJQAAgKgFgEQgFgEgIgBQgHABgFAEQgFAEgBAKIAABiQABAJAFAFQAFAEAHAAQAIAAAFgEQAFgFAAgJIAAgjIgUAAIAAgXIAvAAIAAA6QAAAVgNAMQgNAKgPABIgCABIgCAAgAm7i3IgCAAIgCgBQgPgBgNgKQgMgMAAgVIAAhiQAAgWAMgKQANgMAPgBIACAAIACAAIABAAIACAAQAQABAMAMQANAKABAWIAABiQgBAVgNAMQgMAKgQABIgCABIgBAAgAnIlUQgFAEAAAKIAABiQAAAJAFAFQAGAEAHAAQAHAAAFgEQAGgFAAgJIAAhiQAAgKgGgEQgFgEgHgBQgHABgGAEgAO4i4IgIgoIgkAAIgIAoIgaAAIAqi6IAVAAIApC6gAORj5IAbAAIgOhEIAAAAgALmi4IAAi6IBOAAIAAAZIg0AAIAAA2IAuAAIAAAaIguAAIAAA2IA0AAIAAAbgAJPi4IgIgoIgkAAIgIAoIgbAAIAqi6IAWAAIApC6gAIoj5IAaAAIgNhEIgBAAgAHbi4IgohwIAAAAIAABwIgbAAIAAi6IAZAAIApBwIAAAAIAAhwIAbAAIAAC6gAFxi4IgIgoIgkAAIgHAoIgbAAIApi6IAXAAIAoC6gAFKj5IAbAAIgOhEIAAAAgAD7i4IAAhxIAAAAIgbBPIgMAAIgahPIgBAAIAABxIgaAAIAAi6IAZAAIAiBiIABAAIAhhiIAaAAIAAC6gAAAi4IAAi6IAmAAQAXAAALANQANANAAAXIAABWQAAAagNAMQgNANgXAAgAAZjSIAMAAQAMAAAEgFQAFgGAAgLIAAhZQAAgMgEgFQgFgHgMAAIgMAAgAgwi4IgphwIAAAAIAABwIgbAAIAAi6IAaAAIAoBwIABAAIAAhwIAaAAIAAC6gAiai4IgIgoIgkAAIgIAoIgbAAIAqi6IAWAAIApC6gAjBj5IAaAAIgNhEIgBAAgAl3i4IAAi6IAoAAQALAAAIAEQAJACAGAIQAHAGADALQACAJAAARQAAALgBAKQgCAJgFAHQgFAJgJAFQgKAFgOABIgOAAIAABIgAldkaIAOAAQAPABADgKQADgIAAgOQAAgNgCgKQgDgJgPAAIgPAAgApGi4IAAi6IAaAAIAACfIA1AAIAAAbgAqmi4IAAi6IBPAAIAAAZIg0AAIAAA2IAtAAIAAAaIgtAAIAAA2IA0AAIAAAbgAr0i4Igli6IAcAAIAVCCIAAAAIAWiCIAcAAIgmC6gAtwi4IAAi6IBPAAIAAAZIg1AAIAAA2IAuAAIAAAaIguAAIAAA2IA1AAIAAAbgAvfi4IAAi6IAnAAQAWAAAMANQANANAAAXIAABWQAAAagNAMQgNANgXAAgAvEjSIALAAQAMAAAEgFQAFgGAAgLIAAhZQAAgMgEgFQgFgHgMAAIgLAAg");
	this.shape.setTransform(11.3,22.8,0.929,0.929);

	// Layer 1
	this.instance = new lib.cloud1();
	this.instance.setTransform(157,98,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-157,-98,314,196);


(lib.mc11 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("ALdCdQgKgKAAgNIAAgHIATAAIAAAGQAAAFAEAFQADADAHAAQAKAAACgEQACgGAAgJQAAgKgBgEQgCgEgFgCIgBAAIgCgBIgOgGQgMgEgFgJQgEgJAAgNQAAgPAIgLQAJgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgEQgDgFgHAAQgIAAgDAGQgDAFAAAGQAAAIACADQACAEAGADIABAAIAAAAIAPAHQANAEAEAJQAEAIAAAQQAAAQgIAMQgHALgTAAQgOAAgJgJgAA5CmIgBAAIgBgBQgMAAgJgIQgJgJAAgPIAAhHQAAgQAJgIQAJgJAMgBIABAAIABAAIABAAIACAAQALABAJAJQAKAIAAAQIAAAGIgUAAIAAgGQAAgIgEgCQgDgEgGAAQgFAAgEAEQgEACAAAIIAABHQAAAHAEAEQAEACAFAAQAGAAADgCQAEgEAAgHIAAgZIgPAAIAAgRIAjAAIAAAqQAAAPgKAJQgJAIgLAAIgCABIgBAAgAKQClIAAiIIAUAAIAAB0IAmAAIAAAUgAJzClIgFgdIgbAAIgFAdIgUAAIAeiIIARAAIAeCIgAJXB2IATAAIgJgyIgBAAgAIcClIAAiIIAUAAIAACIgAH8ClIgSg6IgMAAIAAA6IgTAAIAAiIIAfAAQARAAAJALQAIAJAAAVQAAALgDAJQgEAIgJAFIAUA+gAHeBaIALAAQAKAAADgGQADgFAAgKQAAgKgDgFQgDgGgLAAIgKAAgAGEClIAAiIIA6AAIAAATIgmAAIAAAoIAhAAIAAASIghAAIAAAnIAmAAIAAAUgAFSClIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAEoClIgFgdIgbAAIgFAdIgUAAIAeiIIARAAIAeCIgAEMB2IATAAIgJgyIgBAAgADSClIAAhTIAAAAIgTA7IgKAAIgTg7IAAAAIAABTIgUAAIAAiIIATAAIAZBIIAAAAIAZhIIATAAIAACIgAgIClIgehSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIARAAIAACIgAhfClIAAiIIATAAIAACIgAiSClIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAjlClIAAiIIA6AAIAAATIgnAAIAAAoIAiAAIAAASIgiAAIAAAnIAnAAIAAAUgAkCClIgWg8IgLAUIAAAoIgTAAIAAiIIATAAIAAA+IABAAIAcg+IATAAIgaA3IAfBRgAlWClIgSg6IgMAAIAAA6IgTAAIAAiIIAfAAQARAAAJALQAIAJAAAVQAAALgDAJQgEAIgJAFIAUA+gAl0BaIALAAQAKAAADgGQADgFAAgKQAAgKgDgFQgDgGgLAAIgKAAgAmkClIgGgdIgaAAIgGAdIgTAAIAeiIIAQAAIAeCIgAnBB2IATAAIgJgyIAAAAgAn6ClIAAhTIgBAAIgTA7IgJAAIgTg7IgBAAIAABTIgTAAIAAiIIASAAIAZBIIABAAIAYhIIATAAIAACIgAqUClIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gArFClIgdhSIgBAAIAABSIgTAAIAAiIIATAAIAdBSIAAAAIAAhSIAUAAIAACIgAscClIAAiIIAUAAIAACIgAs8ClIgSg6IgMAAIAAA6IgTAAIAAiIIAfAAQARAAAJALQAIAJAAAVQAAALgDAJQgEAIgJAFIAUA+gAtaBaIALAAQAKAAADgGQADgFAAgKQAAgKgDgFQgDgGgLAAIgKAAgAu7ClIAAiIIAdAAQAIABAGACQAHACAFAFQAFAFABAIQACAHAAAMIgBAPQgBAGgDAGQgEAHgHAEQgHADgKAAIgKAAIAAA1gAunBeIAJAAQAMAAACgGQACgHAAgKQAAgJgCgHQgCgHgLAAIgKAAgAOcgbIgBAAIgBgBQgMgBgJgHQgJgJAAgPIAAhIQAAgPAJgJQAJgIAMgBIABAAIABAAIABAAIACAAQALABAJAIQAKAJAAAPIAAAHIgUAAIAAgHQAAgHgEgDQgDgEgGAAQgFAAgEAEQgEADAAAHIAABIQAAAGAEAEQAEADAFAAQAGAAADgDQAEgEAAgGIAAgaIgPAAIAAgRIAjAAIAAArQAAAPgKAJQgJAHgLABIgCABIgBAAgAFFgbIgBAAIgBgBQgMgBgJgHQgJgJAAgPIAAhIQAAgPAJgJQAJgIAMgBIABAAIABAAIABAAIACAAQALABAJAIQAKAJAAAPIAABIQAAAPgKAJQgJAHgLABIgCABIgBAAgAE8iOQgEADAAAHIAABIQAAAGAEAEQAEADAFAAQAGAAADgDQAEgEAAgGIAAhIQAAgHgEgDQgDgEgGAAQgFAAgEAEgAD0gbIgCgBQgKgBgIgHQgJgHAAgRIAAhJQAAgOAJgJQAJgJAOAAQANAAAJAJQAJAJAAAPIAAAHIgTAAIAAgHQAAgFgEgEQgDgEgFgBQgIAAgCAFQgCAFAAAGIAABGQAAAFACAEQADAEAGAAIACAAIABAAQAEgBADgDQADgDAAgGIAAgHIATAAIAAAIQAAANgJAJQgJAKgNAAIgDAAgABugkQgKgKAAgOIAAgHIATAAIAAAHQAAAEAEAFQADAEAHAAQAKAAACgFQACgGAAgJQAAgKgBgDQgCgEgFgDIgBAAIgCAAIgOgGQgMgFgFgIQgEgJAAgNQAAgPAIgLQAJgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgFgHAAQgIAAgDAGQgDAFAAAHQAAAHACAEQACADAGADIABABIAAAAIAPAGQANAEAEAKQAEAIAAAPQAAARgIALQgHAMgTAAQgOgBgJgIgAkPgbIgDgBQgKgBgIgHQgIgHAAgRIAAhJQAAgOAIgJQAJgJAOAAQAOAAAJAJQAIAJAAAPIAAAHIgTAAIAAgHQAAgFgDgEQgDgEgGgBQgHAAgCAFQgDAFAAAGIAABGQAAAFADAEQACAEAHAAIABAAIACAAQADgBADgDQADgDAAgGIAAgHIATAAIAAAIQAAANgIAJQgJAKgNAAIgDAAgAnngkQgKgJAAgOIAAhpIAUAAIAABnQAAAHADADQAEAEAFAAQAFAAADgEQAEgDAAgHIAAhnIATAAIAABpQAAAOgJAJQgJAIgNABQgNgBgJgIgAoigbIgCAAIgBgBQgLgBgJgHQgKgJAAgPIAAhIQAAgPAKgJQAJgIALgBIABAAIACAAIABAAIABAAQAMABAJAIQAJAJAAAPIAABIQAAAPgJAJQgJAHgMABIgBABIgBAAgAoriOQgEADAAAHIAABIQAAAGAEAEQADADAGAAQAFAAAEgDQAEgEAAgGIAAhIQAAgHgEgDQgEgEgFAAQgGAAgDAEgAubgbIgCAAIgBgBQgLgBgJgHQgKgJAAgPIAAhIQAAgPAKgJQAJgIALgBIABAAIACAAIABAAIABAAQAMABAJAIQAJAJAAAPIAAAHIgTAAIAAgHQAAgHgEgDQgEgEgFAAQgGAAgDAEQgEADAAAHIAABIQAAAGAEAEQADADAGAAQAFAAAEgDQAEgEAAgGIAAgaIgQAAIAAgRIAjAAIAAArQAAAPgJAJQgJAHgMABIgBABIgBAAgANZgdIgehRIAAAAIAABRIgTAAIAAiHIASAAIAdBSIABAAIAAhSIATAAIAACHgAMCgdIAAiHIATAAIAACHgAK7gdIAAiHIATAAIAAB0IAnAAIAAATgAJ0gdIAAiHIAUAAIAAB0IAmAAIAAATgAIugdIAAiHIA6AAIAAASIgnAAIAAAoIAiAAIAAATIgiAAIAAAnIAnAAIAAATgAHggdIAAiHIAdAAQAIAAAGADQAHACAFAEQAFAGABAHQACAIAAALIgBAQQgBAGgDAFQgEAHgHAEQgHAEgKAAIgKAAIAAA0gAH0hjIAJAAQAMAAACgHQACgGAAgLQAAgJgCgHQgCgGgLgBIgKAAgAG7gdIAAhSIgBAAIgTA7IgJAAIgTg7IgBAAIAABSIgTAAIAAiHIASAAIAZBIIABAAIAYhIIATAAIAACHgAA2gdIAAh1IgXAAIAAgSIBAAAIAAASIgWAAIAAB1gAAFgdIgchRIAAAAIAABRIgTAAIAAiHIASAAIAbBSIABAAIAAhSIATAAIAACHgAhwgdIAAiHIA6AAIAAASIgnAAIAAAoIAiAAIAAATIgiAAIAAAnIAnAAIAAATgAiXgdIAAiHIAUAAIAACHgAjegdIAAiHIAUAAIAAB0IAmAAIAAATgAlvgdIgSg6IgLAAIAAA6IgUAAIAAiHIAgAAQARAAAIALQAJAJAAAUQAAALgEAJQgEAIgJAGIAVA9gAmMhoIALAAQAKAAADgFQACgFAAgKQAAgLgDgFQgDgGgKAAIgKAAgAp5gdIAAg5IgbhOIAVAAIAPA2IABAAIAPg2IAVAAIgbBOIAAA5gAr1gdIAAiHIA6AAIAAASIgnAAIAAAoIAiAAIAAATIgiAAIAAAnIAnAAIAAATgAsugdIgbiHIAUAAIAQBfIAAAAIAQhfIAUAAIgcCHgAtpgdIAAiHIATAAIAACHg");
	this.shape.setTransform(-3.5,1);

	// Layer 1
	this.instance = new lib.cloud4();
	this.instance.setTransform(-130,84.5,1,1,0,180,0);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc10 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AQuCiQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAFQADADAIAAQAJAAADgEQACgGAAgKQAAgJgCgDQgBgEgGgDIgBAAIgCgBIgNgGQgNgEgEgJQgEgIAAgOQAAgPAIgLQAIgKARgBQAOABAJAIQAIAKABAKIAAACIAAAKIgTAAIAAgFQAAgGgDgFQgEgFgHAAQgIAAgDAFQgCAGAAAGQAAAIABAEQACADAGADIABAAIABAAIAPAGQANAFADAJQAEAJAAAOQAAARgHALQgIAMgSAAQgOAAgKgJgAJkCiQgJgJAAgNIAAhqIATAAIAABoQAAAGAEAEQADAEAFgBQAFABAEgEQADgEAAgGIAAhoIAUAAIAABqQAAANgKAJQgJAJgNAAQgNAAgJgJgAIpCrIgBAAIgBgBQgMAAgJgIQgJgJAAgPIAAhHQAAgQAJgIQAJgJAMgBIABAAIABAAIABAAIACAAQALABAJAJQAKAIAAAQIAABHQAAAPgKAJQgJAIgLAAIgCABIgBAAgAIgA5QgEACAAAIIAABHQAAAHAEADQAEADAFAAQAGAAADgDQAEgDAAgHIAAhHQAAgIgEgCQgDgEgGAAQgFAAgEAEgAC3CiQgKgKAAgNIAAgHIATAAIAAAGQAAAFAEAFQADADAHAAQAKAAACgEQACgGAAgKQAAgJgBgDQgCgEgFgDIgBAAIgCgBIgOgGQgMgEgFgJQgEgIAAgOQAAgPAIgLQAJgKAQgBQAOABAJAIQAJAKABAKIAAACIAAAKIgUAAIAAgFQAAgGgDgFQgDgFgHAAQgIAAgDAFQgDAGAAAGQAAAIACAEQACADAGADIABAAIAAAAIAPAGQANAFAEAJQAEAJAAAOQAAARgIALQgHAMgTAAQgOAAgJgJgAArCrIgCAAIgBgBQgLAAgJgIQgKgJAAgPIAAhHQAAgQAKgIQAJgJALgBIABAAIACAAIABAAIABAAQAMABAJAJQAJAIAAAQIAABHQAAAPgJAJQgJAIgMAAIgBABIgBAAgAAiA5QgEACAAAIIAABHQAAAHAEADQADADAGAAQAFAAAEgDQAEgDAAgHIAAhHQAAgIgEgCQgEgEgFAAQgGAAgDAEgAtaCiQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAFQADADAIAAQAJAAADgEQACgGAAgKQAAgJgCgDQgBgEgGgDIgBAAIgCgBIgNgGQgNgEgEgJQgEgIAAgOQAAgPAIgLQAIgKARgBQAOABAJAIQAIAKABAKIAAACIAAAKIgTAAIAAgFQAAgGgDgFQgEgFgHAAQgIAAgDAFQgCAGAAAGQAAAIABAEQACADAGADIABAAIABAAIAPAGQANAFADAJQAEAJAAAOQAAARgHALQgIAMgSAAQgOAAgKgJgAvmCrIgBAAIgBgBQgMAAgJgIQgJgJAAgPIAAhHQAAgQAJgIQAJgJAMgBIABAAIABAAIABAAIACAAQALABAJAJQAKAIAAAQIAAAHIgUAAIAAgHQAAgIgEgCQgDgEgGAAQgFAAgEAEQgEACAAAIIAABHQAAAHAEADQAEADAFAAQAGAAADgDQAEgDAAgHIAAgaIgPAAIAAgRIAjAAIAAArQAAAPgKAJQgJAIgLAAIgCABIgBAAgAxzCiQgKgKAAgNIAAgHIATAAIAAAGQAAAFAEAFQADADAHAAQAKAAACgEQACgGAAgKQAAgJgBgDQgCgEgFgDIgBAAIgCgBIgOgGQgMgEgFgJQgEgIAAgOQAAgPAIgLQAJgKAQgBQAOABAJAIQAJAKABAKIAAACIAAAKIgUAAIAAgFQAAgGgDgFQgDgFgHAAQgIAAgDAFQgDAGAAAGQAAAIACAEQACADAGADIABAAIAAAAIAPAGQANAFAEAJQAEAJAAAOQAAARgIALQgHAMgTAAQgOAAgJgJgATbCqIAAiIIA6AAIAAATIgmAAIAAAoIAhAAIAAASIghAAIAAAnIAmAAIAAAUgASpCqIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAR2CqIAAiIIATAAIAACIgAPXCqIAAiIIAdAAQASAAAIALQAIALAAANIAAAFQAAALgDAFQgEAFgGAEQAHADADAHQADAGAAAKIAAAIQAAASgJAJQgIAKgTAAgAPrCWIAIAAQAMAAACgGQADgGAAgKQAAgKgDgGQgDgEgLgBIgIAAgAPrBaIAIAAQAKAAAEgFQADgEAAgKQAAgJgEgEQgEgFgJAAIgIAAgAORCqIAAiIIA6AAIAAATIgnAAIAAAoIAiAAIAAASIgiAAIAAAnIAnAAIAAAUgANfCqIgPhYIgBAAIgOBYIgTAAIgUiIIAVAAIAKBXIAPhXIAPAAIAQBYIAAAAIAJhYIAVAAIgUCIgALdCqIgSg6IgMAAIAAA6IgTAAIAAiIIAfAAQARAAAJALQAIAJAAAVQAAALgDAIQgEAJgJAGIAUA9gAK/BfIALAAQAKAAADgGQADgFAAgKQAAgKgDgGQgDgFgLAAIgKAAgAHSCqIAAg5IgahPIAUAAIAQA2IAAAAIAQg2IAUAAIgaBPIAAA5gAF5CqIgehSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIATAAIAACIgAEiCqIAAiIIATAAIAACIgACNCqIgehSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIATAAIAACIgAgZCqIAAiIIATAAIAACIgAhMCqIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAh2CqIgFgdIgbAAIgFAdIgUAAIAeiIIARAAIAeCIgAiSB6IATAAIgJgwIgBAAgAjMCqIAAhTIAAAAIgTA7IgKAAIgTg7IAAAAIAABTIgUAAIAAiIIATAAIAZBIIAAAAIAZhIIATAAIAACIgAk2CqIAAiIIATAAIAACIgAlbCqIgdhSIgBAAIAABSIgTAAIAAiIIATAAIAdBSIAAAAIAAhSIAUAAIAACIgAmoCqIgGgdIgaAAIgGAdIgTAAIAeiIIAQAAIAeCIgAnFB6IATAAIgJgwIAAAAgApNCqIAAiIIAdAAQAQAAAJAKQAJAKAAAPIAABAQAAASgKAKQgJAJgRAAgAo6CXIAJAAQAIAAAEgDQADgEAAgJIAAhCQAAgHgDgEQgDgFgJAAIgJAAgApxCqIgehSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIATAAIAACIgAq/CqIgFgdIgbAAIgFAdIgUAAIAeiIIARAAIAeCIgArbB6IATAAIgJgwIgBAAgAuECqIgdhSIgBAAIAABSIgTAAIAAiIIATAAIAdBSIAAAAIAAhSIAUAAIAACIgAwsCqIAAiIIAUAAIAACIgAzACqIAAiIIA6AAIAAATIgmAAIAAAoIAhAAIAAASIghAAIAAAnIAmAAIAAAUgA0RCqIAAiIIAdAAQAQAAAJAKQAJAKAAAPIAABAQAAASgJAKQgKAJgRAAgAz9CXIAJAAQAIAAADgDQAEgEAAgJIAAhCQAAgHgEgEQgDgFgIAAIgJAAgALUggIgBAAIgBgBQgMgBgJgHQgJgJAAgQIAAhHQAAgQAJgIQAJgIAMgBIABAAIABAAIABAAIACAAQALABAJAIQAKAIAAAQIAAAHIgUAAIAAgHQAAgHgEgDQgDgDgGAAQgFAAgEADQgEADAAAHIAABHQAAAHAEAEQAEADAFAAQAGAAADgDQAEgEAAgHIAAgZIgPAAIAAgRIAjAAIAAAqQAAAQgKAJQgJAHgLABIgCABIgBAAgAEfgpQgKgJAAgOIAAhpIAUAAIAABnQAAAHADADQAEAEAFAAQAFAAADgEQAEgDAAgHIAAhnIATAAIAABpQAAAOgJAJQgJAIgNABQgNgBgJgIgAAAgpQgJgKAAgOIAAgHIARAAIAAAGQAAAFAEAFQADAEAHAAQAKAAACgFQACgGAAgJQAAgJgBgEQgCgEgFgDIgBAAIgCgBIgOgFQgLgFgEgIQgEgJAAgNQAAgPAIgLQAHgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgFgHABQgIgBgDAGQgDAGAAAGQAAAIACADQACADAGADIABABIAAAAIAPAGQANAEAEAKQAEAIAAAPQAAARgIALQgHAMgTAAQgOgBgIgIgAmUgpQgKgJAAgOIAAhpIAUAAIAABnQAAAHADADQAEAEAFAAQAFAAADgEQAEgDAAgHIAAhnIATAAIAABpQAAAOgJAJQgJAIgNABQgNgBgJgIgAnlgpQgJgKAAgOIAAgHIATAAIAAAGQAAAFADAFQADAEAIAAQAJAAADgFQACgGAAgJQAAgJgCgEQgBgEgGgDIgBAAIgCgBIgNgFQgNgFgEgIQgEgJAAgNQAAgPAIgLQAIgLARAAQAOAAAJAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgFgHABQgIgBgDAGQgCAGAAAGQAAAIABADQACADAGADIABABIABAAIAPAGQANAEADAKQAEAIAAAPQAAARgHALQgIAMgSAAQgOgBgKgIgAsQgpQgKgKAAgOIAAgHIATAAIAAAGQAAAFAEAFQADAEAHAAQAKAAACgFQACgGAAgJQAAgJgBgEQgCgEgFgDIgBAAIgCgBIgOgFQgMgFgFgIQgEgJAAgNQAAgPAIgLQAJgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgFgHABQgIgBgDAGQgDAGAAAGQAAAIACADQACADAGADIABABIAAAAIAPAGQANAEAEAKQAEAIAAAPQAAARgIALQgHAMgTAAQgOgBgJgIgAuVggIgDgBQgKgBgIgHQgIgHAAgRIAAhJQAAgOAIgJQAJgJAOAAQAOAAAJAJQAIAKAAANIAAAIIgTAAIAAgHQAAgFgDgFQgDgDgGAAQgHgBgCAFQgDAFAAAGIAABFQAAAGADAEQACAEAHAAIABAAIACAAQADgBADgDQADgDAAgHIAAgGIATAAIAAAIQAAANgIAJQgJAKgNAAIgDAAgAxTggIgCAAIgBgBQgLgBgJgHQgKgJAAgQIAAhHQAAgQAKgIQAJgIALgBIABAAIACAAIABAAIABAAQAMABAJAIQAJAIAAAQIAABHQAAAQgJAJQgJAHgMABIgBABIgBAAgAxciTQgEADAAAHIAABHQAAAHAEAEQADADAGAAQAFAAAEgDQAEgEAAgHIAAhHQAAgHgEgDQgEgDgFAAQgGAAgDADgA0LgpQgJgKAAgOIAAgHIATAAIAAAGQAAAFADAFQADAEAIAAQAJAAADgFQACgGAAgJQAAgJgCgEQgBgEgGgDIgBAAIgCgBIgNgFQgNgFgEgIQgEgJAAgNQAAgPAIgLQAIgLARAAQAOAAAJAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgFgHABQgIgBgDAGQgCAGAAAGQAAAIABADQACADAGADIABABIABAAIAPAGQANAEADAKQAEAIAAAPQAAARgHALQgIAMgSAAQgOgBgKgIgAKRghIgehSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIATAAIAACIgAI6ghIAAiIIATAAIAACIgAIHghIAAh1IgWAAIAAgTIBAAAIAAATIgWAAIAAB1gAHdghIgFgeIgbAAIgFAeIgUAAIAeiIIARAAIAeCIgAHBhRIATAAIgJgxIgBAAgAFmghIAAiIIAUAAIAAB0IAmAAIAAAUgADwghIAAhTIgBAAIgTA7IgJAAIgTg7IgBAAIAABTIgTAAIAAiIIASAAIAZBIIABAAIAYhIIATAAIAACIgACFghIAAiIIAUAAIAACIgABTghIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gAhgghIAAg6IgbhOIAVAAIAPA2IABAAIAPg2IAVAAIgbBOIAAA6gAi5ghIAAiIIAUAAIAAB0IAmAAIAAAUgAj/ghIAAiIIATAAIAAB0IAnAAIAAAUgAkcghIgGgeIgaAAIgGAeIgTAAIAeiIIAQAAIAeCIgAk5hRIATAAIgJgxIAAAAgAoRghIAAiIIATAAIAACIgApKghIgciIIAVAAIAPBeIABAAIAPheIAVAAIgcCIgArJghIAAiIIA6AAIAAATIgnAAIAAAnIAiAAIAAASIgiAAIAAAoIAnAAIAAAUgAszghIgGgeIgaAAIgGAeIgTAAIAeiIIAQAAIAeCIgAtQhRIATAAIgJgxIAAAAgAvjghIgPhYIgBAAIgOBYIgTAAIgUiIIAVAAIAKBWIAPhWIAPAAIAQBYIAAAAIAJhYIAVAAIgUCIgAyYghIAAg8IgYAAIAAA8IgUAAIAAiIIAUAAIAAA6IAYAAIAAg6IATAAIAACIg");
	this.shape.setTransform(5.8,-5.4);

	// Layer 1
	this.instance = new lib.cloud5();
	this.instance.setTransform(160.5,73.5,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-160.5,-73.5,321,147);


(lib.mc9 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AM3CrQgKgKAAgOIAAgHIAUAAIAAAGQAAAGAEAEQADAFAIgBQAKAAACgFQACgGAAgKQAAgKgBgEQgCgDgFgDIgCgBIgBgBIgPgFQgNgGgFgJQgEgJAAgNQAAgRAJgKQAIgMASAAQAPAAAJAJQAJAKABAMIAAABIAAAKIgVAAIAAgEQAAgHgDgGQgDgEgIAAQgIgBgDAGQgDAFAAAIQAAAHACAEQACAEAGADIABABIABAAIAPAGQAOAFAEAJQAEAJAAAQQAAASgIALQgIAMgTAAQgPABgKgKgAAaC0IgCAAIgBAAIgBAAQgMAAgKgJQgIgJAAgQIAAhMQAAgPAIgKQAKgIAMgBIABAAIABAAIACAAIABAAQAMABAKAIQAKAKAAAPIAABMQAAAQgKAJQgKAJgMAAIgBAAgAAPA8QgEAEAAAGIAABMQAAAIAEADQAEADAFAAQAGAAAEgDQAEgDAAgIIAAhMQAAgGgEgEQgEgEgGABQgFgBgEAEgAj4C0IgCAAQgLgBgJgIQgIgGAAgSIAAhOQAAgPAJgJQAJgJAPAAQAOAAAJAJQAKAKAAAPIAAAIIgVAAIAAgHQAAgGgDgFQgEgDgFAAQgIgBgCAFQgDAEAAAIIAABJQAAAGADAEQADAFAGgBIACAAIACAAQADgBADgDQADgDAAgHIAAgHIAVAAIAAAJQAAANgKAJQgJAKgOAAIgDAAgAnjCrQgJgKAAgNIAAhvIAUAAIAABtQAAAHAEAEQADADAGAAQAFAAAEgDQADgEAAgHIAAhtIAVAAIAABvQAAANgKAKQgKAKgNgBQgOABgKgKgAr9C0IgBAAIgBAAIgCAAQgMAAgKgJQgJgJAAgQIAAhMQAAgPAJgKQAKgIAMgBIACAAIABAAIABAAIABAAQAMABAKAIQAKAKAAAPIAABMQAAAQgKAJQgKAJgMAAIgBAAgAsIA8QgEAEAAAGIAABMQAAAIAEADQAEADAGAAQAFAAAFgDQAEgDAAgIIAAhMQAAgGgEgEQgFgEgFABQgGgBgEAEgAtTC0IgDAAQgKgBgJgIQgJgGAAgSIAAhOQAAgPAJgJQAKgJAOAAQAPAAAJAJQAJAKAAAPIAAAIIgUAAIAAgHQAAgGgEgFQgDgDgGAAQgHgBgDAFQgCAEAAAIIAABJQAAAGACAEQADAFAHgBIACAAIABAAQAEgBADgDQADgDAAgHIAAgHIAUAAIAAAJQAAANgJAJQgKAKgNAAIgDAAgALnCzIAAiOIAUAAIAAB6IApAAIAAAUgALICzIgGgeIgbAAIgGAeIgVAAIAgiOIARAAIAgCOgAKqCCIAVAAIgKg0IgBAAgAJtCzIAAiOIAUAAIAACOgAJLCzIgTg8IgMAAIAAA8IgVAAIAAiOIAhAAQASAAAJALQAJAKAAAVQAAAMgEAJQgEAJgJAFIAWBBgAIsBlIALAAQALAAADgGQADgGAAgLQAAgLgDgFQgDgGgMABIgKAAgAHNCzIAAiOIA9AAIAAAUIgoAAIAAApIAjAAIAAATIgjAAIAAAqIAoAAIAAAUgAGYCzIAAh6IgXAAIAAgUIBDAAIAAAUIgXAAIAAB6gAFsCzIgGgeIgbAAIgGAeIgVAAIAgiOIARAAIAgCOgAFOCCIAVAAIgKg0IgBAAgAESCzIAAhWIgVA9IgKAAIgUg9IAAAAIAABWIgVAAIAAiOIAUAAIAaBMIABAAIAZhMIAUAAIAACOgAB/CzIgfhVIAAAAIAABVIgUAAIAAiOIATAAIAfBWIAAAAIAAhWIAVAAIAACOgAgvCzIAAiOIAUAAIAACOgAhlCzIAAh6IgXAAIAAgUIBDAAIAAAUIgXAAIAAB6gAiRCzIgGgeIgbAAIgGAeIgVAAIAgiOIARAAIAgCOgAivCCIAVAAIgKg0IgBAAgAk+CzIAAiOIAVAAIAACOgAlkCzIgfhVIgBAAIAABVIgUAAIAAiOIAUAAIAeBWIABAAIAAhWIAUAAIAACOgAoUCzIAAhWIgUA9IgKAAIgUg9IgBAAIAABWIgUAAIAAiOIATAAIAbBMIAAAAIAahMIAUAAIAACOgAqDCzIAAhWIgUA9IgKAAIgUg9IgBAAIAABWIgUAAIAAiOIATAAIAbBMIAAAAIAahMIAUAAIAACOgACKgqQgFADgFACQgGACgGAAIgBAAIgBAAQgMgBgKgJQgKgIAAgRIAAhLQAAgQAKgJQAKgJAMgBIABAAIABAAIACAAIABAAQAMABAKAJQAKAJAAAQIAABLIgBAIIgCAHIAMAKIgKANgABribQgEADAAAHIAABLQAAAIAEADQAEAEAFAAIADgBIACAAIgJgIIAKgMIAIAHIAAhMQAAgHgEgDQgEgEgGAAQgFAAgEAEgACygtQgKgJAAgOIAAhvIAVAAIAABtQAAAHADAEQAEAEAFAAQAGAAADgEQAEgEAAgHIAAhtIAUAAIAABvQAAAOgJAJQgKAKgOAAQgNAAgKgKgAiAgjIgCAAIgBAAIgBAAQgMgBgKgJQgKgIAAgRIAAhLQAAgQAKgJQAKgJAMgBIABAAIABAAIACAAIABAAQAMABAKAJQAKAJAAAQIAAAIIgVAAIAAgIQAAgHgEgDQgEgEgGAAQgFAAgEAEQgEADAAAHIAABLQAAAIAEADQAEAEAFAAQAGAAAEgEQAEgDAAgIIAAgbIgQAAIAAgSIAlAAIAAAtQAAARgKAIQgKAJgMABIgBAAgAnKgjIgBAAIgBAAIgCAAQgMgBgKgJQgJgIAAgRIAAhLQAAgQAJgJQAKgJAMgBIACAAIABAAIABAAIABAAQAMABAKAJQAKAJAAAQIAABLQAAARgKAIQgKAJgMABIgBAAgAnVibQgEADAAAHIAABLQAAAIAEADQAEAEAGAAQAFAAAFgEQAEgDAAgIIAAhLQAAgHgEgDQgFgEgFAAQgGAAgEAEgAIXgkIAAg8IgchTIAWAAIAQA6IABAAIAQg6IAWAAIgcBTIAAA8gAHPgkIAAh7IgXAAIAAgUIBDAAIAAAUIgXAAIAAB7gAGZgkIAAiPIAVAAIAACPgAFPgkIAAiPIAUAAIAAB7IApAAIAAAUgAEwgkIgGgeIgbAAIgGAeIgVAAIAgiPIARAAIAgCPgAEShWIAVAAIgKgzIgBAAgAgggkIAAhAIgZAAIAABAIgVAAIAAiPIAVAAIAAA9IAZAAIAAg9IAUAAIAACPgAjLgkIAAiPIAUAAIAACPgAjzgkIAAhAIgZAAIAABAIgVAAIAAiPIAVAAIAAA9IAZAAIAAg9IAUAAIAACPgAmXgkIAAiPIAeAAQAJAAAGADQAHACAFAFQAFAGACAIQACAHAAANQAAAJgBAHQgBAHgEAFQgEAHgHAFQgHADgLAAIgLAAIAAA4gAmDhvIAKAAQAMAAADgHQACgGAAgLQAAgKgCgHQgCgHgMAAIgLAAgAo2gkIAAiPIAUAAIAAB7IApAAIAAAUgAqAgkIAAiPIA9AAIAAAUIgpAAIAAApIAjAAIAAAUIgjAAIAAAqIApAAIAAAUgAq8gkIgdiPIAWAAIAQBkIABAAIAQhkIAWAAIgeCPgAscgkIAAiPIA9AAIAAAUIgoAAIAAApIAjAAIAAAUIgjAAIAAAqIAoAAIAAAUgAtxgkIAAiPIAeAAQASABAJAJQAJAKAAARIAABDQAAAUgJAJQgKAKgSAAgAtcg3IAJAAQAJAAADgEQAEgFAAgJIAAhFQAAgIgDgFQgEgEgJAAIgJAAgAAFhRIAAgUIA8AAIAAAUg");
	this.shape.setTransform(-2.8,0.8);

	// Layer 1
	this.instance = new lib.cloud3();
	this.instance.setTransform(-145.3,-71,1.127,1.127);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-145.3,-71,290.8,142);


(lib.mc8 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("ASPC+IgBAAIgBAAIgCAAQgNgBgKgJQgKgJAAgRIAAhPQAAgRAKgJQAKgKANAAIACgBIABAAIABAAIABABQANAAALAKQAJAJABARIAAAIIgWAAIAAgIQABgIgFgDQgFgEgFAAQgGAAgEAEQgFADAAAIIAABPQAAAIAFADQAEAEAGAAQAFAAAFgEQAFgDgBgIIAAgcIgRAAIAAgTIAnAAIAAAvQgBARgJAJQgLAJgNABIgBAAgAC/C+IgCAAIgBAAIgBAAQgMgBgLgJQgKgJAAgRIAAhPQAAgRAKgJQALgKAMAAIABgBIABAAIACAAIACABQAMAAAKAKQAKAJAAARIAAAIIgVAAIAAgIQAAgIgEgDQgEgEgHAAQgFAAgFAEQgEADAAAIIAABPQAAAIAEADQAFAEAFAAQAHAAAEgEQAEgDAAgIIAAgcIgRAAIAAgTIAmAAIAAAvQAAARgKAJQgKAJgMABIgCAAgABMC0QgKgKAAgOIAAh0IAVAAIAAByQAAAHAEAEQADAEAGAAQAGAAADgEQAFgEAAgHIAAhyIAVAAIAAB0QAAAOgLAKQgKAKgOAAQgOAAgKgKgAAMC+IgCAAIgBAAIgBAAQgKgBgLgJQgKgJAAgRIAAhPQAAgRAKgJQALgKAKAAIABgBIABAAIACAAIACABQAMAAAKAKQAKAJAAARIAABPQAAARgKAJQgKAJgMABIgCAAgAAABAQgCADAAAIIAABPQAAAIACADQAFAEAFAAQAHAAAEgEQAEgDAAgIIAAhPQAAgIgEgDQgEgEgHAAQgFAAgFAEgAnIC+IgCAAIgBAAIgBAAQgMgBgLgJQgKgJAAgRIAAhPQAAgRAKgJQALgKAMAAIABgBIABAAIACAAIACABQAMAAAKAKQALAJgBARIAABPQABARgLAJQgKAJgMABIgCAAgAnUBAQgEADAAAIIAABPQAAAIAEADQAFAEAFAAQAHAAAEgEQAEgDABgIIAAhPQgBgIgEgDQgEgEgHAAQgFAAgFAEgAu2C+IgCAAIgBAAIgBAAQgMgBgLgJQgKgJAAgRIAAhPQAAgRAKgJQALgKAMAAIABgBIABAAIACAAIACABQAMAAAKAKQAKAJAAARIAABPQAAARgKAJQgKAJgMABIgCAAgAvCBAQgEADAAAIIAABPQAAAIAEADQAFAEAFAAQAHAAAEgEQAEgDAAgIIAAhPQAAgIgEgDQgEgEgHAAQgFAAgFAEgAREC9IghhaIAAAAIAABaIgWAAIAAiVIAVAAIAgBaIAAAAIAAhaIAWAAIAACVgAPkC9IAAiVIAVAAIAACVgAO8C9IghhaIAAAAIAABaIgWAAIAAiVIAVAAIAgBaIAAAAIAAhaIAWAAIAACVgANcC9IAAiVIAVAAIAACVgAMxC9IAAhbIgVBBIgLAAIgVhBIgBAAIAABbIgVAAIAAiVIAVAAIAbBPIAAAAIAchPIAUAAIAACVgAKfC9IgGggIgdAAIgHAgIgVAAIAiiVIARAAIAhCVgAJ/CJIAWAAIgKg2IgBAAgAIyC9IAAiBIgZAAIAAgUIBIAAIAAAUIgaAAIAACBgAIEC9IgGggIgdAAIgHAgIgVAAIAhiVIASAAIAhCVgAHkCJIAWAAIgLg2IAAAAgAF1C9IAAiVIAfAAQATAAAJAKQAKAKAAASIAABGQABAVgLAKQgKAKgTAAgAGKCpIAKAAQAKAAADgEQAEgFAAgKIAAhIQAAgIgDgFQgEgFgKAAIgKAAgAElC9IAAhCIgbAAIAABCIgWAAIAAiVIAWAAIAABAIAbAAIAAhAIAVAAIAACVgAg5C9IgUhAIgNAAIAABAIgVAAIAAiVIAiAAQATAAAKALQAJALAAAWQAAAMgFAKQgEAJgJAGIAWBEgAhaBrIAMAAQAMAAACgHQADgGAAgLQABgLgEgGQgDgGgMAAIgLAAgAiZC9IAAhCIgbAAIAABCIgVAAIAAiVIAVAAIAABAIAbAAIAAhAIAVAAIAACVgAkAC9IAAiBIgZAAIAAgUIBHAAIAAAUIgZAAIAACBgAldC9IgghaIgBAAIAABaIgWAAIAAiVIAWAAIAfBaIABAAIAAhaIAWAAIAACVgAoWC9IAAiVIAVAAIAACVgApPC9IAAiBIgYAAIAAgUIBHAAIAAAUIgZAAIAACBgAp9C9IgFggIgeAAIgGAgIgWAAIAiiVIARAAIAiCVgAqcCJIAVAAIgKg2IgBAAgArcC9IAAhbIgWBBIgKAAIgVhBIAAAAIAABbIgWAAIAAiVIAVAAIAbBPIABAAIAbhPIAUAAIAACVgAtKC9IgUhAIgNAAIAABAIgWAAIAAiVIAjAAQATAAAJALQAKALAAAWQAAAMgFAKQgDAJgLAGIAXBEgAtrBrIAMAAQAMAAADgHQACgGAAgLQAAgLgDgGQgDgGgMAAIgLAAgAwnC9IAAiVIA/AAIAAAUIgqAAIAAAtIAlAAIAAAUIglAAIAABAgAxOC9IghhaIgBAAIAABaIgWAAIAAiVIAWAAIAfBaIABAAIAAhaIAWAAIAACVgAyvC9IAAiVIAVAAIAACVgANggvQgKgKAAgPIAAgIIAWAAIAAAGQAAAGADAFQADAFAIAAQAMAAACgGQACgGAAgLQAAgKgBgEQgDgEgFgDIgBgBIgCAAIgQgHQgOgFgEgKQgFgJAAgOQAAgRAJgMQAJgMASAAQAQAAAKAKQAJAKABAMIAAABIAAABIAAAKIgVAAIAAgFQAAgHgDgFQgEgGgIAAQgJAAgDAGQgCAGAAAIQAAAIABAEQACAEAHADIABAAIABABIAQAGQAOAFAFAKQADAKAAAQQAAATgIAMQgIANgVAAQgPAAgLgKgAMLgvQgKgKAAgPIAAh0IAVAAIAAByQAAAIAEAEQAEAEAFAAQAGAAAEgEQADgEAAgIIAAhyIAWAAIAAB0QAAAPgLAKQgKAKgOAAQgOAAgKgKgALLglIgBAAIgCAAIgBAAQgMgBgLgJQgKgJAAgSIAAhOQAAgSAKgJQALgJAMgBIABAAIACAAIABAAIACAAQAMABAKAJQALAJAAASIAABOQAAASgLAJQgKAJgMABIgCAAgAK/ikQgEAEAAAIIAABOQAAAIAEAEQAFAEAGAAQAGAAAEgEQAFgEgBgIIAAhOQABgIgFgEQgEgEgGAAQgGAAgFAEgAJGglIgDgBQgLgBgKgHQgIgIgBgSIAAhTQAAgOAKgKQAKgKAPAAQAPAAAKAKQAKAKAAAQIAAAIIgWAAIAAgHQAAgGgEgFQgDgFgGAAQgIAAgCAFQgDAFAAAIIAABMQAAAHADAEQACAFAIAAIABgBIACAAQAEgBADgDQADgDAAgIIAAgHIAWAAIAAAJQAAAOgKALQgKAKgPAAIgCAAgAyLglIgCAAIgBAAIgCAAQgMgBgKgJQgKgJAAgSIAAhOQAAgSAKgJQAKgJAMgBIACAAIABAAIACAAIABAAQAMABALAJQAKAJAAASIAAAHIgVAAIAAgHQAAgIgEgEQgFgEgGAAQgGAAgEAEQgEAEgBAIIAABOQABAIAEAEQAEAEAGAAQAGAAAFgEQAEgEAAgIIAAgcIgSAAIAAgTIAnAAIAAAvQAAASgKAJQgLAJgMABIgBAAgAJ8gmIAAiWIAWAAIAACWgAHZgmIAAiWIBAAAIAAAUIgrAAIAAAsIAlAAIAAAUIglAAIAAAsIArAAIAAAWgAG2gmIgUhAIgNAAIAABAIgVAAIAAiWIAiAAQATAAAKALQAJALAAAWQAAANgFAJQgDAKgKAGIAWBEgAGVh5IAMAAQAMAAACgGQADgGABgLQAAgMgEgGQgDgGgMAAIgLAAgAEqgmIAAiWIAgAAQAJAAAHADQAGACAGAGQAGAGACAHQABAIAAAOIgBARQAAAHgFAGQgEAHgIAEQgHAEgMAAIgKAAIAAA7gAFAh1IAKAAQANAAACgHQACgHABgLQAAgKgCgIQgCgIgOAAIgKAAgAC1gmIAAiWIBBAAIAAAUIgrAAIAAAsIAlAAIAAAUIglAAIAAAsIArAAIAAAWgAB+gmIAAiCIgYAAIAAgUIBGAAIAAAUIgYAAIAACCgABQgmIgGggIgeAAIgFAgIgWAAIAiiWIARAAIAiCWgAAxhbIAVAAIgKg2IgBAAgAg9gmIAAiWIAgAAQASAAAKALQAIAKgBASIAABGQAAAVgIAKQgKAKgTAAgAgng6IAKAAQAIAAAEgFQAEgEAAgKIAAhIQAAgJgEgFQgDgFgJAAIgKAAgAhngmIAAiWIAVAAIAACWgAi1gmIAAiWIAVAAIAACAIAqAAIAAAWgAjWgmIgFggIgeAAIgGAgIgWAAIAiiWIARAAIAiCWgAj1hbIAVAAIgKg2IgBAAgAlKgmIgeiWIAXAAIARBpIABAAIAQhpIAXAAIgeCWgAnhgmIAAiWIAfAAQATAAAJALQAKAKAAASIAABGQAAAVgKAKQgKAKgTAAgAnMg6IAKAAQAKAAADgFQAEgEAAgKIAAhIQAAgJgDgFQgEgFgKAAIgKAAgAoIgmIghhaIAAAAIAABaIgWAAIAAiWIAVAAIAgBaIAAAAIAAhaIAWAAIAACWgApegmIgGggIgdAAIgHAgIgVAAIAhiWIASAAIAhCWgAp+hbIAWAAIgLg2IAAAAgArdgmIgVhAIgMAAIAABAIgWAAIAAiWIAiAAQAUAAAJALQAKALAAAWQAAANgFAJQgEAKgKAGIAXBEgAr+h5IALAAQAMAAADgGQADgGAAgLQAAgMgDgGQgDgGgNAAIgKAAgAthgmIAAiWIA/AAIAAAUIgqAAIAAAsIAlAAIAAAUIglAAIAAAsIAqAAIAAAWgAuLgmIAAhDIgbAAIAABDIgVAAIAAiWIAVAAIAABAIAbAAIAAhAIAWAAIAACWgAvygmIAAiCIgZAAIAAgUIBHAAIAAAUIgZAAIAACCgAwggmIgHggIgdAAIgGAgIgVAAIAhiWIARAAIAiCWgAxAhbIAWAAIgLg2IgBAAg");
	this.shape.setTransform(-7.9,3.9);

	// Layer 1
	this.instance = new lib.cloud5();
	this.instance.setTransform(-160.5,-73.5);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-160.5,-73.5,321,147);


(lib.mc7 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AHMEJQgKgKABgNIAAgHIATAAIAAAGQgBAFAEAEQADAEAHAAQAKAAADgFQACgFAAgKQgBgJgBgEQgBgEgGgCIgBgBIgCAAIgOgGQgMgFgFgIQgDgJAAgNQAAgPAIgLQAIgLAQAAQAOAAAKAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgEgFQgDgEgHAAQgIAAgDAFQgDAGABAGQgBAIACADQACAEAGADIABAAIABAAIAPAGQANAFADAJQAEAIAAAPQAAARgIALQgHAMgTAAQgNAAgKgJgAFOESIgDgBQgJgBgJgHQgIgHAAgQIAAhKQAAgOAIgJQAKgJAOAAQANAAAJAJQAJAKgBAOIAAAHIgTAAIAAgGQABgGgEgEQgDgEgFAAQgIAAgCAEQgCAFAAAHIAABFQAAAFACAEQADAEAGAAIACAAIABAAQAEgBADgDQACgDAAgGIAAgHIATAAIAAAJQABAMgJAJQgJAKgNAAIgDAAgAgfEJQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAEQADAEAHAAQAIAAADgFQABgFAAgKQAAgJgBgEQgCgEgDgCIgBgBIgCAAIgOgGQgMgFgFgIQgDgJAAgNQgBgPAJgLQAIgLARAAQALAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgCgFQgCgEgHAAQgIAAgDAFQgCAGgBAGQABAIABADQACAEAGADIABAAIAAAAIAOAGQANAFADAJQAEAIAAAPQAAARgIALQgHAMgRAAQgOAAgJgJgAmmEJQgKgKAAgNIAAgHIATAAIAAAGQAAAFAEAEQADAEAHAAQAKAAACgFQACgFABgKQAAgJgCgEQgBgEgGgCIgBgBIgCAAIgNgGQgNgFgEgIQgEgJgBgNQABgPAHgLQAJgLAQAAQAOAAAKAJQAIAJABALIAAABIAAAKIgTAAIAAgEQgBgHgDgFQgDgEgHAAQgIAAgDAFQgCAGAAAGQAAAIABADQACAEAGADIABAAIABAAIAOAGQAOAFADAJQAEAIAAAPQAAARgHALQgIAMgTAAQgNAAgKgJgAodESIgCgBQgKgBgIgHQgIgHgBgQIAAhKQABgOAIgJQAJgJAOAAQANAAAJAJQAJAKAAAOIAAAHIgTAAIAAgGQAAgGgEgEQgDgEgFAAQgHAAgDAEQgCAFAAAHIAABFQAAAFACAEQADAEAGAAIACAAIACAAQADgBADgDQADgDAAgGIAAgHIATAAIAAAJQAAAMgJAJQgJAKgMAAIgEAAgAqAEJQgKgJAAgOIAAhpIAUAAIAABnQAAAHAEAEQADADAFAAQAFAAAEgDQADgEAAgHIAAhnIATAAIAABpQAAAOgJAJQgJAJgNAAQgNAAgJgJgAsMESIgCAAIgBgBQgLAAgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgJALgBIABAAIACAAIABAAIACAAQALABAJAJQAJAIAAAPIAABIQAAAPgJAJQgJAIgLAAIgCABIgBAAgAsVCfQgEADAAAHIAABIQAAAHAEADQADADAGAAQAGAAADgDQAEgDAAgHIAAhIQAAgHgEgDQgDgDgGAAQgGAAgDADgAF/ERIAAiIIA7AAIAAATIgnAAIAAAnIAiAAIAAATIgiAAIAAAnIAnAAIAAAUgAELERIAAiIIAUAAIAACIgADTERIgciIIAUAAIAQBfIABAAIAPhfIAVAAIgcCIgACdERIgSg6IgLAAIAAA6IgTAAIAAiIIAfAAQARAAAJALQAIAJAAAUQAAAMgEAIQgDAJgKAFIAVA+gACADGIALAAQAKAAADgGQADgFAAgKQAAgKgEgGQgDgFgKAAIgKAAgAAmERIAAiIIA6AAIAAATIgmAAIAAAnIAhAAIAAATIghAAIAAAnIAmAAIAAAUgAiaERIAAiIIAdAAQARAAAIAKQAKAJgBAQIAABAQAAASgJAKQgKAJgRAAgAiGD+IAJAAQAIAAAEgEQADgEAAgIIAAhCQAAgIgDgEQgEgEgIAAIgJAAgAi+ERIgdhSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIAAAAIAAhSIAUAAIAACIgAkLERIgGgdIgaAAIgGAdIgTAAIAeiIIAQAAIAfCIgAkoDhIAUAAIgJgxIgBAAgAneERIAAh1IgWAAIAAgTIBAAAIAAATIgXAAIAAB1gArbERIAAiIIAdAAQARAAAIAKQAKAJgBAQIAABAQAAASgJAKQgKAJgRAAgArHD+IAJAAQAIAAAEgEQADgEAAgIIAAhCQAAgIgDgEQgEgEgIAAIgJAAgAtLERIgSg6IgMAAIAAA6IgUAAIAAiIIAgAAQARAAAIALQAJAJAAAUQAAAMgEAIQgEAJgIAFIAUA+gAtpDGIALAAQAKAAADgGQADgFgBgKQAAgKgCgGQgDgFgLAAIgKAAgAvKERIAAiIIAdAAQAIAAAGADQAGACAGAFQAFAFABAHQACAIAAAMIgBAPQgBAGgDAGQgEAGgHAEQgHAEgKAAIgKAAIAAA1gAu2DKIAJAAQALAAACgHQADgGAAgKQAAgKgCgGQgCgHgLAAIgKAAgANBA7QgJgJAAgNIAAhnIATAAIAABlQABAHADADQADAEAGAAQAFAAADgEQADgDABgHIAAhlIATAAIAABnQAAANgKAJQgJAJgMAAQgNAAgKgJgAMHBEIgCAAIgBAAQgLgBgJgIQgKgIAAgQIAAhFQAAgQAKgIQAJgIALgBIABAAIACAAIABAAIABAAQALABAJAIQAKAIAAAQIAABFQAAAQgKAIQgJAIgLABIgBAAIgBAAgAL+gsQgFADAAAHIAABFQAAAHAFAEQADADAGAAQAFAAAEgDQADgEAAgHIAAhFQAAgHgDgDQgEgEgFAAQgGAAgDAEgAHDBEIgCAAIgBAAQgLgBgJgIQgJgIAAgQIAAhFQAAgQAJgIQAJgIALgBIABAAIACAAIABAAIACAAQALABAJAIQAJAIAAAQIAABFQAAAQgJAIQgJAIgLABIgCAAIgBAAgAG6gsQgEADAAAHIAABFQAAAHAEAEQADADAGAAQAGAAADgDQAEgEAAgHIAAhFQAAgHgEgDQgDgEgGAAQgGAAgDAEgAEJBEIgCAAIgBAAQgMgBgJgIQgJgIAAgQIAAhFQAAgQAJgIQAJgIAMgBIABAAIACAAIAAAAIACAAQAMABAIAIQAKAIAAAQIAABFQAAAQgKAIQgIAIgMABIgCAAIAAAAgAEAgsQgEADgBAHIAABFQABAHAEAEQADADAGAAQAFAAAEgDQADgEAAgHIAAhFQAAgHgDgDQgEgEgFAAQgGAAgDAEgAgJBEIgBAAIgBAAQgMgBgIgIQgKgIAAgQIAAhFQAAgQAKgIQAIgIAMgBIABAAIABAAIABAAIACAAQAJABAKAIQAJAIAAAQIAABFQAAAQgJAIQgKAIgJABIgCAAIgBAAgAgSgsQgDADAAAHIAABFQAAAHADAEQAEADAFAAQAGAAADgDQADgEAAgHIAAhFQAAgHgDgDQgDgEgGAAQgFAAgEAEgAsQBEIgCAAQgLgBgHgHQgJgHAAgRIAAhIQAAgNAJgJQAJgJANAAQAOAAAJAJQAJAJAAAOIAAAIIgTAAIAAgHQgBgFgDgFQgDgEgGAAQgHAAgCAFQgCAEgBAHIAABDQABAGACAEQACAEAHAAIACAAIABAAQADgBADgDQAEgDAAgHIAAgGIATAAIAAAIQAAANgJAJQgJAJgNAAIgDAAgAtdBEIgCAAIgBAAQgLgBgKgIQgJgIAAgQIAAhFQAAgQAJgIQAKgIALgBIABAAIACAAIABAAIABAAQALABAJAIQAKAIAAAQIAABFQAAAQgKAIQgJAIgLABIgBAAIgBAAgAtmgsQgFADAAAHIAABFQAAAHAFAEQADADAGAAQAFAAAEgDQADgEAAgHIAAhFQAAgHgDgDQgEgEgFAAQgGAAgDAEgAvEA7QgKgJAAgOIAAgHIATAAIAAAGQABAFADAFQADAEAIAAQAJAAACgFQACgGAAgJQAAgKgBgDQgCgEgFgDIgBAAIgCgBIgNgFQgNgDgEgJQgFgIAAgNQABgQAHgKQAJgLARAAQAOAAAIAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgFgHAAQgIAAgDAGQgDAFAAAHQAAAHACAEQACADAGADIABABIABAAIAOAGQANAEAEAHQAEAJAAAPQAAARgHALQgIALgSAAQgPAAgJgJgAO6BDIgSg6IgMAAIAAA6IgTAAIAAiFIAgAAQAQAAAJAKQAJAKgBAUQABALgEAJQgEAGgJAGIAVA9gAOcgGIALAAQALAAADgFQACgGAAgKQAAgKgDgFQgDgGgLAAIgKAAgAKwBDIAAg5IgbhMIAUAAIAQA2IAAAAIAQg2IAVAAIgbBMIAAA5gAIzBDIAAiFIA7AAIAAASIgnAAIAAAoIAiAAIAAAQIgiAAIAAAoIAnAAIAAATgAICBDIAAhzIgXAAIAAgSIBAAAIAAASIgWAAIAABzgAF+BDIAAhQIAAAAIgTA4IgKAAIgTg4IAAAAIAABQIgUAAIAAiFIASAAIAZBGIABAAIAZhGIASAAIAACFgADJBDIgSg6IgLAAIAAA6IgUAAIAAiFIAgAAQARAAAIAKQAJAKAAAUQAAALgEAJQgEAGgJAGIAUA9gACsgGIALAAQAKAAACgFQADgGAAgKQAAgKgDgFQgDgGgKAAIgKAAgABLBDIAAiFIAcAAQAJAAAGACQAGACAFAFQAFAGACAHQACAHAAAMIgBAPQgBAHgEADQgEAHgGAEQgIADgKAAIgKAAIAAA1gABegCIAJAAQAMAAACgGQACgHABgKQAAgJgCgHQgDgHgLAAIgKAAgAhaBDIAAhzIgWAAIAAgSIBAAAIAAASIgXAAIAABzgAinBDIgGgdIgbAAIgFAdIgUAAIAfiFIAQAAIAeCFgAjEAUIATAAIgJgvIAAAAgAj/BDIAAiFIAUAAIAACFgAlQBDIAAiFIAdAAQAQAAAJAJQAJAKAAAQIAAA9QAAATgJAJQgKAJgRAAgAk9AxIAJAAQAJAAADgEQADgEABgJIAAg/QgBgIgDgEQgDgFgJAAIgJAAgAmXBDIAAiFIA7AAIAAASIgnAAIAAAoIAhAAIAAAQIghAAIAAAoIAnAAIAAATgAm8BDIAAhQIAAAAIgTA4IgKAAIgTg4IAAAAIAABQIgUAAIAAiFIASAAIAZBGIABAAIAYhGIATAAIAACFgApqBDIAAiFIAUAAIAAByIAmAAIAAATgAqHBDIgFgdIgbAAIgGAdIgTAAIAeiFIAQAAIAfCFgAqkAUIATAAIgJgvIAAAAgAreBDIAAiFIAUAAIAACFgAJbiHIgCAAIgBgBQgLAAgJgIQgJgJAAgPIAAhIQAAgPAJgIQAJgJALgBIABAAIACAAIABAAIACAAQALABAJAJQAJAIAAAPIAABIQAAAPgJAJQgJAIgLAAIgCABIgBAAgAJSj6QgEADAAAHIAABIQAAAHAEADQAEADAFAAQAFAAAEgDQAEgDAAgHIAAhIQAAgHgEgDQgEgDgFAAQgFAAgEADgADeiHIgBAAIgBgBQgLAAgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgJALgBIABAAIABAAIABAAIACAAQALABAKAJQAJAIAAAPIAABIQAAAPgJAJQgKAIgLAAIgCABIgBAAgADVj6QgDADAAAHIAABIQAAAHADADQAEADAFAAQAGAAADgDQAFgDAAgHIAAhIQAAgHgFgDQgDgDgGAAQgFAAgEADgAkwiHIgCAAIgBgBQgLAAgJgIQgJgJAAgPIAAhIQAAgPAJgIQAJgJALgBIABAAIACAAIABAAIACAAQALABAJAJQAJAIAAAPIAABIQAAAPgJAJQgJAIgLAAIgCABIgBAAgAk5j6QgEADAAAHIAABIQAAAHAEADQAEADAFAAQAFAAAEgDQAEgDAAgHIAAhIQAAgHgEgDQgEgDgFAAQgFAAgEADgAnriHIgBAAIgCgBQgLAAgJgIQgKgJAAgPIAAhIQAAgPAKgIQAJgJALgBIACAAIABAAIABAAIABAAQAMABAJAJQAJAIAAAPIAAAHIgTAAIAAgHQAAgHgEgDQgDgDgGAAQgGAAgDADQgEADAAAHIAABIQAAAHAEADQADADAGAAQAGAAADgDQAEgDAAgHIAAgaIgQAAIAAgRIAjAAIAAArQAAAPgJAJQgJAIgMAAIgBABIgBAAgAKMiIIAAiIIA6AAIAAATIgmAAIAAApIAiAAIAAASIgiAAIAAA6gAH4iIIgSg6IgMAAIAAA6IgTAAIAAiIIAgAAQAQAAAJALQAJAJgBAUQABAMgEAIQgEAJgJAFIAVA+gAHajTIALAAQALAAADgGQACgFAAgKQAAgKgDgGQgDgFgLAAIgKAAgAGAiIIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAATIghAAIAAAnIAnAAIAAAUgAFPiIIgQhYIAAAAIgPBYIgSAAIgUiIIAUAAIAKBWIAQhWIAPAAIAQBYIAAAAIAJhYIAVAAIgUCIgABxiIIAAiIIAdAAQAIAAAHADQAGACAFAFQAFAFACAHQABAIAAAMIAAAPQgCAGgDAGQgEAGgHAEQgGAEgLAAIgKAAIAAA1gACFjPIAJAAQAMAAACgHQADgGgBgKQAAgKgBgGQgCgHgMAAIgKAAgAAHiIIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAATIghAAIAAAnIAnAAIAAAUgAgciIIAAg8IgYAAIAAA8IgUAAIAAiIIAUAAIAAA6IAYAAIAAg6IATAAIAACIgAh5iIIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gAjOiIIgehSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIABAAIAAhSIATAAIAACIgAm6iIIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAATIghAAIAAAnIAnAAIAAAUgAooiIIgFgdIgbAAIgFAdIgTAAIAdiIIARAAIAeCIgApEi4IATAAIgJgxIAAAAgAp4iIIgSg6IgLAAIAAA6IgUAAIAAiIIAfAAQARAAAJALQAJAJAAAUQgBAMgDAIQgEAJgJAFIAUA+gAqVjTIAKAAQALAAACgGQADgFAAgKQAAgKgDgGQgDgFgKAAIgKAAgArviIIAAiIIA5AAIAAATIgmAAIAAAnIAiAAIAAATIgiAAIAAAnIAmAAIAAAUgAsoiIIgciIIAVAAIAPBfIABAAIAPhfIAVAAIgcCIgAuEiIIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAATIghAAIAAAnIAnAAIAAAUgAvKiIIAAiIIAUAAIAAB0IAmAAIAAAUg");
	this.shape.setTransform(-9.9,-18.4);

	// Layer 1
	this.instance = new lib.cloud1();
	this.instance.setTransform(-157,-98);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-157,-98,314,196);


(lib.mc6 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0099FF").s().p("AAeCdIgBAAIgBAAQgKgBgJgIQgIgGAAgOIAAhAQAAgPAIgHQAJgIAKAAIABAAIABAAIABAAIABAAQAKAAAJAIQAIAHAAAPIAABAQAAAOgIAGQgJAIgKABIgBAAIgBAAgAAWA2QgEADAAAHIAABAQAAAGAEADQADACAFAAQAFAAADgCQAEgDAAgGIAAhAQAAgHgEgDQgDgDgFAAQgFAAgDADgAg8CUQgJgHAAgMIAAgHIASAAIAAAGQAAAEADAEQACADAHAAQAJAAACgEQACgFAAgJQAAgIgCgDQgBgEgFgCIgBAAIgBgBIgNgFQgLgEgEgIQgEgIAAgLQAAgNAIgLQAHgJAPAAQANAAAIAIQAHAIABAKIAAAAIAAABIAAAJIgRAAIAAgEQAAgGgDgEQgDgFgHAAQgHAAgCAFQgCAFAAAGQAAAGABAEQABADAGADIABAAIABAAIANAFQALAFAEAIQADAHAAANQAAAQgHAKQgGAKgRAAQgNAAgIgJgAmZCUQgJgHAAgMIAAgHIASAAIAAAGQAAAEADAEQACADAHAAQAJAAACgEQACgFAAgJQAAgIgCgDQgBgEgFgCIgBAAIgBgBIgNgFQgLgEgEgIQgEgIAAgLQAAgNAIgLQAHgJAPAAQANAAAIAIQAHAIABAKIAAAAIAAABIAAAJIgRAAIAAgEQAAgGgDgEQgDgFgHAAQgHAAgCAFQgCAFAAAGQAAAGABAEQABADAGADIABAAIABAAIANAFQALAFAEAIQADAHAAANQAAAQgHAKQgGAKgRAAQgNAAgIgJgAuiCdIgBAAIgBAAQgKgBgJgIQgIgGAAgOIAAhAQAAgPAIgHQAJgIAKAAIABAAIABAAIABAAIABAAQAKAAAJAIQAIAHAAAPIAABAQAAAOgIAGQgJAIgKABIgBAAIgBAAgAuqA2QgEADAAAHIAABAQAAAGAEADQADACAFAAQAFAAADgCQAEgDAAgGIAAhAQAAgHgEgDQgDgDgFAAQgFAAgDADgAG0CcIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAGYCcIgQg0IgLAAIAAA0IgRAAIAAh5IAcAAQAPAAAIAJQAHAJAAARQAAALgDAHQgDAIgJAFIATA3gAF9BZIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAFSCcIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gAE5BxIARAAIgJgrgAD6CcIgOhPIAAAAIgNBPIgQAAIgTh5IATAAIAJBNIANhNIAOAAIAOBOIAAAAIAJhOIASAAIgSB5gACVCcIAAhpIgUAAIAAgQIA5AAIAAAQIgUAAIAABpgABKCcIAAh5IA0AAIAAAQIgiAAIAAAlIAeAAIAAAQIgeAAIAAA0gAipCcIAAh5IAaAAQAOAAAIAIQAIAJAAAPIAAA4QAAAQgIAJQgJAIgPAAgAiYCLIAIAAQAIAAADgDQADgDAAgJIAAg5QAAgIgDgEQgDgEgIAAIgIAAgAjJCcIgbhJIAABJIgRAAIAAh5IAQAAIAbBJIAAhJIARAAIAAB5gAkOCcIgFgaIgYAAIgFAaIgRAAIAbh5IAOAAIAbB5gAkoBxIARAAIgIgrgAnjCcIAAh5IAaAAQAHAAAFACQAGABAEAFQAFAFACAHQABAGAAALIgBANQgBAGgDAFQgDAGgGADQgGAEgKAAIgJAAIAAAvgAnSBdIAJAAQAKgBACgFQACgGAAgJQAAgJgCgGQgCgGgKAAIgJAAgAooCcIAAh5IAaAAQAHAAAFACQAGABAEAFQAFAFACAHQABAGAAALIgBANQgBAGgDAFQgDAGgGADQgGAEgKAAIgJAAIAAAvgAoXBdIAJAAQAKgBACgFQACgGAAgJQAAgJgCgGQgCgGgKAAIgJAAgApCCcIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gApbBxIARAAIgJgrgArMCcIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAjIAiAAIAAASgAsLCcIAAh5IASAAIAABnIAiAAIAAASgAstCcIAAh5IASAAIAAB5gAt2CcIAAh5IAaAAQARAAAHAKQAHAIAAANIAAAEQAAAKgDAEQgEAFgFAEQAGADADAFQADAGAAAJIAAAHQAAAQgIAIQgIAJgRAAgAtkCKIAHAAQALAAACgFQACgGAAgJQAAgIgCgGQgDgEgKAAIgHAAgAtkBUIAIAAQAIAAADgDQADgFAAgJQAAgHgDgEQgEgFgIAAIgHAAgAvgCcIAAhJIAAAAIgRA0IgJAAIgRg0IAAAAIAABJIgSAAIAAh5IARAAIAWBAIABAAIAWhAIARAAIAAB5gAMfghIgCAAQgJgCgIgFQgHgHAAgOIAAhDQAAgMAIgIQAHgIANAAQAMAAAIAIQAIAIAAANIAAAGIgRAAIAAgFQAAgFgDgEQgDgEgFAAQgHAAgCAEQgCAEAAAHIAAA9QAAAGADADQACADAGABIABAAIACAAQADgBACgDQADgDAAgGIAAgFIARAAIAAAHQAAALgIAJQgIAIgMAAIgCAAgAD+ghIgBAAIgBAAQgKgBgIgHQgJgIAAgOIAAhAQAAgOAJgHQAIgHAKgBIABAAIABAAIACAAIABAAQAKABAIAHQAJAHAAAOIAAAGIgSAAIAAgGQAAgGgDgDQgEgDgFAAQgEAAgEADQgDADAAAGIAABAQAAAHADACQAEAEAEAAQAFAAAEgEQADgCAAgHIAAgWIgOAAIAAgQIAgAAIAAAmQAAAOgJAIQgIAHgKABIgBAAIgCAAgACigpQgIgIAAgMIAAheIARAAIAABcQAAAGADADQADAEAFAAQAFAAADgEQADgDAAgGIAAhcIARAAIAABeQAAAMgIAIQgJAIgLAAQgMAAgIgIgABtghIgBAAIgBAAQgKgBgIgHQgJgIAAgOIAAhAQAAgOAJgHQAIgHAKgBIABAAIABAAIACAAIABAAQAKABAIAHQAJAHAAAOIAABAQAAAOgJAIQgIAHgKABIgBAAIgCAAgABliIQgDADAAAGIAABAQAAAHADACQAEAEAEAAQAFAAAEgEQADgCAAgHIAAhAQAAgGgDgDQgEgDgFAAQgEAAgEADgAjWgpQgIgJAAgLIAAgHIARAAIAAAFQAAAFADAEQADADAHABQAIgBACgEQACgFAAgIQAAgJgBgDQgCgDgEgCIgBgBIgCAAIgMgGQgLgFgEgHQgEgHAAgMQAAgNAHgLQAIgJAPAAQAMAAAIAIQAIAIAAAKIAAAAIAAABIAAAJIgRAAIAAgEQAAgGgDgFQgCgEgHAAQgHAAgDAFQgCAFAAAGQAAAHACADQABADAGACIAAABIABAAIANAFQAMAEADAIQADAJAAANQAAAOgGALQgHAKgQAAQgNAAgJgIgAwBghIgBAAIgBAAQgKgBgIgHQgJgIAAgOIAAhAQAAgOAJgHQAIgHAKgBIABAAIABAAIACAAIABAAQAKABAIAHQAJAHAAAOIAAAGIgSAAIAAgGQAAgGgDgDQgEgDgFAAQgEAAgEADQgDADAAAGIAABAQAAAHADACQAEAEAEAAQAFAAAEgEQADgCAAgHIAAgWIgOAAIAAgQIAgAAIAAAmQAAAOgJAIQgIAHgKABIgBAAIgCAAgAPrgiIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAkIAjAAIAAARgAO5giIgZh5IASAAIAOBVIAOhVIATAAIgZB5gAOEgiIAAh5IARAAIAAB5gANWgiIAAhpIgUAAIAAgQIA6AAIAAAQIgUAAIAABpgALsgiIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gALThMIARAAIgJgsgAKkgiIgQgzIgKAAIAAAzIgSAAIAAh5IAcAAQAQAAAHAJQAIAJAAARQAAALgDAHQgEAIgIAFIATA3gAKKhlIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgDgFgJAAIgJAAgAI5giIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAkIAiAAIAAARgAIMgiIAAhpIgUAAIAAgQIA6AAIAAAQIgUAAIAABpgAHhgiIgbhJIAABJIgRAAIAAh5IAQAAIAbBJIAAhJIARAAIAAB5gAGTgiIAAh5IASAAIAAB5gAFSgiIAAg2IgWAAIAAA2IgRAAIAAh5IARAAIAAA0IAWAAIAAg0IARAAIAAB5gAA1giIgQgzIgLAAIAAAzIgRAAIAAh5IAcAAQAPAAAIAJQAHAJAAARQAAALgDAHQgDAIgJAFIATA3gAAahlIAKAAQAJAAADgFQACgFAAgJQAAgJgDgFQgCgFgKAAIgJAAgAgXgiIAAg2IgVAAIAAA2IgSAAIAAh5IASAAIAAA0IAVAAIAAg0IARAAIAAB5gAhrgiIAAhpIgUAAIAAgQIA6AAIAAAQIgUAAIAABpgAkjgiIAAh5IAaAAQAOAAAIAIQAIAJAAAPIAAA4QAAARgIAIQgJAIgPAAgAkSgyIAIAAQAIgBADgDQADgDAAgJIAAg5QAAgIgDgEQgDgEgIAAIgIAAgAk9giIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gAlWhMIARAAIgJgsgAmngiIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAkIAjAAIAAARgAnmgiIAAh5IARAAIAABoIAjAAIAAARgApFgiIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAkIAiAAIAAARgApygiIAAhpIgUAAIAAgQIA6AAIAAAQIgUAAIAABpgAqXgiIgFgaIgYAAIgFAaIgRAAIAbh5IAPAAIAbB5gAqwhMIARAAIgJgsgArfgiIgQgzIgKAAIAAAzIgSAAIAAh5IAcAAQAQAAAHAJQAIAJAAARQAAALgDAHQgEAIgIAFIATA3gAr5hlIAKAAQAJAAACgFQADgFAAgJQAAgJgDgFQgDgFgJAAIgJAAgAtKgiIAAh5IA0AAIAAAQIgiAAIAAAkIAeAAIAAAQIgeAAIAAAkIAiAAIAAARgAtqgiIgahJIAABJIgSAAIAAh5IARAAIAaBJIAAhJIASAAIAAB5gAvUgiIAAh5IA0AAIAAAQIgjAAIAAAkIAeAAIAAAQIgeAAIAAAkIAjAAIAAARg");
	this.shape.setTransform(4.3,86.5);

	// Layer 1
	this.instance = new lib.cloud2();
	this.instance.setTransform(-123.5,-157);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-123.5,-157,247,314);


(lib.mc5 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Aj9DWQgIgGAAgMIAAgFIAQAAIAAAEQAAAFACADQADAEAFAAQAIgBACgEQACgEAAgHQAAgIgCgDQgBgDgEgCIgBAAIgBgBIgLgEQgKgEgDgHQgEgGAAgLQAAgLAHgJQAGgJANAAQAMAAAGAIQAHAHABAJIAAAIIgPAAIAAgEQAAgEgDgFQgCgDgGAAQgGAAgCAEQgCAEAAAFIABAJQABADAFACIABAAIAMAGQAKADADAHQADAHAAAMQAAANgGAJQgGAIgPAAQgLABgHgIgAn3DWQgIgGAAgMIAAgFIAQAAIAAAEQAAAFACADQADAEAFAAQAIgBACgEQACgEAAgHQAAgIgCgDQgBgDgEgCIgBAAIgBgBIgLgEQgKgEgDgHQgEgGAAgLQAAgLAHgJQAGgJANAAQAMAAAGAIQAHAHABAJIAAAIIgPAAIAAgEQAAgEgDgFQgCgDgGAAQgGAAgCAEQgCAEAAAFIABAJQABADAFACIABAAIAMAGQAKADADAHQADAHAAAMQAAANgGAJQgGAIgPAAQgLABgHgIgApuDWQgIgGAAgMIAAgFIAQAAIAAAEQAAAFACADQADAEAFAAQAIgBACgEQACgEAAgHQAAgIgCgDQgBgDgEgCIgBAAIgBgBIgLgEQgKgEgDgHQgEgGAAgLQAAgLAHgJQAGgJANAAQAMAAAGAIQAHAHABAJIAAAIIgPAAIAAgEQAAgEgDgFQgCgDgGAAQgGAAgCAEQgCAEAAAFIABAJQABADAFACIABAAIAMAGQAKADADAHQADAHAAAMQAAANgGAJQgGAIgPAAQgLABgHgIgAkfDdIAAhBIgBAAIgPAuIgHAAIgPguIgBAAIAABBIgPAAIAAhrIAPAAIATA5IABAAIATg5IAPAAIAABrgAmNDdIAAhrIAuAAIAAAPIgeAAIAAAfIAaAAIAAAOIgaAAIAAAfIAeAAIAAAQgAm0DdIAAhcIgSAAIAAgPIAzAAIAAAPIgSAAIAABcgAonDdIAAgtIgVg+IAQAAIAMAqIABAAIAMgqIAQAAIgVA+IAAAtgAIeA2IgBAAIgBAAIgBAAQgJgBgIgGQgHgHAAgMIAAg3QAAgMAHgHQAIgGAJAAIABAAIABAAIABAAIABAAQAJAAAHAGQAHAHAAAMIAAAGIgPAAIAAgGQAAgFgDgDQgDgCgEgBQgEABgEACQgDADAAAFIAAA3QAAAGADACQAEACAEABQAEgBADgCQADgCAAgGIAAgVIgMAAIAAgLIAbAAIAAAgQAAAMgHAHQgHAGgJABIgBAAgAHeA2IgBAAIgBAAIgBAAQgJgBgIgGQgHgHAAgMIAAg3QAAgMAHgHQAIgGAJAAIABAAIABAAIABAAIABAAQAJAAAHAGQAHAHAAAMIAAA3QAAAMgHAHQgHAGgJABIgBAAgAHVgjQgDADAAAFIAAA3QAAAGADACQAEACAEABQAEgBADgCQADgCAAgGIAAg3QAAgFgDgDQgDgCgEgBQgEABgEACgAFmA2IgBAAIgBAAIgBAAQgJgBgHgGQgHgHAAgMIAAg3QAAgMAHgHQAHgGAJAAIABAAIABAAIABAAIABAAQAJAAAHAGQAIAHAAAMIAAA3QAAAMgIAHQgHAGgJABIgBAAgAFegjQgDADAAAFIAAA3QAAAGADACQADACAEABQAEgBAEgCQADgCAAgGIAAg3QAAgFgDgDQgEgCgEgBQgEABgDACgACjA2IgCAAQgIgCgGgFQgHgFAAgNIAAg5QAAgKAHgIQAHgGALAAQALAAAHAGQAHAIAAALIAAAGIgQAAIAAgGQAAgEgCgDQgDgDgEgBQgGABgCADQgCADAAAGIAAA1QAAAEACAEQADADAFAAIABAAIABAAIAFgDQACgDAAgFIAAgGIAQAAIAAAHQAAAKgHAIQgIAGgKABIgCAAgAhfA2IgBAAIgBAAIgBAAQgJgBgIgGQgHgHAAgMIAAg3QAAgMAHgHQAIgGAJAAIABAAIABAAIABAAIABAAQAJAAAHAGQAHAHAAAMIAAA3QAAAMgHAHQgHAGgJABIgBAAgAhogjQgDADAAAFIAAA3QAAAGADACQAEACAEABQAEgBADgCQADgCAAgGIAAg3QAAgFgDgDQgDgCgEgBQgEABgEACgAnAA2IgBAAIgBAAIgBAAQgJgBgIgGQgHgHAAgMIAAg3QAAgMAHgHQAIgGAJAAIABAAIABAAIABAAIABAAQAJAAAHAGQAHAHAAAMIAAA3QAAAMgHAHQgHAGgJABIgBAAgAnJgjQgDADAAAFIAAA3QAAAGADACQAEACAEABQAEgBADgCQADgCAAgGIAAg3QAAgFgDgDQgDgCgEgBQgEABgEACgAJSA1IAAgtIgVg8IAQAAIANAqIAAAAIAMgqIARAAIgVA8IAAAtgAGMA1IAAhpIAQAAIAABaIAeAAIAAAPgAEwA1IgXg+IAAA+IgQAAIAAhpIAPAAIAXA/IAAg/IAQAAIAABpgADsA1IAAgwIgTAAIAAAwIgPAAIAAhpIAPAAIAAAuIATAAIAAguIAPAAIAABpgABVA1IAAhpIAuAAIAAAOIgeAAIAAAgIAaAAIAAAMIgaAAIAAAgIAeAAIAAAPgAAuA1IAAhbIgSAAIAAgOIAzAAIAAAOIgSAAIAABbgAgTA1IgXg+IAAA+IgQAAIAAhpIAPAAIAXA/IAAg/IAQAAIAABpgAiXA1IAAhpIAPAAIAABpgAi/A1IAAhbIgSAAIAAgOIAzAAIAAAOIgSAAIAABbgAjgA1IgFgXIgVAAIgEAXIgPAAIAXhpIANAAIAYBpgAj3APIAPAAIgHgkgAkkA1IAAg/IgBAAIgPAsIgHAAIgPgsIgBAAIAAA/IgPAAIAAhpIAPAAIATA3IABAAIATg3IAPAAIAABpgAlzA1IgPguIgJAAIAAAuIgPAAIAAhpIAZAAQANAAAHAIQAHAIAAAPQAAAJgDAIQgDAEgIAEIARAxgAmLgEIAJAAQAIAAACgFQACgEAAgIQAAgIgCgEQgCgEgJgBIgIAAgAoSA1IAAhpIAuAAIAAAOIgeAAIAAAhIAaAAIAAAMIgaAAIAAAugAouA1IgXg+IAAA+IgQAAIAAhpIAPAAIAXA/IAAg/IAQAAIAABpgApyA1IAAhpIAPAAIAABpgAGKh4QgHgGAAgMIAAgFIAPAAIAAAEQAAAFADADQACAEAGgBQAIAAABgDQACgFAAgIQAAgHgBgDQgBgCgEgCIgBgBIgCAAIgLgFQgKgEgDgHQgDgHAAgKQAAgMAGgIQAHgIANAAQALgBAHAIQAHAHAAAIIAAAJIgPAAIAAgEQAAgEgCgFQgDgDgGAAQgGgBgCAFQgCAEAAAFIABAJQABADAGADIABAAIALAEQALAEADAHQACAHAAALQAAAOgFAJQgGAJgPAAQgLAAgIgIgAFNh4QgHgGAAgMIAAgFIAPAAIAAAEQAAAFADADQACAEAGgBQAIAAABgDQACgFAAgIQAAgHgBgDQgBgCgEgCIgBgBIgCAAIgLgFQgKgEgDgHQgDgHAAgKQAAgMAGgIQAHgIANAAQALgBAHAIQAHAHAAAIIAAAJIgPAAIAAgEQAAgEgCgFQgDgDgGAAQgGgBgCAFQgCAEAAAFIABAJQABADAGADIABAAIALAEQALAEADAHQACAHAAALQAAAOgFAJQgGAJgPAAQgLAAgIgIgACuhwIgCAAQgIgCgHgFQgGgGAAgNIAAg6QAAgLAGgGQAHgIALABQALgBAHAIQAHAGAAAMIAAAGIgPAAIAAgFQAAgFgDgDQgCgDgFAAQgFAAgCADQgCAEAAAFIAAA2QAAAFACAEQACACAFAAIABAAIACAAIAEgCQADgDAAgFIAAgGIAPAAIAAAHQAAAKgHAHQgHAIgKAAIgCAAgAh+hwIgBAAIgBAAIgBAAQgJgBgIgHQgHgGAAgMIAAg4QAAgNAHgGQAIgHAJAAIABAAIABAAIABAAIABAAQAJAAAHAHQAHAGAAANIAAA4QAAAMgHAGQgHAHgJABIgBAAgAiHjLQgDADAAAGIAAA4QAAAFADADQAEADAEgBQAEABADgDQADgDAAgFIAAg4QAAgGgDgDQgDgCgEAAQgEAAgEACgAlthwIgBAAIgBAAIgBAAQgJgBgHgHQgHgGAAgMIAAg4QAAgNAHgGQAHgHAJAAIABAAIABAAIABAAIABAAQAJAAAHAHQAIAGAAANIAAA4QAAAMgIAGQgHAHgJABIgBAAgAl1jLQgDADAAAGIAAA4QAAAFADADQADADAEgBQAEABAEgDQADgDAAgFIAAg4QAAgGgDgDQgEgCgEAAQgEAAgDACgAEyhxIgEgXIgVAAIgFAXIgPAAIAYhrIANAAIAXBrgAEciWIAPAAIgIgngADUhxIAAhrIAQAAIAABbIAeAAIAAAQgAAdhxIAAhrIAXAAQANAAAHAIQAHAHAAANIAAAyQAAAOgHAIQgIAHgNAAgAAtiAIAHAAQAGAAADgCQADgEAAgHIAAgzQAAgGgDgEQgCgDgHAAIgHAAgAgYhxIAAhrIAPAAIAABbIAdAAIAAAQgAgxhxIgPgtIgJAAIAAAtIgPAAIAAhrIAZAAQANAAAHAIQAHAIAAAPQAAAKgDAGQgDAHgIAEIARAxgAhJisIAJAAQAIAAACgEQACgFAAgIQAAgIgCgEQgCgFgJABIgIAAgAi/hxIgMhGIgBAAIgLBGIgOAAIgQhrIAQAAIAIBEIAMhEIALAAIANBGIAAAAIAIhGIAQAAIgQBrgAk4hxIAAgtIgVg+IAQAAIAMArIABAAIAMgrIAQAAIgVA+IAAAtgAm+hxIAAhrIAPAAIAABbIAfAAIAAAQgAn7hxIAAhrIAXAAQAGAAAFACQAFABAEAFQAEAEABAGQACAFAAAKIgBAMQgBAFgDAEQgDAGgFADQgGACgIAAIgIAAIAAAqgAnsipIAIAAQAJAAACgFQABgFAAgIQAAgHgBgGQgCgGgJABIgIAAgAozhxIAAhrIAuAAIAAAPIgeAAIAAAfIAaAAIAAAPIgaAAIAAAeIAeAAIAAAQgApzhxIAAhrIAXAAQANAAAHAIQAHAHAAANIAAAyQAAAOgHAIQgIAHgNAAgApjiAIAHAAQAGAAADgCQADgEAAgHIAAgzQAAgGgDgEQgCgDgHAAIgHAAgABdiTIAAgPIAtAAIAAAPg");
	this.shape.setTransform(8.5,-1.3);

	// Layer 1
	this.instance = new lib.cloud3();
	this.instance.setTransform(-129,-63);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-129,-63,258,126);


(lib.mc4 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("ABkCwIgCAAIgBAAQgLgBgJgIQgKgIABgQIAAhIQgBgPAKgIQAJgIALgCIABAAIACAAIABAAIACAAQALACAJAIQAJAIAAAPIAABIQAAAQgJAIQgJAIgLABIgCAAIgBAAgABbA9QgEAEAAAGIAABIQAAAHAEADQADADAGABQAGgBADgDQAEgDAAgHIAAhIQAAgGgEgEQgDgDgGAAQgGAAgDADgAm0CwIgCAAIgBAAQgLgBgKgIQgJgIAAgQIAAhIQAAgPAJgIQAKgIALgCIABAAIACAAIABAAIABAAQALACAJAIQAKAIAAAPIAABIQAAAQgKAIQgJAIgLABIgBAAIgBAAgAm9A9QgFAEAAAGIAABIQAAAHAFADQADADAGABQAFgBAEgDQADgDAAgHIAAhIQAAgGgDgEQgEgDgFAAQgGAAgDADgAoFCwIgDAAQgKgCgIgGQgJgIABgQIAAhKQgBgOAJgJQAJgIAOgBQAOABAJAIQAIAKAAAOIAAAIIgTAAIAAgHQAAgGgEgEQgCgEgGAAQgIAAgCAEQgCAFAAAHIAABFQAAAGACADQADAEAGABIACAAIABAAQAEgCADgDQADgDAAgGIAAgHIATAAIAAAJQAAANgIAJQgKAJgNAAIgCAAgArgCwIgCAAIgBAAQgLgBgJgIQgJgIAAgQIAAhIQAAgPAJgIQAJgIALgCIABAAIACAAIABAAIACAAQALACAJAIQAJAIAAAPIAABIQAAAQgJAIQgJAIgLABIgCAAIgBAAgArpA9QgEAEAAAGIAABIQAAAHAEADQAEADAFABQAFgBAEgDQAEgDAAgHIAAhIQAAgGgEgEQgEgDgFAAQgFAAgEADgADGCvIgehRIAAAAIAABRIgTAAIAAiHIASAAIAdBSIABAAIAAhSIAUAAIAACHgAAeCvIAAiHIAUAAIAACHgAgTCvIAAh1IgWAAIAAgSIA+AAIAAASIgVAAIAAB1gAhGCvIAAiHIAUAAIAACHgAh4CvIAAh1IgXAAIAAgSIBAAAIAAASIgWAAIAAB1gAjMCvIAAiHIA6AAIAAASIgmAAIAAAoIAhAAIAAASIghAAIAAAnIAmAAIAAAUgAkaCvIAAiHIAdAAQAIAAAHACQAGACAFAFQAFAGACAGQABAIAAAMIAAAPQgCAGgDAGQgEAGgHAEQgGAEgLAAIgKAAIAAA1gAkGBoIAJAAQAMAAACgGQACgHAAgKQAAgKgBgGQgCgHgMAAIgKAAgAk/CvIAAhTIAAAAIgTA7IgKAAIgTg7IgBAAIAABTIgTAAIAAiHIASAAIAZBIIABAAIAZhIIASAAIAACHgApqCvIAAhTIgBAAIgTA7IgKAAIgTg7IAAAAIAABTIgUAAIAAiHIATAAIAZBIIABAAIAYhIIATAAIAACHgAsgCvIgSg6IgLAAIAAA6IgUAAIAAiHIAgAAQARgBAIALQAJAKAAAUQAAALgDAIQgEAJgKAGIAVA9gAs9BkIALAAQAKAAADgFQADgGgBgKQAAgKgCgGQgDgFgLAAIgKAAgAuXCvIAAiHIA6AAIAAASIgnAAIAAApIAiAAIAAASIgiAAIAAA6gANfguQgKgKAAgOIAAgHIATAAIAAAGQAAAFAEAFQADAEAHAAQAKAAACgFQACgFABgKQAAgJgCgEQgBgEgGgDIgBAAIgCgBIgNgFQgNgFgEgJQgEgIgBgNQABgPAHgLQAJgLAQAAQAOAAAKAJQAIAJABALIAAABIAAAKIgTAAIAAgEQgBgHgDgFQgDgFgHABQgIgBgDAGQgCAGAAAGQAAAIABADQACAEAGADIABAAIABAAIAOAGQAOAEADAKQAEAIAAAPQAAARgHALQgIAMgTAAQgNgBgKgIgAMSguQgJgKAAgOIAAgHIATAAIAAAGQAAAFADAFQADAEAHAAQAKAAADgFQABgFABgKQgBgJgBgEQgCgEgFgDIgBAAIgCgBIgOgFQgMgFgFgJQgDgIAAgNQgBgPAJgLQAIgLARAAQANAAAJAJQAJAJABALIAAABIAAAKIgTAAIAAgEQgBgHgCgFQgEgFgHABQgIgBgDAGQgCAGgBAGQABAIABADQACAEAGADIABAAIAAAAIAQAGQANAEADAKQAEAIAAAPQAAARgIALQgHAMgTAAQgOgBgJgIgAIDguQgJgKAAgOIAAgHIATAAIAAAGQgBAFAEAFQADAEAHAAQAKAAADgFQACgFAAgKQgBgJgBgEQgBgEgGgDIgBAAIgCgBIgOgFQgMgFgFgJQgDgIAAgNQgBgPAJgLQAIgLAQAAQAOAAAJAJQAJAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgFgHABQgIgBgDAGQgDAGABAGQgBAIACADQACAEAGADIABAAIAAAAIAQAGQANAEADAKQAEAIAAAPQAAARgIALQgHAMgTAAQgOgBgJgIgAG1guQgJgKAAgNIAAhpIATAAIAABnQABAHADADQADAEAGAAQAFAAADgEQADgDABgHIAAhnIATAAIAABpQAAANgJAKQgJAIgNABQgNgBgKgIgACfguQgKgKAAgNIAAhpIAUAAIAABnQAAAHAEADQADAEAFAAQAFAAAEgEQADgDAAgHIAAhnIATAAIAABpQAAANgJAKQgJAIgNABQgNgBgJgIgABkglIgCAAIgBgBQgLgBgJgHQgKgJABgQIAAhHQgBgPAKgIQAJgJALgBIABAAIACAAIABAAIACAAQALABAJAJQAJAIAAAPIAABHQAAAQgJAJQgJAHgLABIgCABIgBAAgABbiYQgEADAAAHIAABHQAAAIAEADQADADAGAAQAGAAADgDQAEgDAAgIIAAhHQAAgHgEgDQgDgEgGABQgGgBgDAEgALFgmIAAiIIA7AAIAAATIgnAAIAAAnIAhAAIAAASIghAAIAAAoIAnAAIAAAUgAKhgmIgdhSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIAAAAIAAhSIAUAAIAACIgAJLgmIAAiIIATAAIAACIgAFbgmIAAiIIAcAAQATAAAIALQAIAKAAANIAAAFQAAALgDAGQgEAEgGAFQAGADAEAGQADAGAAALIAAAIQAAARgJAKQgJAKgTAAgAFug6IAJAAQAMAAACgGQACgHABgKQgBgKgCgFQgEgFgKAAIgJAAgAFuh2IAJAAQAKgBAEgEQACgFABgJQgBgJgDgFQgEgFgKABIgIAAgAEYgmIgSg7IgMAAIAAA7IgUAAIAAiIIAgAAQARAAAIALQAJAJAAAUQAAALgEAJQgEAIgIAGIAUA+gAD6hyIALAAQAKABADgGQADgFgBgLQAAgKgCgFQgDgGgLABIgKAAgAANgmIAAg6IgYhOIASAAIAPA2IABAAIAQg2IAUAAIgbBOIAAA6gAhtgmIAAiIIA6AAIAAATIgnAAIAAAnIAiAAIAAASIgiAAIAAAoIAnAAIAAAUgAifgmIAAh1IgXAAIAAgTIBAAAIAAATIgVAAIAAB1gAjJgmIgGgeIgaAAIgFAeIgUAAIAeiIIARAAIAdCIgAjlhWIATAAIgJgxIgBAAgAkggmIAAiIIATAAIAACIgAlSgmIAAh1IgXAAIAAgTIBAAAIAAATIgWAAIAAB1gAmEgmIgdhSIAAAAIAABSIgTAAIAAiIIASAAIAdBSIAAAAIAAhSIAUAAIAACIgAn7gmIAAiIIA6AAIAAATIgmAAIAAAnIAiAAIAAASIgiAAIAAAoIAmAAIAAAUgAobgmIgSg7IgLAAIAAA7IgUAAIAAiIIAgAAQARAAAIALQAJAJAAAUQAAALgDAJQgEAIgKAGIAVA+gAo4hyIALAAQAKABADgGQADgFgBgLQAAgKgCgFQgDgGgLABIgKAAgAqSgmIAAiIIA6AAIAAATIgnAAIAAAnIAiAAIAAASIgiAAIAAAoIAnAAIAAAUgArZgmIAAiIIA7AAIAAATIgnAAIAAAoIAhAAIAAASIghAAIAAA7gAsfgmIAAiIIA6AAIAAATIgmAAIAAAoIAhAAIAAASIghAAIAAA7gAtGgmIAAiIIAUAAIAACIgAuXgmIAAiIIAdAAQAQAAAJAKQAJAJAAAQIAABAQAAASgKAKQgJAIgRABgAuEg5IAJAAQAJAAADgEQADgEAAgIIAAhCQAAgIgDgEQgDgFgJABIgJAAg");
	this.shape.setTransform(2,-1);

	// Layer 1
	this.instance = new lib.cloud4();
	this.instance.setTransform(-130,-84.5);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc3 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#0064B0").s().p("AJuCnQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAFQADADAHAAQAKABADgGQABgFAAgJQAAgKgBgEQgCgEgFgCIgBAAIgCgBIgOgFQgMgFgFgJQgDgJAAgNQgBgPAJgLQAIgLARABQANgBAJAKQAJAJABAKIAAABIAAAKIgUAAIAAgDQAAgIgCgEQgEgFgHAAQgIAAgDAGQgCAFgBAGQABAIABADQACAEAGADIABAAIAAAAIAQAHQANAEADAJQAEAIAAAQQAAAQgIAMQgHALgTAAQgOAAgJgJgAFGCnQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAFQAEADAHAAQAKABACgGQACgFAAgJQAAgKgCgEQgBgEgGgCIgBAAIgBgBIgOgFQgNgFgEgJQgEgJAAgNQAAgPAIgLQAJgLAQABQAOgBAJAKQAIAJABAKIAAABIAAAKIgTAAIAAgDQAAgIgDgEQgEgFgGAAQgIAAgEAGQgCAFAAAGQAAAIABADQACAEAGADIABAAIABAAIAPAHQANAEAEAJQADAIAAAQQABAQgIAMQgIALgSAAQgOAAgKgJgABUCwIgBAAIgBgBQgMAAgJgIQgKgJAAgPIAAhHQAAgQAKgIQAJgIAMgBIABAAIABAAIABAAIABAAQAMABAJAIQAKAIAAAQIAABHQAAAPgKAJQgJAIgMAAIgBABIgBAAgABLA9QgEAEAAAHIAABHQAAAHAEAEQADACAGAAQAFAAAEgCQAEgEAAgHIAAhHQAAgHgEgEQgEgDgFAAQgGAAgDADgAjOCwIgDgBQgKAAgIgIQgJgGAAgRIAAhKQAAgNAJgJQAJgKAOABQAOgBAIAKQAJAJAAAOIAAAHIgTAAIAAgGQAAgFgDgFQgDgEgGAAQgHAAgCAFQgDAEAAAHIAABFQAAAFADAFQACADAHAAIABAAIABAAQAEAAADgDQADgEAAgGIAAgGIATAAIAAAIQAAAMgJAJQgIAKgNAAIgDAAgAnNCnQgKgJAAgNIAAhpIAUAAIAABmQAAAIAEADQADADAFAAQAFAAAEgDQADgDAAgIIAAhmIATAAIAABpQAAANgJAJQgJAJgNAAQgNAAgJgJgAIhCvIAAiHIA7AAIAAASIgnAAIAAAnIAhAAIAAATIghAAIAAAoIAnAAIAAATgAHbCvIAAiHIAUAAIAAB0IAmAAIAAATgAG+CvIgFgdIgbAAIgGAdIgTAAIAeiHIAQAAIAeCHgAGhCAIATAAIgJgyIAAAAgADXCvIAAiHIA6AAIAAASIgnAAIAAAnIAhAAIAAATIghAAIAAAoIAnAAIAAATgAC2CvIgRg6IgMAAIAAA6IgTAAIAAiHIAfAAQARgBAJALQAIAJAAAUQAAAMgEAJQgDAIgKAFIAVA+gACZBkIALAAQAKAAADgGQADgFAAgKQAAgKgEgFQgCgGgLAAIgKAAgAAPCvIAAhSIgBAAIgRA6IgJAAIgTg6IgBAAIAABSIgTAAIAAiHIATAAIAZBHIAAAAIAWhHIAUAAIAACHgAidCvIAAiHIA6AAIAAASIgnAAIAAAnIAiAAIAAATIgiAAIAAAoIAnAAIAAATgAkPCvIgdhSIgBAAIAABSIgTAAIAAiHIATAAIAdBRIAAAAIAAhRIAUAAIAACHgAmGCvIAAiHIA7AAIAAASIgnAAIAAAnIAhAAIAAATIghAAIAAAoIAnAAIAAATgAodCvIAAiHIAUAAIAAB0IAmAAIAAATgApjCvIAAiHIA5AAIAAASIgmAAIAAApIAhAAIAAASIghAAIAAA6gAqHCvIgehSIgBAAIAABSIgTAAIAAiHIATAAIAdBRIAAAAIAAhRIAUAAIAACHgAreCvIAAiHIATAAIAACHgAFigvQgKgJAAgNIAAgHIATAAIAAAGQAAAFADAEQAEAEAHAAQAKAAACgFQACgFAAgKQAAgJgCgEQgBgEgGgCIgBgBIgBAAIgOgGQgNgFgEgJQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgEgGgBQgIABgEAFQgCAGAAAGQAAAIABADQACAEAGACIABABIABAAIAPAGQANAFAEAIQADAJAAAPQABARgIALQgIALgSAAQgOABgJgKgADkgmIgCAAIgBAAQgMAAgJgJQgJgIAAgPIAAhIQAAgPAJgJQAJgIAMgBIABAAIACAAIAAAAIACAAQAMABAJAIQAJAJAAAPIAAAHIgUAAIAAgHQAAgHgEgDQgDgDgFgBQgGABgEADQgDADAAAHIAABIQAAAGADAEQAEADAGAAQAFAAADgDQAEgEAAgGIAAgaIgPAAIAAgRIAjAAIAAArQAAAPgJAIQgJAJgMAAIgCAAIAAAAgAAwgvQgKgJAAgNIAAgHIATAAIAAAGQAAAFADAEQAEAEAHAAQAKAAACgFQACgFAAgKQAAgJgCgEQgBgEgGgCIgBgBIgBAAIgOgGQgNgFgEgJQgEgIAAgNQAAgQAIgKQAJgLAQAAQAOAAAJAJQAIAJABALIAAABIAAAKIgTAAIAAgEQAAgHgDgFQgEgEgGgBQgIABgEAFQgCAGAAAGQAAAIABADQACAEAGACIABABIABAAIAPAGQANAFAEAIQADAJAAAPQABARgIALQgIALgSAAQgOABgJgKgAgcgvQgJgJAAgNIAAgHIATAAIAAAGQAAAFAEAEQACAEAIAAQAHAAADgFQACgFAAgKQAAgJgCgEQgBgEgFgCIAAgBIgCAAIgNgGQgMgFgFgJQgEgIAAgNQAAgQAIgKQAIgLARAAQAMAAAJAJQAJAJABALIAAABIAAAKIgUAAIAAgEQAAgHgDgFQgDgEgGgBQgHABgDAFQgDAGAAAGQAAAIACADQABAEAHACIABABIAAAAIANAGQANAFADAIQAFAJAAAPQAAARgIALQgHALgRAAQgOABgKgKgAK3gnIAAh1IgXAAIAAgSIBAAAIAAASIgWAAIAAB1gAKMgnIgFgcIgaAAIgGAcIgTAAIAdiHIARAAIAeCHgAJwhWIATAAIgJgxIgBAAgAI3gnIAAg8IgZAAIAAA8IgTAAIAAiHIATAAIAAA6IAZAAIAAg6IATAAIAACHgAHZgnIAAh1IgWAAIAAgSIBAAAIAAASIgXAAIAAB1gAEVgnIAAiHIA6AAIAAASIgmAAIAAAoIAhAAIAAATIghAAIAAAnIAmAAIAAATgACngnIgFgcIgbAAIgGAcIgTAAIAeiHIAQAAIAeCHgACKhWIATAAIgJgxIAAAAgAhognIAAiHIA6AAIAAASIgnAAIAAAoIAiAAIAAATIgiAAIAAAnIAnAAIAAATgAiNgnIAAhSIgBAAIgTA6IgJAAIgUg6IAAAAIAABSIgUAAIAAiHIATAAIAZBIIAAAAIAZhIIATAAIAACHgAkVgnIgSg5IgMAAIAAA5IgTAAIAAiHIAgAAQAQAAAJAKQAJAKAAAUQAAAMgEAIQgEAIgJAGIAUA9gAkzhxIAMAAQAJgBADgFQADgGAAgKQAAgJgDgGQgDgFgKgBIgLAAgAmNgnIAAiHIA7AAIAAASIgnAAIAAAoIAhAAIAAATIghAAIAAAnIAnAAIAAATgAnFgnIgciHIAVAAIAPBfIABAAIAQhfIAUAAIgcCHgAoAgnIAAiHIATAAIAACHgApHgnIAAiHIATAAIAAB0IAmAAIAAATgAqOgnIAAiHIA6AAIAAASIgnAAIAAAoIAiAAIAAATIgiAAIAAAnIAnAAIAAATgArfgnIAAiHIAdAAQAQAAAJAJQAJAKAAAQIAAA/QAAATgJAJQgKAKgRgBgArMg5IAJAAQAJAAADgEQAEgEAAgJIAAhBQAAgIgEgEQgDgEgJgBIgJAAg");
	this.shape.setTransform(-3.4,5);

	// Layer 1
	this.instance = new lib.cloud4();
	this.instance.setTransform(130,84.5,1,1,180);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-130,-84.5,260,169);


(lib.mc2 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AHwCnQgJgKAAgNIAAgHIATAAIAAAGQAAAFAEAFQADADAHAAQAJABADgGQACgFAAgJQAAgKgCgEQgBgEgGgCIgBAAIgCgBIgNgFQgNgFgEgJQgEgJAAgNQAAgPAIgLQAIgLARABQAOgBAJAKQAJAJAAAKIAAABIAAAKIgTAAIAAgDQAAgIgDgEQgEgFgGAAQgJAAgCAGQgDAFAAAGQAAAIABADQADAEAFADIABAAIABAAIAPAHQANAEADAJQAEAIAAAQQABAQgIAMQgHALgTAAQgOAAgKgJgAC4CwIgCAAIgBgBQgLAAgJgIQgJgJAAgPIAAhHQAAgQAJgIQAJgIALgBIABAAIACAAIABAAIACAAQALABAJAIQAJAIABAQIAABHQgBAPgJAJQgJAIgLAAIgCABIgBAAgACvA9QgEAEAAAHIAABHQAAAHAEAEQADADAGgBQAFABAEgDQAEgEAAgHIAAhHQAAgHgEgEQgEgDgFAAQgGAAgDADgAASCnQgJgKAAgNIAAgHIATAAIAAAGQAAAFADAFQAEADAHAAQAKABACgGQACgFAAgJQAAgKgCgEQgBgEgGgCIgBAAIgBgBIgOgFQgNgFgEgJQgEgJAAgNQAAgPAIgLQAJgLAQABQAOgBAJAKQAIAJABAKIAAABIAAAKIgTAAIAAgDQAAgIgDgEQgEgFgGAAQgIAAgEAGQgCAFAAAGQAAAIABADQACAEAGADIABAAIABAAIAPAHQANAEAEAJQADAIAAAQQABAQgIAMQgIALgSAAQgOAAgKgJgAg5CnQgKgJAAgNIAAhpIAUAAIAABmQAAAIAEADQADADAFAAQAFAAAEgDQADgDAAgIIAAhmIATAAIAABpQAAANgJAJQgJAJgNAAQgNAAgJgJgAh0CwIgCgBQgLAAgHgIQgJgGAAgRIAAhKQAAgNAJgJQAIgKAOABQAOgBAJAKQAJAJAAAOIAAAHIgUAAIAAgGQAAgFgDgFQgDgEgGAAQgHAAgCAFQgDAEABAHIAABFQgBAFADAFQACADAHAAIABAAIACAAQADAAADgDQADgEAAgGIAAgGIAUAAIAAAIQAAAMgJAJQgJAKgNAAIgDAAgAnCCwIgCAAIgBgBQgLAAgKgIQgJgJAAgPIAAhHQAAgQAJgIQAKgIALgBIABAAIACAAIABAAIABAAQAMABAIAIQAKAIAAAQIAABHQAAAPgKAJQgIAIgMAAIgBABIgBAAgAnLA9QgFAEAAAHIAABHQAAAHAFAEQADADAGgBQAFABAEgDQADgEAAgHIAAhHQAAgHgDgEQgEgDgFAAQgGAAgDADgAHLCvIgSg6IgMAAIAAA6IgUAAIAAiHIAgAAQARgBAJALQAIAJAAAUQAAAMgEAJQgDAIgJAFIAUA+gAGtBkIALAAQAKAAADgGQADgFAAgKQgBgKgDgFQgCgGgLAAIgKAAgAFTCvIAAiHIA6AAIAAASIgmAAIAAAnIAhAAIAAATIghAAIAAAoIAmAAIAAATgAEtCvIAAhSIAAAAIgTA6IgKAAIgTg6IAAAAIAABSIgTAAIAAiHIASAAIAZBHIABAAIAYhHIATAAIAACHgABmCvIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAj6CvIAAiHIATAAIAAB0IAnAAIAAATgAkXCvIgGgdIgaAAIgFAdIgUAAIAeiHIARAAIAeCHgAkzCAIATAAIgJgyIgBAAgAl/CvIAAg5IgbhOIAVAAIAPA1IABAAIAPg1IAVAAIgbBOIAAA5gAopCvIAAiHIAUAAIAAB0IAnAAIAAATgAFXguQgIgJAAgNQAAgMAFgIQAGgJAHgHQgEgHgEgIQgDgHAAgJQgBgLAIgIQAGgHANAAQANAAAGAHQAHAHAAAKQAAAKgGAIQgGAJgIAHIAFAJIAHAKIADAFIADAEIALgNIALAIIgOATIAOAWIgTAAIgHgKQgFAGgGADQgHACgHAAQgMAAgIgIgAFjhRQgDAFAAAHQAAAHAEAEQAEAFAHABQADgBADgCIAGgEIABgBIABgBIgTgeIgHAKgAFpiOQgCADAAAEQgBAFADAFIAFAKIACgCIACgCIAGgIQACgEAAgGIAAAAQgBgEgCgCQgDgDgDAAQgFAAgDAEgAACgmIgCAAQgJgBgHgHQgJgHAAgQIAAhLQAAgNAJgJQAJgJALAAQAOAAAJAJQAJAKAAAOIAAAHIgTAAIAAgGQgBgGgDgEQgDgFgGAAQgFABgCAEQgCAEgBAIIAABFQABAFACAEQACAEAFAAIACAAIABAAQADgBADgDQAEgDAAgGIAAgHIATAAIAAAJQAAAMgJAJQgJAJgNAAIgDAAgADngnIAAiHIA6AAIAAASIgmAAIAAAoIAhAAIAAATIghAAIAAAnIAmAAIAAATgACugnIgbiHIAVAAIAPBfIAAAAIAQhfIAUAAIgcCHgABzgnIAAiHIAUAAIAACHgABAgnIAAh1IgWAAIAAgSIBAAAIAAASIgWAAIAAB1gAg1gnIgGgcIgaAAIgFAcIgUAAIAeiHIARAAIAdCHgAhRhWIATAAIgJgxIgBAAgAitgnIgehRIAAAAIAABRIgTAAIAAiHIASAAIAdBSIABAAIAAhSIAUAAIAACHgAkEgnIAAiHIATAAIAACHgAkhgnIgGgcIgbAAIgFAcIgUAAIAfiHIAQAAIAeCHgAk+hWIATAAIgJgxIgBAAgAmEgnIAAh1IgXAAIAAgSIBAAAIAAASIgVAAIAAB1gAnYgnIAAiHIA7AAIAAASIgnAAIAAAoIAhAAIAAATIghAAIAAAnIAnAAIAAATgAn3gnIgSg5IgMAAIAAA5IgUAAIAAiHIAgAAQARAAAJAKQAIAKAAAUQAAAMgEAIQgEAIgIAGIAUA9gAoVhxIALAAQAKgBADgFQACgGAAgKQAAgJgDgGQgCgFgLgBIgKAAg");
	this.shape.setTransform(-3.7,-1.3);

	// Layer 1
	this.instance = new lib.cloud3();
	this.instance.setTransform(-129,-63);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-129,-63,258,126);


(lib.mc1 = function() {
	this.initialize();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AAhBLQgLgMAAgQIAAiBIAYAAIAAB+QgBAJAFAFQAFADAFAAQAHAAAEgDQAFgFAAgJIAAh+IAYAAIAACBQAAAQgMAMQgMAKgQABQgPgBgMgKgAglBWIgBAAIgBAAIgCAAQgPgCgLgJQgMgLABgUIAAhWQgBgTAMgLQALgKAPgBIACAAIABAAIABAAIACAAQAOABALAKQALALAAATIAABWQAAAUgLALQgLAJgOACIgCAAgAgxg3QgGAEAAAJIAABWQAAAJAGAFQAEADAHAAQAGAAAFgDQAFgFAAgJIAAhWQAAgJgFgEQgFgEgGAAQgHAAgEAEgAmFBLQgMgMAAgRIAAgJIAYAAIAAAHQAAAHAEAGQAEAEAJAAQANAAACgGQADgGAAgMQAAgNgCgDQgCgGgHgCIgBgBIgCAAIgRgIQgQgEgGgLQgEgLAAgPQgBgTAKgOQALgNAUAAQASAAALALQAKAMACANIAAABIAAABIAAAMIgYAAIAAgFQAAgJgEgGQgEgGgKAAQgJAAgDAHQgDAGAAAJQAAAJACAFQACAEAHADIABABIABABIASAGQARAGAEAJQAFAMAAARQAAAVgJAOQgJAOgYABQgRgBgMgKgAtaBWIgBAAIgCAAIgCAAQgOgCgLgJQgMgLAAgUIAAhWQAAgTAMgLQALgKAOgBIACAAIACAAIABAAIABAAQAPABALAKQAMALAAATIAABWQAAAUgMALQgLAJgPACIgBAAgAtng3QgFAEAAAJIAABWQAAAJAFAFQAFADAHAAQAGAAAFgDQAFgFAAgJIAAhWQAAgJgFgEQgFgEgGAAQgHAAgFAEgAJnBJQgNgLAAgSIAAgJIAYAAIAAAIQAAAHAFAGQADAEAKAAQALABAEgIQACgGAAgMQAAgMgCgEQgCgFgGgDIgCAAIgCgBIgSgHQgPgEgFgLQgGgKABgQQgBgTALgOQAKgNAVAAQASgBAKALQALAMABAOIAAABIAAABIAAALIgYAAIAAgFQAAgIgEgGQgEgHgJAAQgKABgDAGQgDAHgBAJQAAAJACAEQADAFAIADIABAAIABABIASAHQAPAGAFAJQAFALAAASQAAAVgJAOQgKAPgWAAQgSAAgLgMgAC4BVIgXhIIgOAAIAABIIgZAAIAAinIAnAAQAVABAMALQAKANgBAZQAAAOgEALQgEAIgMAHIAaBNgACTgHIANAAQANAAADgGQADgHAAgMQAAgOgDgGQgEgHgNAAIgMAAgAiSBVIAAhHIgihgIAaAAIATBEIABAAIAThEIAaAAIghBgIAABHgAksBVIAAinIBIAAIAAAXIgwAAIAAAxIApAAIAAAVIgpAAIAAAxIAwAAIAAAZgAm8BVIAAinIAZAAIAACngAnrBVIAAhlIgZBGIgLAAIgYhGIgBAAIAABlIgYAAIAAinIAYAAIAeBXIABAAIAfhXIAXAAIAACngApvBVIAAinIAZAAIAACngAquBVIAAiQIgcAAIAAgXIBQAAIAAAXIgcAAIAACQgAsdBVIAAinIAkAAQAJABAIACQAHADAGAGQAHAHACAIQACAKAAAPQAAAKgBAJQgBAHgEAGQgGAHgIAFQgIAFgNAAIgNAAIAABCgAsGgCIAMAAQAPABACgJQACgIABgMQAAgMgCgIQgDgJgOAAIgNAAgAM+BTIAAimIBHAAIAAAWIgwAAIAAAyIAqAAIAAAUIgqAAIAAAyIAwAAIAAAYgAL/BTIAAiQIgcAAIAAgWIBQAAIAAAWIgcAAIAACQgALABTIAAimIAYAAIAACmgAH7BTIAAimIAjAAQAXAAAJANQAKANAAARIAAAFQABAOgFAHQgEAGgIAFQAJADAEAHQADAIAAANIAAAKQAAAVgLAMQgKAMgYAAgAISA7IALAAQAOAAAEgIQADgIAAgMQgBgNgDgGQgDgHgOAAIgLAAgAISgOIALAAQAMAAAEgGQAFgGAAgMQgBgKgEgHQgFgFgLgBIgLAAgAGjBTIAAimIBIAAIAAAWIgwAAIAAAyIApAAIAAAUIgpAAIAAAyIAwAAIAAAYgAFkBTIgShqIgSBqIgXAAIgZimIAZAAIANBpIAAAAIAThpIASAAIAUBrIAAAAIAMhrIAZAAIgYCmg");
	this.shape.setTransform(-12.6,-19.5);

	// Layer 1
	this.instance = new lib.cloud1();
	this.instance.setTransform(-157,-98);

	this.addChild(this.instance,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-157,-98,314,196);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;